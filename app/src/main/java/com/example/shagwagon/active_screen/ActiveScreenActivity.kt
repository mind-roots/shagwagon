package com.example.shagwagon.active_screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.shagwagon.R

class ActiveScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_active_screen)
    }
}
