package com.example.shagwagon.expanded_view_screen

import android.os.Parcel
import android.os.Parcelable

class ExpandableInnerListModel() : Parcelable {


    var service_id: String = ""
    var service_name: String = ""
    var price: String = ""
    var discount: String = ""
    var checked = false

    constructor(parcel: Parcel) : this() {
        service_id = parcel.readString()
        service_name = parcel.readString()
        price = parcel.readString()
        discount = parcel.readString()
        checked = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(service_id)
        parcel.writeString(service_name)
        parcel.writeString(price)
        parcel.writeString(discount)
        parcel.writeByte(if (checked) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ExpandableInnerListModel> {
        override fun createFromParcel(parcel: Parcel): ExpandableInnerListModel {
            return ExpandableInnerListModel(parcel)
        }

        override fun newArray(size: Int): Array<ExpandableInnerListModel?> {
            return arrayOfNulls(size)
        }
    }

}