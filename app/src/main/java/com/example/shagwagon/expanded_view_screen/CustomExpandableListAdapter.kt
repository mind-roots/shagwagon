package com.example.shagwagon.expanded_view_screen

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import android.widget.*
import com.example.shagwagon.R
import com.example.shagwagon.pending_screen.Pendingmodel
import com.google.gson.Gson
import com.google.gson.JsonObject


class CustomExpandableListAdapter(
    private val context: Context
) : BaseExpandableListAdapter() {

    var mm = context as abc
    var parentList = ArrayList<ExpandedListModel>()

    lateinit var listDataHeader: ArrayList<String>
    var image: String = ""

    lateinit var checkBox: CheckBox
    var checkedArray = ArrayList<String>()

    lateinit var view: View
    //lateinit var listTitleTextView: TextView


    override fun getChild(listPosition: Int, expandedListPosition: Int): ExpandableInnerListModel {
        //  return expandableListDetail[expandableListTitle]
        val parent = parentList[listPosition]
        return parent.modelcategories[expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int, expandedListPosition: Int,
        isLastChild: Boolean, convertView: View?, parent: ViewGroup
    ): View {

        val parent = parentList[listPosition]

        val childItem = parent.modelcategories[expandedListPosition]

//        val listTitle = getGroup(listPosition)
//        val expandedListText = getChild(listPosition, expandedListPosition)
//
//        val sharedPreference = context.getSharedPreferences("Status", Context.MODE_PRIVATE)
//        val gson: Gson = Gson()
//        var json: String = sharedPreference.getString("data", "")
//        val data: Pendingmodel = gson.fromJson(json, Pendingmodel::class.java)
        var convertView = convertView


        if (convertView == null) {
            val layoutInflater = this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_item, null)
        }


        /*view = convertView!!.findViewById<View>(R.id.view) as View
        view.visibility = View.INVISIBLE*/


        checkBox = convertView!!.findViewById<View>(R.id.checkbox) as CheckBox
        checkBox.text = childItem.service_name

        /*if (parent.checked) {
            checkBox.isChecked = childItem.checked
        }
*/
        /*if(checkedArray.size!=0){
            for(i in 0 until checkedArray.count()){
                if(checkedArray[i]==expandedListText.name){
                    checkBox.isChecked=true
                }
                else{
                    checkBox.isChecked=false
                }
            }
        }*/


        var exist = 0


        checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
           // mm.getdata(expandedListText, isChecked)

            if (isChecked) {

                parent.checked = true
                childItem.checked = true

            } else {
                parent.checked = false
                childItem.checked = false
            }

            parent.modelcategories[expandedListPosition] = childItem
            parentList[listPosition] = parent

            notifyDataSetChanged()
        }

/*

        val expandedListTextView = convertView!!
            .findViewById<View>(R.id.expandedListItem) as TextView
*/

        val expandedListTextView1 = convertView
            .findViewById<View>(R.id.expandedListItemPrice) as TextView
        expandedListTextView1.text = childItem.price




        return convertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        val parent = parentList[listPosition]
        return parent.modelcategories.size
    }

    override fun getGroup(listPosition: Int): ExpandedListModel {
        //Log.e("data",data.category_name)

        return parentList[listPosition]

    }

    override fun getGroupCount(): Int {
        return parentList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        listPosition: Int, isExpanded: Boolean,
        convertView: View?, parent: ViewGroup
    ): View {

        val parentItem = parentList[listPosition]

        var convertView = convertView
       // val listTitle = getGroup(listPosition)
        if (convertView == null) {
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_group, null)
        }


        val listTitleTextView = convertView!!
            .findViewById<View>(R.id.listTitle) as TextView
        listTitleTextView.setTypeface(null, Typeface.BOLD)
        listTitleTextView.text = parentItem.category_name
        listTitleTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down_black, 0)
        val imageView = convertView.findViewById<View>(R.id.logo) as ImageView
        Picasso.get().load(parentItem.category_image).into(imageView)


        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        //view.visibility = View.VISIBLE
        //listTitleTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up_black, 0)

        return true
    }

    interface abc {
        fun getdata(
            abc: ModelSubCategory,
            checked: Boolean
        )

    }


    /*fun getResult(): String{

        for (model in parentList){

            if (model.checked){
                for (subCat in model.modelcategories){
                    if (subCat.checked){
                        val obj = JsonObject()
                        obj.addProperty("", subCat.service_id)
                        obj.addProperty("", subCat.service_id)
                        obj.addProperty("", subCat.service_id)
                        obj.addProperty("", subCat.service_id)

                    }
                }
            }

        }


    }
*/
    fun update(list : ArrayList<ExpandedListModel>){
        parentList = list
        notifyDataSetChanged()
    }
}