package com.example.shagwagon.expanded_view_screen

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.ArrayList

class ExpandedRespository(var application: Application) {
    private var msucessful = MutableLiveData<ArrayList<ExpandedListModel>>()
    private val mfailure = MutableLiveData<String>()
    val modelarray = ArrayList<ExpandedListModel>()

    fun getdata() {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()



        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.getServices()
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val resp = response.body()!!.string()
                    val json = JSONObject(resp)
                    val statuss = json.optString("status")
                    val msg = json.optString("msg")


                    if (statuss == "true") {
                        val data = json.optJSONArray("data")
                        for (i in 0 until data.length()) {
                            val model = ExpandedListModel()
                            val arrayobject1 = data.optJSONObject(i)
                            model.category_id = arrayobject1.optString("category_id")
                            model.category_name = arrayobject1.optString("category_name")
                            model.category_image = arrayobject1.optString("category_image")

                            Log.e("categorny name", model.category_name)




                            val data2 = arrayobject1.optJSONArray("services")
                            val internalmodel = ArrayList<ExpandableInnerListModel>()

                            for (j in 0 until data2.length()) {
                                val expandableInnerListModel =
                                    ExpandableInnerListModel()
                                val arrayobject2 = data2.optJSONObject(j)
                                expandableInnerListModel.service_id = arrayobject2.optString("service_id")
                                expandableInnerListModel.service_name = arrayobject2.optString("service_name")
                                Log.e("service name", expandableInnerListModel.service_name)
                                expandableInnerListModel.price = arrayobject2.optString("price")
                                //expandableInnerListModel.discount = arrayobject2.optString("discount")
                                internalmodel.add(expandableInnerListModel)



                            }
                            model.modelcategories = internalmodel
                            modelarray.add(model)
                        }
                        msucessful.value = modelarray
                    }
                }

                else {

                    val msg = "No Response"
                    mfailure.value = msg
                    Log.e(msg, mfailure.value)
                }


            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                mfailure.value = msg
                Log.e("Failure", mfailure.value)

            }


        })
    }

    fun pendingSucessful(): MutableLiveData<ArrayList<ExpandedListModel>> {

        return msucessful
    }

    //failure
    fun pendingFailure(): MutableLiveData<String> {

        return mfailure
    }

}