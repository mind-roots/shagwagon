package com.example.shagwagon.expanded_view_screen

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import com.example.shagwagon.pending_screen.InnerModel
import com.example.shagwagon.pending_screen.Pendingmodel
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception
import java.util.ArrayList

class ExpandedViewRespository(var application: Application)  {

    var mSucessful = MutableLiveData<Pendingmodel>()
    private val mfailure = MutableLiveData<String>()
    val modelarray = ArrayList<ExpandedListModel>()

    fun getdata(service: JSONArray, booking_id:String, updated_amount:String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()

        var expandedViewmodel = ExpandedViewModel()
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.AddExtraService(auth,service,booking_id,updated_amount)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val resp = response.body()!!.string()
                    val json = JSONObject(resp)
                    val statuss = json.optString("status")
                    val msg = json.optString("msg")


                    var model = Pendingmodel()

                    try {

                        val innerobject = json.getJSONObject("data")
                        model.booking_id = innerobject.optString("booking_id")
                        model.booking_type = innerobject.optString("booking_type")
                        model.booking_date = innerobject.optString("booking_date")
                        model.booking_time = innerobject.optString("booking_time")
                        model.latitude = innerobject.optString("latitude")
                        model.longitude = innerobject.optString("longitude")
                        model.total_amount = innerobject.optString("total_amount")
                        model.created_at = innerobject.optString("created_at")
                        model.status = innerobject.optString("status")
                        model.customer_id = innerobject.optString("customer_id")
                        model.driver_id = innerobject.optString("driver_id")
                        model.customer_name = innerobject.optString("customer_name")
                        model.customer_rating = innerobject.optString("customer_rating")
                        model.driver_name = innerobject.optString("driver_name")
                        model.driver_rating = innerobject.optString("driver_rating")
                        model.profile_image = innerobject.optString("profile_image")
                        model.driver_phone = innerobject.optString("driver_phone")
                        model.statuss = statuss
                        //  arraylist.add(model)
                        val arrayobj = innerobject.optJSONArray("booked_services")

                        var internalmodel = ArrayList<InnerModel>()
                        for (i in 0 until arrayobj.length()) {
                            val innermodel = InnerModel()
                            //getting the object
                            val innerobject = arrayobj.optJSONObject(i)
                            innermodel.service_id = innerobject.optString("service_id")
                            innermodel.price = innerobject.optString("price")
                            innermodel.name = innerobject.optString("name")

                            internalmodel.add(innermodel)

                        }
                        model.modelcategories=internalmodel

                        mSucessful.value = model

                    }catch (e:Exception){
                        e.printStackTrace()
                    }

                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                mfailure.value = msg
                Log.e("Failure", mfailure.value)

            }


        })
    }

    fun pendingSucessful(): MutableLiveData<Pendingmodel> {

        return mSucessful
    }

    //failure
    fun pendingFailure(): MutableLiveData<String> {

        return mfailure
    }
}