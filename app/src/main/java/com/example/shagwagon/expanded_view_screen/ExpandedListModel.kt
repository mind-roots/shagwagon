package com.example.shagwagon.expanded_view_screen

import android.os.Parcel
import android.os.Parcelable

class ExpandedListModel(): Parcelable  {


    var category_id : String = ""
    var category_name : String = ""
    var category_image : String = ""
    var checked = false
    var isExpanded = false
    var isCollapsed = true
    var modelcategories = ArrayList<ExpandableInnerListModel>()

    constructor(parcel: Parcel) : this() {
        category_id = parcel.readString()
        category_name = parcel.readString()
        category_image = parcel.readString()
        checked = parcel.readByte() != 0.toByte()
        isExpanded = parcel.readByte() != 0.toByte()
        isCollapsed = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(category_id)
        parcel.writeString(category_name)
        parcel.writeString(category_image)
        parcel.writeByte(if (checked) 1 else 0)
        parcel.writeByte(if (isExpanded) 1 else 0)
        parcel.writeByte(if (isCollapsed) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ExpandedListModel> {
        override fun createFromParcel(parcel: Parcel): ExpandedListModel {
            return ExpandedListModel(parcel)
        }

        override fun newArray(size: Int): Array<ExpandedListModel?> {
            return arrayOfNulls(size)
        }
    }


}