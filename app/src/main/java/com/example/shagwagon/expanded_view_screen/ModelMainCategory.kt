package com.example.shagwagon.expanded_view_screen

class ModelMainCategory {
    var image : String=""
    var h_name : String=""
    var status : Boolean =false
    var children : ArrayList<ModelSubCategory> = ArrayList()
}