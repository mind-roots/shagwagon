package com.example.shagwagon.expanded_view_screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ExpandableListView
import android.widget.Toast
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.shagwagon.databinding.ActivityExpandedViewBinding
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shagwagon.R
import com.example.shagwagon.pending_screen.Pendingmodel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_booking__screen.toolbar23
import kotlinx.android.synthetic.main.activity_expanded_view.*
import org.json.JSONArray
import java.util.HashMap


class ExpandedView : AppCompatActivity(), CustomExpandableListAdapter.abc {
    private lateinit var mvmodel: ExpandedViewmodelNew
    private lateinit var mvmodel1: ExpandedViewModel2
    private lateinit var mbinding: ActivityExpandedViewBinding
    private lateinit var recyclerAdapter: ParentAdapter
    var expandableListView: ExpandableListView? = null
    lateinit var expandableListAdapter: CustomExpandableListAdapter
    var expandableListTitle: ArrayList<ModelMainCategory>? = ArrayList()

    val expandableListDetail = HashMap<ModelMainCategory, List<ModelSubCategory>>()
    var expandableListData: ExpandableListData =
        ExpandableListData()
    var checkedArray = ArrayList<ModelSubCategory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_expanded_view)
        mvmodel = ViewModelProviders.of(this).get(ExpandedViewmodelNew::class.java)
        mvmodel1 = ViewModelProviders.of(this).get(ExpandedViewModel2::class.java)
        mbinding.expandedProgressBar.visibility = View.VISIBLE


        setAdapter()
        mvmodel.getdata()
        mvmodel.pendingSucessful().observe(this, Observer {
            recyclerAdapter.update(it)
            mbinding.expandedProgressBar.visibility = View.GONE
            //Toast.makeText(this, "all gud", Toast.LENGTH_SHORT).show()
        })
        //on failure
        mvmodel.pendingFailure().observe(this, Observer {
            mbinding.expandedProgressBar.visibility = View.GONE
            Toast.makeText(this, "Connection Issues", Toast.LENGTH_SHORT).show()


        })
        Log.e("expandableListDetail", expandableListDetail.toString())


        val toolbar = findViewById<Toolbar>(R.id.toolbar2)
        title = ""
        //get the id of toolbar and textview
        var text: TextView = findViewById(R.id.toolbar23text)
        var righttext: TextView = findViewById(R.id.toolbar24text)
        righttext.text = "Done"
        //set the text
        text.text = "Booking Details"


        righttext.setOnClickListener {
            var newTotalAmount: Double = 0.0
            var array = JSONArray()
            val addService = recyclerAdapter.getResult()

            if (addService.totalAmount == 0.0) {

                Toast.makeText(this, "Please select any service", Toast.LENGTH_SHORT).show()

            } else {
                newTotalAmount = addService.totalAmount


                array = addService.jsonArray
                mbinding.expandedProgressBar.visibility = View.VISIBLE

                // Log.e("Json Array", addService.toString())
                val sharedPreference = getSharedPreferences("Status", 0)
                val gson: Gson = Gson()
                val json1: String = sharedPreference.getString("data", "")
                val data_main: Pendingmodel = gson.fromJson(json1, Pendingmodel::class.java)

                var booking_id = data_main.booking_id
                var total_amount = data_main.total_amount.toDouble()
                Log.e("bookingId & totalAmount", "$booking_id $total_amount")
                total_amount += newTotalAmount

                Log.e("bookingId & totalAmount", "$booking_id $total_amount")

                var expandedViewmodel = ExpandedViewModel()
                expandedViewmodel.booking_id = booking_id
                expandedViewmodel.updated_amount = total_amount.toString()
                //expandedViewmodel.services = addService

                mvmodel1.getdata1(array, booking_id, total_amount.toString())
                mvmodel1.pendingSucessful1().observe(this, Observer {


                    Log.e("check expanded view", it.total_amount)
                    val json: String = gson.toJson(it)
                    var editor = sharedPreference.edit()
                    editor.clear()
                    editor.putString("data", json)
                    editor.putString("status", "5")
                    editor.commit()
                    mbinding.expandedProgressBar.visibility = View.INVISIBLE
                    Toast.makeText(this, "Added Successfully", Toast.LENGTH_LONG).show()
                    finish()
                })

                mvmodel1.pendingFailure().observe(this, Observer {
                    mbinding.expandedProgressBar.visibility = View.INVISIBLE
                    Toast.makeText(this, "Not Added", Toast.LENGTH_LONG).show()
                    Log.e("check expanded view", it)

                })
            }


        }
        setSupportActionBar(toolbar23)
        //enabled thebackbutton
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)


        /*//getting  the data from intent
        var data = intent.getParcelableExtra<ExpandedListModel>("keyname")
        var size = intent.getIntExtra("size",1)

        var name = data.category_name
        Log.e("data", data.category_name)


        for(i in 0 until data.modelcategories.size)
        {
            val data2 = data.modelcategories[i]
            expandableListTitle= listOf(data2.service_name)
            Log.e("expandableListTitle", expandableListTitle.toString())
        }

        println(expandableListTitle)

*/


        setAdapter()


    }

    override fun getdata(
        expandedListText: ModelSubCategory,
        isChecked: Boolean
    ) {
        if (checkedArray.size > 0) {
            var exist = 0
            for (i in 0 until checkedArray.size) {
                if (checkedArray[i].id == expandedListText.id) {
                    exist = 1

                }

            }
            if (exist == 0) {
                checkedArray.add(expandedListText)
            } else {
                checkedArray.add(expandedListText)
            }
        } else {
            for (i in 0 until checkedArray.size) {
                if (checkedArray[i].name == expandedListText.name) {
                    checkedArray.removeAt(i)
                    break
                }

            }


        }
    }

    private fun setAdapter() {
        /*      expandableListView = findViewById<View>(R.id.expandableListView) as ExpandableListView

      //        expandableListDetail = expandableListData.getData()
      //        expandableListTitle = ArrayList<String>(expandableListDetail!!.keys)
             *//* expandableListAdapter = CustomExpandableListAdapter(
            this,
            expandableListTitle,
            expandableListDetail
        )*//*
        expandableListAdapter = CustomExpandableListAdapter(this)
        expandableListView!!.setAdapter(expandableListAdapter)*/

        mbinding.recyclerParent.visibility = View.VISIBLE

        var manager = LinearLayoutManager(this)
        mbinding.recyclerParent.layoutManager = manager//set layout in recycler
        recyclerAdapter = ParentAdapter(
            this
        )
        recyclerParent.adapter = recyclerAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            if (item.itemId == android.R.id.home) {
                finish()
            }


        }
        return super.onOptionsItemSelected(item)
    }
}

