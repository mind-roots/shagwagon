package com.example.shagwagon.expanded_view_screen

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.R
import com.example.shagwagon.databinding.ListItemBinding

class ChildAdapter(
    var context: Context,
    listen:ChildListener
) : RecyclerView.Adapter<ChildAdapter.ViewHolder>() {

    private lateinit var parentModel: ExpandedListModel
    private var parentPos: Int = -1
    lateinit var mbinding: ListItemBinding
    var setCheck = listen
    var it = ArrayList<ExpandableInnerListModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return it.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        val data: ExpandableInnerListModel = it[position]

        holder.checkBox.text = data.service_name
        holder.price.text = "$"+data.price

        holder.checkBox.isChecked = data.checked

        holder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->

            data.checked = isChecked
            parentModel.modelcategories[position] = data

            setCheck.onServiceSelection(parentModel, parentPos)
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView.rootView) {

        var checkBox: CheckBox = itemView.findViewById(R.id.checkbox)
        var price: TextView = itemView.findViewById(R.id.expandedListItemPrice)


    }

    fun update(size: ExpandedListModel, parentPosition: Int) {
        parentModel = size
        it = size.modelcategories
        parentPos = parentPosition
        notifyDataSetChanged()
    }


    interface ChildListener{
        fun onServiceSelection(data: ExpandedListModel, parentPosition: Int)
    }

}