package com.example.shagwagon.expanded_view_screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class ExpandedViewmodelNew(application: Application) : AndroidViewModel(application)
{
    var mrepo = ExpandedRespository(application)

    fun pendingSucessful(): MutableLiveData<ArrayList<ExpandedListModel>> {

        return mrepo.pendingSucessful()
    }

    fun pendingFailure(): MutableLiveData<String> {

        return mrepo.pendingFailure()
    }

    fun getdata() {
        mrepo.getdata()
    }
}


