package com.example.shagwagon.expanded_view_screen

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shagwagon.R
import org.json.JSONArray
import org.json.JSONObject
import java.util.logging.Handler


class ParentAdapter(var context: Activity) : RecyclerView.Adapter<ParentAdapter.ViewHolder>(),
    ChildAdapter.ChildListener {


    var mData = ArrayList<ExpandedListModel>()
    lateinit var recyclerAdapter: ChildAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_group, parent, false))

    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data = mData[position]


        //send the data
        // holder.bind(data)
        holder.title.text = data.category_name
        Glide.with(context)
            .load(data.category_image)
            .into(holder.logo)
        var a = 0

        if (data.isExpanded) {
            holder.recyclerChild.visibility = View.VISIBLE
            holder.view.visibility = View.VISIBLE
            holder.title.setTextColor(context.resources.getColor(com.example.shagwagon.R.color.colorPrimaryDark))
            holder.upDown.setImageResource(R.drawable.ic_arrow_up_black)
        } else {
            holder.recyclerChild.visibility = View.GONE
            holder.view.visibility = View.GONE
            holder.title.setTextColor(context.resources.getColor(R.color.black))
            holder.upDown.setImageResource(R.drawable.ic_arrow_down_black)
        }
        recyclerAdapter = ChildAdapter(context, this)
        val manager = LinearLayoutManager(context)
        holder.recyclerChild.layoutManager = manager//set layout in recycler
        holder.recyclerChild.adapter = recyclerAdapter
        recyclerAdapter.update(data, position)

        holder.relativeLayout.setOnClickListener {
            mData[position].isExpanded = !data.isExpanded
            mData[position].isCollapsed = data.isCollapsed

            notifyDataSetChanged()

        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView.rootView) {
        var title: TextView = itemView.findViewById(R.id.listTitle)
        var recyclerChild: RecyclerView = itemView.findViewById(R.id.recyclerChild)
        var logo: ImageView = itemView.findViewById(R.id.logo)
        var view: View = itemView.findViewById(R.id.view)
        var upDown: ImageView = itemView.findViewById(R.id.upDown)
        var relativeLayout : RelativeLayout = itemView.findViewById(R.id.relativeLayoutGroup)

    }

    fun update(size: ArrayList<ExpandedListModel>) {
        mData = size
        notifyDataSetChanged()
    }


    override fun onServiceSelection(data: ExpandedListModel, parentPosition: Int) {
        mData[parentPosition] = data
        notifyDataSetChanged()
    }


    fun getResult(): ResultModel {

        val addServiceList = JSONArray()
        val resultModel = ResultModel()
        for (i in 0 until mData.size) {
            for (j in 0 until mData[i].modelcategories.size) {
                if (mData[i].modelcategories[j].checked) {
                    var addService: JSONObject = JSONObject()
                    addService.put("price", mData[i].modelcategories[j].price)
                    addService.put("service_id", mData[i].modelcategories[j].service_id)

                    resultModel.totalAmount += mData[i].modelcategories[j].price.toDouble()


                    addServiceList.put(addService)
                }

                resultModel.jsonArray = addServiceList


            }
        }
        return resultModel
    }
}