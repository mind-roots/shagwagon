package com.example.shagwagon.expanded_view_screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.pending_screen.Pendingmodel
import org.json.JSONArray

class ExpandedViewModel2(application: Application) : AndroidViewModel(application) {
    var mrepo = ExpandedViewRespository(application)

    fun pendingSucessful1(): MutableLiveData<Pendingmodel> {

        return mrepo.pendingSucessful()
    }

    fun pendingFailure(): MutableLiveData<String> {

        return mrepo.pendingFailure()
    }

    fun getdata1(service: JSONArray, booking_id:String, updated_amount:String) {
        mrepo.getdata(service,booking_id,updated_amount)
    }
}