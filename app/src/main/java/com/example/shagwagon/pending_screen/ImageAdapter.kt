package com.example.shagwagon.pending_screen

import android.media.Image
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import java.lang.Exception


//@BindingAdapter("setimage")
//
//fun setimage(image: ImageView, text: String) {
//
//    Picasso.get().load(text).into(image)
//}
@BindingAdapter("imageuser")
fun imageAdapter(view: ImageView, image:String) {
    //set the library
    try {
        Glide.with(view.context).load(image).into(view)
    }catch (e : Exception){

    }
}