package com.example.shagwagon.pending_screen

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.databinding.PendingAdapterBinding
import android.text.style.UnderlineSpan
import android.text.SpannableString
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.shagwagon.R
import com.example.shagwagon.boooking_details_googleMap.DirectionsJSONParser
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.start_pending_booking.BookingViewmodel
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.gson.Gson
import com.google.maps.DirectionsApi
import com.google.maps.GeoApiContext
import com.google.maps.model.DirectionsResult
import com.google.maps.model.TravelMode
import kotlinx.android.synthetic.main.pending_adapter.view.*
import org.joda.time.DateTime
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.URL
import java.util.HashMap
import java.util.concurrent.TimeUnit
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.sin


class PendingAdapter(
    var context: FragmentActivity,
    pendingFragment: Pending_Fragment
) :
    RecyclerView.Adapter<PendingAdapter.ViewHolder>(), OnMapReadyCallback,
    GoogleApiClient.ConnectionCallbacks {
    override fun onConnectionSuspended(p0: Int) {
    }


    var it = ArrayList<Pendingmodel>()

    var interfaceAddress = pendingFragment as setdataInterface
    lateinit var mbinding: PendingAdapterBinding
    private lateinit var latitude: String
    private lateinit var longitude: String
    lateinit var mvmodel: BookingViewmodel
    private var overview = 0
    private var googleMap: GoogleMap? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var longitudesource: Double = 28.5383
    private var latitudesource: Double = -81.3792
    private var longitudedestination: String = "28.5383"
    private var latitudedestination: String = "-81.3792"
    private var resultDestination: String = ""
    var resultSource: String = "123"
    lateinit var results: DirectionsResult
    internal var markerPoints: java.util.ArrayList<LatLng> = java.util.ArrayList()
    var direction = DirectionsJSONParser(context)
    var dialogRun = 0

    //new mthod
    // val bus = Bus(ThreadEnforcer.MAIN)
    //set the click


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        mbinding = PendingAdapterBinding.inflate(LayoutInflater.from(context), parent, false)
        mvmodel = ViewModelProviders.of(context).get(BookingViewmodel::class.java)

        var a = context!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
        var b = a.getString("status", "")



        if (b == "5" || b == "4") {
            mbinding.pendingApi.visibility = View.GONE
        }


        /*val udata = "Total: _____"
        val text = SpannableString(udata)
        mbinding.textView16.text = text*/


        // bus.register(this)
        return ViewHolder(mbinding)

    }

    override fun getItemCount(): Int {
        return it.size
    }

    fun getGeoContext(): GeoApiContext? {
        val geoApiContext = GeoApiContext()
        return geoApiContext
            .setQueryRateLimit(3)
            .setApiKey(context.getString(R.string.google_maps_key))
            .setConnectTimeout(1, TimeUnit.SECONDS)
            .setReadTimeout(1, TimeUnit.SECONDS)
            .setWriteTimeout(1, TimeUnit.SECONDS)
    }


    fun getDirectionsDetails(
        origin: String,
        destination: String,
        mode: TravelMode
    ): DirectionsResult {
        val now = DateTime()
        var a = DirectionsResult()
        try {
            a = DirectionsApi.newRequest(getGeoContext())
                .mode(mode)
                .origin(origin)
                .destination(destination)
                .departureTime(now)
                .await()
        } catch (e: ApiException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        return a


    }


    fun getEndLocationTitle(results: DirectionsResult): String? {

        var timeRemaining = results.routes[overview].legs[overview].duration.humanReadable
        var a = timeRemaining.replace("hours", "h")
        var b = a.replace("mins", "m")
        //mbinding.textView12.text = b

        return "Time :" + results.routes[overview].legs[overview].duration.humanReadable

    }

    fun addMarkersToMap(results: DirectionsResult, googleMap: GoogleMap?) {
        googleMap!!.addMarker(
            MarkerOptions().position(
                LatLng(
                    results.routes[overview].legs[overview].startLocation.lat,
                    results.routes[overview].legs[overview].startLocation.lng
                )
            ).title(results.routes[overview].legs[overview].startAddress)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location))
        )
        googleMap.addMarker(
            MarkerOptions().position(
                LatLng(
                    results.routes[overview].legs[overview].endLocation.lat,
                    results.routes[overview].legs[overview].endLocation.lng
                )
            ).title(results.routes[overview].legs[overview].endAddress).snippet(
                getEndLocationTitle(
                    results
                )
            )

        )
    }


    @SuppressLint("MissingPermission")
    fun setupGoogleMapScreenSettings(googleMap: GoogleMap?) {
        googleMap?.isBuildingsEnabled = true
        googleMap?.isIndoorEnabled = true
        googleMap?.isTrafficEnabled = true
        try {
            googleMap?.isMyLocationEnabled = true
        } catch (e: Exception) {

        }
        val mUiSettings = googleMap?.uiSettings
        //mUiSettings?.isZoomControlsEnabled = true
        mUiSettings?.isCompassEnabled = true
        mUiSettings?.isMyLocationButtonEnabled = true
        mUiSettings?.isMyLocationButtonEnabled = true
        mUiSettings?.isScrollGesturesEnabled = true
        mUiSettings?.isZoomGesturesEnabled = true
        mUiSettings?.isTiltGesturesEnabled = true
        mUiSettings?.isRotateGesturesEnabled = true
        mUiSettings?.isMapToolbarEnabled = true
        mUiSettings?.isMapToolbarEnabled = true
    }


    override fun onMapReady(map: GoogleMap?) {
        googleMap = map
        setupGoogleMapScreenSettings(googleMap)
        //googleMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
        googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
        try {
            if (ActivityCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context!!, Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // ActivityCompat.requestPermissions(getApplication(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 123);

                fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                    if (location != null) {
                        latitudesource = location.latitude
                        longitudesource = location.longitude
                        setMap(latitudedestination, longitudedestination)

                    } else {
                        try {
                            var shared =
                                context!!.getSharedPreferences("Location", Context.MODE_PRIVATE)
                            latitudesource = shared.getString("latitude", "").toDouble()
                            longitudesource = shared.getString("longitude", "").toDouble()
                            setMap(latitudedestination, longitudedestination)
                        } catch (e: Exception) {

                            Toast.makeText(
                                context,
                                "Your Location Not found please Enable Gps",
                                Toast.LENGTH_LONG
                            )
                                .show()
                            e.printStackTrace()

                        }


                    }
                }
            } else {
                fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                    if (location != null) {
                        latitudesource = location.latitude
                        longitudesource = location.longitude
                        setMap(latitudedestination, longitudedestination)


                    } else {
                        try {
                            var shared =
                                context!!.getSharedPreferences("Location", Context.MODE_PRIVATE)
                            latitudesource = shared.getString("latitude", "").toDouble()
                            longitudesource = shared.getString("longitude", "").toDouble()
                            setMap(latitudedestination, longitudedestination)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Toast.makeText(
                                context,
                                "Your Location Not found please Enable Gps",
                                Toast.LENGTH_LONG
                            )
                                .show()


                        }


                    }
                }


            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun setMap(latitudedestination: String, longitudedestination: String) {
        val lat1: Double?
        val longitude1: Double?
        val lat2: Double?
        val longitude2: Double?


        if (latitudesource != 28.5383 && longitudesource != -81.3792) {
            lat1 = latitudesource
            longitude1 = longitudesource

            lat2 = latitudedestination.toDouble()
            longitude2 = longitudedestination.toDouble()

            val latLng = LatLng(lat1, longitude1)
            val latLng1 = LatLng(lat2, longitude2)

            markerPoints.add(latLng)
            markerPoints.add(latLng1)
            val options = MarkerOptions()
            options.position(latLng1)
            /*if (markerPoints.size == 1) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
            } else if (markerPoints.size == 2) {
                //options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            }*/

            googleMap?.addMarker(options)

            if (markerPoints.size >= 2) {
                val origin = markerPoints[0] as LatLng
                val dest = markerPoints[1] as LatLng

                // Getting URL to the Google Directions API
                val url = getDirectionsUrl(origin, dest)



                direction.isNetworkConnected(context)


                if (direction.isNetworkConnected(context)) {

                    val downloadTask = DownloadTask()

                    // Start downloading json data from Google Directions API
                    downloadTask.execute(url)
                } else {
                    Toast.makeText(context, "No Network Available", Toast.LENGTH_LONG)
                }
                getEndLocationTitle(googleMap)
            }
            markerPoints.clear()
        }
    }

    private fun getEndLocationTitle(map: GoogleMap?) {
        var shared = context!!.getSharedPreferences("Map", Context.MODE_PRIVATE)
        var address = shared.getString("address", "")

        var latLng = LatLng(latitudedestination.toDouble(), longitudedestination.toDouble())

        if (address != "") {

            map?.addMarker(
                MarkerOptions().position(latLng)
                    .title(address)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            )
        }


    }

    private inner class DownloadTask : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg url: String): String {

            var data = ""

            try {
                data = downloadUrl(url[0])
            } catch (e: Exception) {
                Log.d("Background Task", e.toString())
            }



            return data
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            val parserTask = ParserTask()
            parserTask.execute(result)
        }
    }


    private inner class ParserTask : AsyncTask<String, Int, List<List<HashMap<String, String>>>>() {

        // Parsing the data in non-ui thread
        override fun doInBackground(vararg jsonData: String): List<List<HashMap<String, String>>>? {

            val jObject: JSONObject
            var routes: List<List<HashMap<String, String>>>? = null

            try {
                jObject = JSONObject(jsonData[0])
                val parser = DirectionsJSONParser(context)

                routes = parser.parse(jObject)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return routes
        }

        override fun onPostExecute(result: List<List<HashMap<String, String>>>) {
            var points: java.util.ArrayList<LatLng>
            var lat: Double = 28.5383
            var lng: Double = -81.3792
            var lineOptions: PolylineOptions? = null
            val markerOptions = MarkerOptions()

            for (i in result.indices) {

                points = java.util.ArrayList()
                lineOptions = PolylineOptions()

                var path = result[i]

                for (j in path.indices) {
                    val point = path[j]

                    lat = java.lang.Double.parseDouble(point["lat"]!!)
                    lng = java.lang.Double.parseDouble(point["lng"]!!)
                    val position = LatLng(lat, lng)

                    points.add(position)
                }

                lineOptions.addAll(points)
                lineOptions.width(12f)
                lineOptions.color(Color.RED)
                lineOptions.geodesic(true)


            }


            if (lineOptions != null) {
                googleMap?.addPolyline(lineOptions)
                googleMap?.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            lat, lng
                        ), 12F
                    )
                )
                /*  //mbinding.progressBarMap.visibility = View.INVISIBLE
                  var shared = context!!.getSharedPreferences("Map", Context.MODE_PRIVATE)
                  var time = shared.getString("time", "")

                  *//*if (time != "") {
                    var a = time.replace("hours", "h")
                    var b = a.replace("mins", "m")
                    mbinding.textView12.text = b
                    mbinding.textView12.text = time
                } else {
                    mbinding.textView12.text = "Time"
                }*/

            } else {

                googleMap?.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            lat, lng
                        ), 7F
                    )
                )

                if (dialogRun == 0) {
                    dialogRun = 1
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("No route exist")
                    builder.setPositiveButton("Ok")
                    { dialog, which ->
                        dialog.dismiss()
                        // Close all activites

//                    finish()
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }

            // Drawing polyline in the Google Map for the i-th route


        }
    }

    private fun getDirectionsUrl(origin: LatLng, dest: LatLng): String {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude

        // Sensor enabled
        val sensor = "sensor=false"
        val mode = "mode=driving"
        val key = "key=AIzaSyCL6OAjSuN_k848r8KRC_VHJT4ft9N2rus"
        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor&$mode&$key"

        // Output format
        val output = "json"

        // Building the url to the web service


        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters"
    }

    /**
     * A method to download json data from url
     */
    @Throws(IOException::class)
    private fun downloadUrl(strUrl: String): String {
        var data = ""
        var iStream: InputStream? = null
        var urlConnection: HttpURLConnection? = null
        try {
            val url = URL(strUrl)

            urlConnection = url.openConnection() as HttpURLConnection

            urlConnection.connect()

            iStream = urlConnection.inputStream

            val br = BufferedReader(InputStreamReader(iStream))

            val sb = StringBuffer()

//            var line = ""
//            while ((line = br.readLine()) != null) {
//                sb.append(line)
//            }


            data = direction.readData(br)

            br.close()

        } catch (e: Exception) {
            Log.d("Exception", e.toString())
        } finally {
            iStream!!.close()
            urlConnection!!.disconnect()
        }
        return data
    }


    override fun onConnected(bundle: Bundle?) {

        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        }
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data: Pendingmodel = Pendingmodel()
        if (it.size == 0) {


        } else {
            data = it[position]

        }

        val time =
            Constants.parseTime(data.booking_time, Constants.SERVER_TIME_RESOURCES_FORMAT, Constants.LOCAL_TIME_FORMAT)
        mbinding.bookingTimedate.text = data.booking_date + " " + time


        //send the data
        holder.bind(data)

        if (data.profile_image.isEmpty()) {
            Glide.with(context)
                .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                .into(mbinding.imageView5)
        } else {
            Glide.with(context)
                .load(data.profile_image)
                .into(mbinding.imageView5)
        }



        latitudedestination = data.latitude
        longitudedestination = data.longitude


        val geocoder = Geocoder(context)
        var address: Address?
        try {
            // 2
            val addresses =
                geocoder.getFromLocation(
                    latitudedestination.toDouble(),
                    longitudedestination.toDouble(),
                    1
                )
            address = addresses[0]
            Log.e("Map Fragment ", address.toString())
            val builder = StringBuilder()
            builder.append(address?.featureName).append(",\t")
            builder.append(address?.getAddressLine(0)).append(",\t")
            resultDestination = builder.toString()
            Log.e("Map Fragment2 ", resultDestination)
        } catch (e: Exception) {
            Log.e("MapsActivity", e.localizedMessage)
        }


        try {
            var shared = context!!.getSharedPreferences("Location", Context.MODE_PRIVATE)
            var latitude_source = shared.getString("latitude", "")
            var longitude_source = shared.getString("longitude", "")
            if (latitude_source != "" && longitude_source != "") {

                var distanceResult = distance(
                    latitude_source.toDouble(),
                    longitude_source.toDouble(),
                    latitudedestination.toDouble(),
                    longitudedestination.toDouble()
                )
                mbinding.textView12.text = String.format("%.2f", distanceResult) + "km"
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


//set the click on the bimnding
//        holder.itemView.setOnClickListener {
//
//
//            val intent = Intent(context, PendingBookingActivity::class.java)
//            intent.putExtra("AllData", data)
//
//            context!!.startActivity(intent)
//        }


        var a = context!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
        var b = a.getString("status", "")



        if (b == "5" || b == "4") {
            holder.itemView.pendingApi.visibility = View.GONE
        }
        holder.itemView.textView25.setOnClickListener {
            val mDialog = androidx.appcompat.app.AlertDialog.Builder(context)
            mDialog.setMessage("Do you want to call")
            mDialog.setCancelable(true)

            //yes button set the api of alertv dialog
            mDialog.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                val intent = Intent(Intent.ACTION_DIAL)
                val number = data.customer_phone
                intent.data = Uri.parse("tel:$number")
                context.startActivity(intent)
            })

            mDialog.setNegativeButton("No", DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
            })
            mDialog.show()
        }

        holder.itemView.openMap.setOnClickListener {
            var dialog: Dialog = Dialog(context)
            dialogRun = 0
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            latitudedestination = data.latitude
            longitudedestination = data.longitude
            dialog.setContentView(R.layout.map_dialog)
            dialog.show()



            var progressBar = dialog.findViewById<ProgressBar>(R.id.progressBar)
            //progressBar.visibility = View.VISIBLE
            var googleMap: GoogleMap
            var mapView = dialog.findViewById<MapView>(R.id.map)

            MapsInitializer.initialize(context)


            var close_button = dialog.findViewById<ImageView>(R.id.close_button)
            close_button.setOnClickListener {
                dialog.dismiss()
            }

            mapView.onCreate(dialog.onSaveInstanceState())
            mapView.onResume()
            mapView.getMapAsync(this)
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
            setMap(latitudedestination, longitudedestination)
            progressBar.visibility = View.INVISIBLE
        }



        holder.itemView.pendingApi.setOnClickListener {

            var bundle = Bundle()
            var abc = bundle.getString("ifcheck")


//            holder.itemView.imageView6.visibility = View.INVISIBLE
            val booking_id = data.booking_id
            mvmodel.SetActiveBooking(booking_id, "4")
            val sharedPreference = context.getSharedPreferences("Status", Context.MODE_PRIVATE)
            val gson: Gson = Gson()
            val json: String = gson.toJson(data)
            var editor = sharedPreference.edit()
            editor.putString("data", json)
            editor.putString("status", "4")
            editor.commit()
            interfaceAddress.setadapterData(position, data, abc)
        }

//
//..............................inner adapter set.................................................//

        var manager = LinearLayoutManager(context!!)

        mbinding.innerrecyclerview.layoutManager = manager
        mbinding.innerrecyclerview.adapter = PendingInnerAdapter(context!!, data)
        //...........convert the address.....................//
        latitude = data.latitude
        longitude = data.longitude
//method of converting the adress
        getAddress()


    }


    class ViewHolder(var mbinding: PendingAdapterBinding) : RecyclerView.ViewHolder(mbinding.root) {
        fun bind(data: Pendingmodel) {

            mbinding.data = data
            mbinding.executePendingBindings()
            val Booking_type = data.booking_type
            if (Booking_type == "2") {
                //set the underline text

                val udata = "SCHEDULED"
                val text = SpannableString(udata)
                //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
                mbinding.textView14.text = text
            } else if (Booking_type == "3") {
                //set the underline text
                val udata = "SUBSCRIBED"
                val text = SpannableString(udata)
                //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
                mbinding.textView14.text = text
            } else {

                //set the underline text
                val udata = "INSTANT BOOKING"
                val text = SpannableString(udata)
                //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
                mbinding.textView14.text = text
            }


        }


    }


    //geo coading
    private fun getAddress(): String? {
        // 1
        val geocoder = Geocoder(context)
        val address: Address?

        try {
            // 2
            val addresses = geocoder.getFromLocation(latitude.toDouble(), longitude.toDouble(), 1)
            address = addresses[0]
            val builder = StringBuilder()
//            builder.append(address.featureName).append(",\t")
//            builder.append(address.thoroughfare).append(",\t")
            builder.append(address.subAdminArea).append("  ")
            builder.append(address.countryName)
            var result = builder.toString()
            mbinding.textView11.text = result
        } catch (e: java.lang.Exception) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return null
    }

    //update the data in the arraylist
    fun update(size: ArrayList<Pendingmodel>) {

        it = size
        notifyDataSetChanged()

    }

    //click set
    interface setdataInterface {

        fun setadapterData(
            position: Int,
            data: Pendingmodel,
            abc: String?
        )

    }


    fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        var theta: Double = lon1 - lon2
        var dist: Double =
            sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(
                deg2rad(theta)
            );
        dist = acos(dist)
        dist = rad2deg(dist)
        dist *= 60 * 1.1515 * 1.609
        return (dist)
    }

    private fun rad2deg(rad: Double): Double {
        return (rad * 180.0 / Math.PI)

    }

    fun deg2rad(deg: Double): Double {
        return (deg * Math.PI / 180.0)
    }


}
