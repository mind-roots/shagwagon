package com.example.shagwagon.pending_screen

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shagwagon.completed_screen.Completed_Screen_Fragment
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.databinding.FragmentPendingBinding
import com.example.shagwagon.home_screen.HomeFragment
import com.example.shagwagon.sign_in.Sign_in_Activity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_pending_.*


class Pending_Fragment : Fragment(), PendingAdapter.setdataInterface {

    lateinit var interfaceAddress:setdataInterface1


    private lateinit var recyclerAdapter: PendingAdapter
    // , PendingAdapter.setdataInterface
    private val Request_Code = 1
    lateinit var mbinding: FragmentPendingBinding
    lateinit var mvmodel: PendingViewmodel
    lateinit var inerface: getDataInterface
    var abc: String = "123"
    var a: ArrayList<Pendingmodel> = ArrayList()
    lateinit var update: pendingData


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mbinding = FragmentPendingBinding.inflate(LayoutInflater.from(context), container, false)
        mvmodel = ViewModelProviders.of(this).get(PendingViewmodel::class.java)


        //return inflater.inflate(R.layout.fragment_pending_, container, false)
        interfaceAddress = parentFragment as setdataInterface1
        return mbinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //...........using Api to Send the data........................//
        setAdapter()


        a = update.getPendingData()

        if (a.size == 0) {
            mbinding.shimmerlayout.startShimmerAnimation()
            mbinding.shimmerlayout.visibility = View.VISIBLE
            mvmodel.getdata()
        } else {
            mbinding.shimmerlayout.stopShimmerAnimation()
            mbinding.shimmerlayout.visibility = View.INVISIBLE
            recyclerAdapter.update(a)
        }
        mbinding.noview.text = "No pending bookings yet"
//sucessful case
        //........................Api observer..........................//
        mvmodel.pendingSucessful().observe(this, Observer {

            //set the layout

            a = it

            update.sendingPendingData(a)

//            var manager = LinearLayoutManager(context)
//            mbinding.recyclerpending.layoutManager = manager

            //  mbinding.recyclerpending.adapter = PendingAdapter(context, it)
            if (it.size > 0) {

                mbinding.shimmerlayout.stopShimmerAnimation()
                mbinding.shimmerlayout.visibility = View.GONE
                mbinding.recyclerpending.visibility = View.VISIBLE
                mbinding.noview.visibility = View.GONE
                mbinding.imageView.visibility = View.GONE
                recyclerAdapter.update(it)

            } else {
                mbinding.shimmerlayout.stopShimmerAnimation()
                mbinding.shimmerlayout.visibility = View.VISIBLE
                mbinding.recyclerpending.visibility = View.VISIBLE
                mbinding.noview.visibility = View.VISIBLE
                mbinding.imageView.visibility = View.VISIBLE


            }


        })

        // failure case

        mvmodel.pendingFailure().observe(this, Observer {



            if(it.contentEquals("Auth code has been expired.")){
                Toast.makeText(activity, " Logout successfully", Toast.LENGTH_SHORT).show()


                val edit = Constants.getPrefs(activity!!).edit()
                edit.clear()
                edit.apply()

                val i = Intent(activity, Sign_in_Activity::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
                activity?.finish()
            }
            else{
                mbinding.shimmerlayout.stopShimmerAnimation()
                mbinding.shimmerlayout.visibility = View.GONE
                mbinding.layoutprnding.visibility = View.VISIBLE
                mbinding.recyclerpending.visibility = View.VISIBLE
                mbinding.noview.visibility = View.VISIBLE
                mbinding.imageView.visibility = View.VISIBLE

                //Toast.makeText(context!!, it, Toast.LENGTH_SHORT).show()
            }
            //mbinding.shimmerlayout.startShimmerAnimation()


        })


        //..................................PENDING API............................//


    }

    override fun setadapterData(
        position: Int,
        data: Pendingmodel,
        abc: String?
    ) {


        val sharedPreference = context!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
        val gson: Gson = Gson()
        var json: String = sharedPreference.getString("data", "")

        if (json.isNotEmpty()) {
            val data_stored: Pendingmodel = gson.fromJson(json, Pendingmodel::class.java)
            //Log.e("Shared Preference data",data_stored.total_amount)
            interfaceAddress.setadapterData1(position, data_stored, abc)
        } else {
            interfaceAddress.setadapterData1(position, data, abc)

        }

        /* val intent = Intent(context, PendingBookingActivity::class.java)
         intent.putExtra("AllData", data)
         startActivityForResult(intent, Request_Code)*/

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Request_Code && resultCode == Activity.RESULT_OK) {

            val data = data!!.getParcelableExtra<Pendingmodel>("Key")
//set the data in the interface
            inerface.datainterface(data)
        }
    }

    //set adapter
    fun setAdapter() {

        var manager = LinearLayoutManager(context)
        recyclerpending.layoutManager = manager//set layout in recycler
        recyclerAdapter = PendingAdapter(
            activity!!,
            this@Pending_Fragment
        )
        //initilize or creating the object  adapter or set the context
        recyclerpending.adapter = recyclerAdapter//set adapter in the recycler

    }
    //interface set method ......//

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as pendingData

    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as pendingData


        //  inerface = activity as getDataInterface
    }

    //set interface
    interface getDataInterface {
        fun datainterface(data: Pendingmodel)

    }

    interface setdataInterface1 {


        fun setadapterData1(position: Int, data: Pendingmodel, abc: String?)

    }

    interface pendingData {
        fun sendingPendingData(pendingmodel: ArrayList<Pendingmodel>)
        fun getPendingData(): ArrayList<Pendingmodel>


    }


}

