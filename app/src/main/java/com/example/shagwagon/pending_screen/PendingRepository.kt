package com.example.shagwagon.pending_screen

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import com.example.shagwagon.sign_in.ErrorModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class PendingRepository(var application: Application) {


    private var msucessful = MutableLiveData<ArrayList<Pendingmodel>>()
    val modelarray = ArrayList<Pendingmodel>()
    val mfailure = MutableLiveData<String>()


    fun getdata() {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()

        val auth = Constants.getPrefs(application).getString(Constants.token, "")


        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.pendingBookings(auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {


                if (response.isSuccessful) {
                    val resp = response.body()!!.string()
                    try {
                        val json = JSONObject(resp)
                        val status = json.optString("status")
                        val msg1 = json.optString("msg")
                        if (status == "true") {
                            val data = json.optJSONArray("data")


                            for (i in 0 until data.length()) {
                                val model = Pendingmodel()//  outer model

                                val arrayobject1 = data.optJSONObject(i)
                                model.booking_id = arrayobject1.optString("booking_id")
                                model.booking_type = arrayobject1.optString("booking_type")
                                model.booking_date = arrayobject1.optString("booking_date")
                                model.booking_time = arrayobject1.optString("booking_time")
                                model.latitude = arrayobject1.optString("latitude")
                                model.longitude = arrayobject1.optString("longitude")
                                model.total_amount = arrayobject1.optString("total_amount")
                                model.customer_name = arrayobject1.optString("customer_name")
                                model.customer_rating = arrayobject1.optString("customer_rating")
                                model.profile_image = arrayobject1.optString("profile_image")
                                model.driver_name = arrayobject1.optString("driver_name")
                                model.driver_rating = arrayobject1.optString("driver_rating")
                                model.driver_phone = arrayobject1.optString("driver_phone")
                                model.customer_id = arrayobject1.optString("customer_id")
                                model.driver_id = arrayobject1.optString("driver_id")
                                model.driver_image = arrayobject1.optString("driver_image")
                                model.customer_phone = arrayobject1.optString("customer_phone")
                                modelarray.add(model)

                                val data2 = arrayobject1.optJSONArray("booked_services")

                                var internalmodel = ArrayList<InnerModel>()
                                for (i in 0 until data2.length()) {

                                    val innermodel = InnerModel()

                                    val arrayobject2 = data2.optJSONObject(i)
                                    innermodel.service_id = arrayobject2.optString("service_id")
                                    innermodel.price = arrayobject2.optString("price")
                                    innermodel.name = arrayobject2.optString("name")
                                    internalmodel.add(innermodel)
                                    model.modelcategories = internalmodel//add the inner model in outer model

                                    // modelarray.add(model)
                                    msucessful.value = modelarray

                                }
                            }
                        } else {

                            val msgAuth = msg1

                            if(msgAuth.contentEquals("Auth code has been expired.")){
                                mfailure.value=msgAuth
                            }
                            else{
                                val msg = "Sorry No Data Found"
                                Log.e("Message Pending", msg)

                                mfailure.value = msg
                            }


                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }


            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                mfailure.value = msg

            }


        })
    }


    //sucessful
    fun pendingSucessful(): MutableLiveData<ArrayList<Pendingmodel>> {

        return msucessful
    }

    //failure
    fun pendingFailure(): MutableLiveData<String> {

        return mfailure
    }
}


