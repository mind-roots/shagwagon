package com.example.shagwagon.pending_screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.facebook.shimmer.ShimmerFrameLayout

class PendingViewmodel(application: Application) : AndroidViewModel(application) {

    var mrepo = PendingRepository(application)


    fun pendingSucessful(): MutableLiveData<ArrayList<Pendingmodel>> {

        return mrepo.pendingSucessful()
    }


    fun pendingFailure(): MutableLiveData<String> {

        return mrepo.pendingFailure()
    }

    fun getdata() {
        mrepo.getdata()
    }

}
