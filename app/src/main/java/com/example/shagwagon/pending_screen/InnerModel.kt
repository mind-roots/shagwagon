package com.example.shagwagon.pending_screen

import android.os.Parcel
import android.os.Parcelable

class InnerModel() : Parcelable {

    var service_id:String=" "
    var price:String=" "
    var name:String=" "

    constructor(parcel: Parcel) : this() {
        service_id = parcel.readString()
        price = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(service_id)
        parcel.writeString(price)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<InnerModel> {
        override fun createFromParcel(parcel: Parcel): InnerModel {
            return InnerModel(parcel)
        }

        override fun newArray(size: Int): Array<InnerModel?> {
            return arrayOfNulls(size)
        }
    }
}