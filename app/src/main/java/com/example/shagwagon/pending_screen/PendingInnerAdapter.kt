package com.example.shagwagon.pending_screen

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.databinding.PendingInnerAdapterBinding

class PendingInnerAdapter(var context: Context, var data: Pendingmodel) :
    RecyclerView.Adapter<PendingInnerAdapter.ViewHolder>() {
    lateinit var mbinding: PendingInnerAdapterBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mbinding = PendingInnerAdapterBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(mbinding)

    }

    override fun getItemCount(): Int {

        return data.modelcategories!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val data = data.modelcategories!![position]

        holder.bind(data)



    }


    class ViewHolder(var mbinding: PendingInnerAdapterBinding) : RecyclerView.ViewHolder(mbinding.root) {
        fun bind(data: InnerModel) {

            mbinding.data = data

            mbinding.executePendingBindings()


        }

    }

}
