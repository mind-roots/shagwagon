package com.example.shagwagon.pending_screen

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable

class Pendingmodel(): Parcelable {


    var booking_id: String = " "
    var booking_type: String = " "
    var booking_date: String = " "
    var booking_time: String = " "
    var latitude: String = " "
    var longitude: String = " "
    var total_amount: String = " "
    var customer_id:String=""
    var customer_name: String = " "
    var customer_phone:String=" "
    var customer_rating: String = " "
    var profile_image: String = " "
    var driver_name: String = " "
    var driver_rating: String = " "
    var driver_phone: String = " "
    var created_at:String=" "

    var status: String = " "
    var driver_id:String=""
    var driver_image:String=""
    var statuss:String=""
    var modelcategories: List<InnerModel> = ArrayList()

    constructor(parcel: Parcel) : this() {
        booking_id = parcel.readString()
        booking_type = parcel.readString()
        booking_date = parcel.readString()
        booking_time = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        total_amount = parcel.readString()
        customer_name = parcel.readString()
        customer_rating = parcel.readString()
        profile_image = parcel.readString()
        driver_name = parcel.readString()
        driver_rating = parcel.readString()
        driver_phone = parcel.readString()
        created_at = parcel.readString()
        status = parcel.readString()
        driver_id = parcel.readString()
        statuss = parcel.readString()
        customer_id = parcel.readString()
        customer_phone = parcel.readString()
        driver_image = parcel.readString()
        modelcategories = parcel.createTypedArrayList(InnerModel)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(booking_id)
        parcel.writeString(booking_type)
        parcel.writeString(booking_date)
        parcel.writeString(booking_time)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(total_amount)
        parcel.writeString(customer_name)
        parcel.writeString(customer_rating)
        parcel.writeString(profile_image)
        parcel.writeString(driver_name)
        parcel.writeString(driver_rating)
        parcel.writeString(driver_phone)
        parcel.writeString(created_at)
        parcel.writeString(status)
        parcel.writeString(driver_id)
        parcel.writeString(statuss)
        parcel.writeTypedList(modelcategories)
        parcel.writeString(customer_id)
        parcel.writeString(customer_phone)
        parcel.writeString(driver_image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pendingmodel> {
        override fun createFromParcel(parcel: Parcel): Pendingmodel {
            return Pendingmodel(parcel)
        }

        override fun newArray(size: Int): Array<Pendingmodel?> {
            return arrayOfNulls(size)
        }
    }


}
