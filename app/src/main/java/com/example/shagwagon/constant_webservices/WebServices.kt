package com.example.shagwagon.constant_webservices

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*

interface WebServices {

    /* -- Login */
    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("device_id") device_id: String,
        @Field("device_type") device_type: String,
        @Field("re_login") re_login:String

    ): Call<ResponseBody>

    /* -- Logout Api */
    @FormUrlEncoded
    @POST("logout")
    fun logout(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    /* -- googleMap Api */
    @FormUrlEncoded
    @POST("update_driver_location")
    fun location(
        @Field("auth_code") auth_code: String,
        @Field("latitude") latitude: String,
        @Field("longitude") longitude: String
    ): Call<ResponseBody>

    /* -- change_profile_image*/
//    @FormUrlEncoded
//    @POST("change_profile_image")
//    fun changeProfile(
//        @Field("auth_code")auth_code:String,
//        @Field(" file")file:String
//
//    ):Call<ResponseBody>
    /* -- change_profile_image*/

    @Multipart
    @POST("change_profile_image")
    fun changeProfile(
        @Part("auth_code") auth_code: RequestBody,
        @Part file: MultipartBody.Part,
        @Part("type") type: RequestBody
    ): Call<ResponseBody>


    /* -- forgotpassword */
    @FormUrlEncoded
    @POST("forgotpassword")
    fun forgotpassword(
        @Field("email") email: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    /* -- Delete Acount */
    @FormUrlEncoded
    @POST("deleteaccount")
    fun deleteaccount(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    /* -- updateprofile  */
    @FormUrlEncoded
    @POST("updateprofile")
    fun updateprofile(
        @Field("auth_code") auth_code: String,
        @Field("name") name: String,
        @Field("password") password: String,
        @Field("contact_number") contact_number: String,
        @Field("type") type: String

    ): Call<ResponseBody>


    /* -- pendingBookings  */
    @FormUrlEncoded
    @POST("pendingBookings")
    fun pendingBookings(
        @Field("auth_code") auth_code: String

    ): Call<ResponseBody>

    /* -- completedBookings  */
    @FormUrlEncoded
    @POST("completedBookings")
    fun completedBookings(
        @Field("auth_code") auth_code: String,
        @Field("limit") limit: String,
        @Field(" offset") offset: String

    ): Call<ResponseBody>


    /* -- activeBooking  */
    @FormUrlEncoded
    @POST("updateBookingStatus")
    fun updateBookingStatus(
        @Field("auth_code") auth_code: String,
        @Field(" booking_id") booking_id: String,
        @Field("status") status:String,
        @Field("type") type: String

    ): Call<ResponseBody>

    @GET("getservices")
    fun getServices():Call<ResponseBody>

    @FormUrlEncoded
    @POST("MarkasComplete")
    fun MarkasComplete(
        @Field("auth_code") auth_code: String,
        @Field("booking_id")booking_id: String
    ):Call<ResponseBody>

    @FormUrlEncoded
    @POST("addextraService")
    fun AddExtraService(
        @Field("auth_code") auth_code : String,
        @Field("services") services : JSONArray,
        @Field("booking_id") booking_id: String,
        @Field("updated_amount") updated_amount : String
    ):Call<ResponseBody>

    @FormUrlEncoded
    @POST("giverating")
    fun GiveRating(
        @Field("auth_code") auth_code: String,
        @Field("booking_id") booking_id: String,
        @Field("customer_id") customer_id: String,
        @Field("driver_id") driver_id: String,
        @Field("rating") rating:String,
        @Field("type") type:String
        ):Call<ResponseBody>

    @FormUrlEncoded
    @POST("changepassword")
    fun ChangePassword(
        @Field("auth_code") auth_code: String,
        @Field("old_password") old_password : String,
        @Field("new_password") new_password : String,
        @Field("type") type:String
    ):Call<ResponseBody>

    @FormUrlEncoded
    @POST("driver_in_out_time")
    fun DriveInandOut(
        @Field("auth_code") auth_code: String,
        @Field("clock_in") clock_in:String,
        @Field("clock_out") clock_out:String
    ):Call<ResponseBody>


    @FormUrlEncoded
    @POST("register_device")
    fun RegisterDevice(
        @Field("auth_code") auth_code: String,
        @Field("device_id") device_id: String,
        @Field("device_type") device_type: String,
        @Field("type") type: String
    ):Call<ResponseBody>

    @FormUrlEncoded
    @POST("CheckLastBookingStatus")
    fun CheckLastBookingStatus(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String
    ):Call<ResponseBody>

}