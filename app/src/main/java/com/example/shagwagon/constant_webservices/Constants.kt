package com.example.shagwagon.constant_webservices

import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.view.inputmethod.InputMethodManager
import retrofit2.Retrofit
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Constants {
    companion object {

        val LOCAL_TIME_FORMAT: String = "hh:mm a"
        val data: String = "data"
        //token is used for storing the authcode
        val token: String = "token"
        val profile: String = "username"
        const val BASE_URL = "https://www.shagwagon.com/dev/api/Data_v1/"
        // const val BASE_URL = "http://cloudart.com.au/projects/shagwagon/api/Data_v1/"
        // const val BASE_URL = "https://www.shagwagon.com/api/Data_v1/"
        val SERVER_TIME_RESOURCES_FORMAT = "HH:mm:ss"
        val SERVER_TIME_FORMAT = "HH:mm:ss.SSS"


        fun parseTime(date: String, inFormat: String, outFormat: String): String? {

            try {
                val date1 = SimpleDateFormat(inFormat, Locale.getDefault()).parse(date)
                val dateFormat = SimpleDateFormat(outFormat, Locale.getDefault())
                return dateFormat.format(date1)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return null

        }


        fun getPrefs(context: Context): SharedPreferences {
            return context.getSharedPreferences("Login", Context.MODE_PRIVATE)//table name
        }


        //dialog box that used for loading
        fun showLoginDialog(context: Context) {
            android.app.AlertDialog.Builder(context)
                .setMessage("ERROR!")
                .setPositiveButton("Ok")
                { dialog, which -> dialog.dismiss() }
                .show()

        }

        fun getLocation(lat: String, long: String, context: Context) {

        }

        fun hideSoftKeyboard(view: View, context: Context) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN)
        }


    }


}