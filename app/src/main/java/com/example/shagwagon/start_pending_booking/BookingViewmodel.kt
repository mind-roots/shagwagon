package com.example.shagwagon.start_pending_booking

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.pending_screen.Pendingmodel

class BookingViewmodel(application: Application):AndroidViewModel(application) {


    var mrepo=PendingBookingRepository(application)

    //create the function to get the data from the repository



    fun SetActiveBooking(bookingId: String,status:String) {

        mrepo.SetActiveBooking(bookingId,status)
    }


    //..........sending the data in activity in observer.............//
    fun ActiveDataSucessful(): MutableLiveData<Pendingmodel> {


        return mrepo.ActiveDataSucessful()
    }

    fun ActiveDataFailure(): MutableLiveData<String> {


        return mrepo.ActiveDataFailure()
    }


}
