package com.example.shagwagon.start_pending_booking

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import com.example.shagwagon.pending_screen.InnerModel
import com.example.shagwagon.pending_screen.Pendingmodel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class PendingBookingRepository(var application: Application) {
    var mSucessful = MutableLiveData<Pendingmodel>()
    private val mfailure = MutableLiveData<String>()
    fun SetActiveBooking(bookingId: String,status: String) {

        setBooking(bookingId,status)
    }


    private fun setBooking(bookingId: String,status:String) {
        //getting the authcode
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()


        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.updateBookingStatus(auth, bookingId,status,"2")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val resp = response.body()!!.string()
                    val json = JSONObject(resp)
                    val statuss = json.optString("status")
                    val msg = json.optString("msg")

                    var model = Pendingmodel()

                    val innerobject = json.getJSONObject("data")
                    model.booking_type = innerobject.optString("booking_type")
                    model.booking_date = innerobject.optString("booking_date")
                    model.booking_time = innerobject.optString("booking_time")
                    model.latitude = innerobject.optString("latitude")
                    model.longitude = innerobject.optString("longitude")
                    model.total_amount = innerobject.optString("total_amount")
                    model.created_at = innerobject.optString("created_at")
                    model.status = innerobject.optString("status")
                    model.driver_id = innerobject.optString("driver_id")
                    model.customer_name = innerobject.optString("customer_name")
                    model.profile_image = innerobject.optString("customer_image")
                    model.customer_rating = innerobject.optString("status")
                    model.driver_name = innerobject.optString("driver_id")
                    model.driver_rating = innerobject.optString("driver_rating")
                    model.driver_phone = innerobject.optString("driver_phone")
                    model.statuss = statuss
                    //  arraylist.add(model)
                   /* val arrayobj = innerobject.optJSONArray("booked_services")

                    var internalmodel = ArrayList<InnerModel>()
                    for (i in 0 until arrayobj.length()) {
                        val innermodel = InnerModel()
                        //getting the object
                        val innerobject = arrayobj.optJSONObject(i)
                        innermodel.service_id = innerobject.optString("service_id")
                        innermodel.price = innerobject.optString("price")
                        innermodel.name = innerobject.optString("name")

                        internalmodel.add(innermodel)
                        model.modelcategories=internalmodel
                    }*/
                    mSucessful.value = model
                } else {

                    val msg = "No Response"
                    mfailure.value = msg
                }


            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                mfailure.value = msg

            }

        })


    }

    fun ActiveDataSucessful(): MutableLiveData<Pendingmodel> {


        return mSucessful
    }

    fun ActiveDataFailure(): MutableLiveData<String> {


        return mfailure
    }
}
