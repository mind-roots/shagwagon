package com.example.shagwagon.start_pending_booking


import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.shagwagon.R
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.databinding.FragmentPendingbookingActivityBinding
import com.example.shagwagon.pending_screen.Pendingmodel
import kotlinx.android.synthetic.main.fragment_pendingbooking__activity.*
import java.io.IOException
import java.lang.Exception
import java.lang.StringBuilder


class PendingBookingActivity : AppCompatActivity() {


    lateinit var longitude: String
    lateinit var latitude: String
    private lateinit var text: TextView
    lateinit var mbinding: FragmentPendingbookingActivityBinding
    lateinit var mvmodel: BookingViewmodel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.fragment_pendingbooking__activity)
        mvmodel = ViewModelProviders.of(this).get(BookingViewmodel::class.java)


        var alldatas = intent.getParcelableExtra<Pendingmodel>("AllData")
        var model = Data()


        model.customer_name = alldatas.customer_name
        model.total_amount = alldatas.total_amount
        model.booking_date = alldatas.booking_date
        // address set in the method
        latitude = alldatas.latitude
        longitude = alldatas.longitude
        // model.booking_time = alldatas.booking_time
        mbinding.datas = model
        val time = alldatas.booking_time
        val profile_image = alldatas.profile_image
//converting the time format
        val dates = Constants.parseTime(time, Constants.SERVER_TIME_RESOURCES_FORMAT,
            Constants.LOCAL_TIME_FORMAT)
        mbinding.textView28.text = dates
        //set the image


        if(profile_image.isNotEmpty()){
            Glide.with(this)
                .load(profile_image)
                .into(imageView3 as ImageView)
        }
        else{
            Glide.with(this)
                .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                .into(imageView3 as ImageView)
        }



        //...........set the condition for the schedule  completed and subscription................................//
        val booking_type = alldatas.booking_type
        if (booking_type == "2") {
            //set the underline text
            val udata = "SCHEDULE"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView21.text = text
        } else if (booking_type == "3") {

            //set the underline text
            val udata = "SUBSCRIBED"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView21.text = text
        } else {

            //set the underline text
            val udata = "INSTANT BOOKING"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView21.text = text
        }


        //......................set the empty title for the toolbar...............................//
        title = " "
        //get the id of toolbar and textview
        text = findViewById(R.id.toolbar2text)
        //set the text
        text.text = "Pending Booking"
        setSupportActionBar(toolbar22)
        //enabled thebackbutton
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.left_arrow)

//.......................set data in adapter....................//
        val manager = LinearLayoutManager(this)
        mbinding.pendingrecycler.layoutManager = manager
        mbinding.pendingrecycler.adapter = StartBookingAdapter(this, alldatas)

// convertin the adress
        getAddress()


        //..........Active Screen Api..................//

        mbinding.button3.setOnClickListener {

            //getting the booking id
        /*    val booking_id = alldatas.booking_id
            mvmodel.SetActiveBooking(booking_id)

*/
        }

        //................Active Screen Api Observer.............................//

        //on data sucessull
        mvmodel.ActiveDataSucessful().observe(this, Observer {
           // BusProvider.getInstance().post(MyValue("test event"))
           BusProvider.getInstance().post(MyValue(it))//set the model
            finish()


        })

        //on failure observer
        mvmodel.ActiveDataFailure().observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()

        })


    }

    //........Event bus method...........//
    override fun onStart() {
        super.onStart()
        BusProvider.getInstance().register(this)


    }

//    override fun onDestroy() {
//        super.onDestroy()
//        BusProvider.getInstance().unregister(this)
//    }



    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            if (item.itemId == android.R.id.home) {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    //geo coading
    private fun getAddress(): String? {
        // 1
        val geocoder = Geocoder(this)
        val address: Address?

        try {
            // 2
            val addresses = geocoder.getFromLocation(latitude.toDouble(), longitude.toDouble(), 1)
            address = addresses[0]
            val builder = StringBuilder()

//            builder.append(address.featureName).append(" ,\t")
//            builder.append(address.thoroughfare).append(",\t")
            builder.append(address.subAdminArea).append(",\t")
            builder.append(address.countryName)

            var result = builder.toString()
            mbinding.textView20.text = result

        } catch (e: Exception) {
            Log.e("MapsActivity", e.localizedMessage)
        }
        return null
    }

}
