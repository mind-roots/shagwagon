package com.example.shagwagon.start_pending_booking

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.databinding.StartBookingPendingAdpterBinding
import com.example.shagwagon.pending_screen.InnerModel
import com.example.shagwagon.pending_screen.Pendingmodel

class StartBookingAdapter(var activity: Context, var alldatas: Pendingmodel) :
    RecyclerView.Adapter<StartBookingAdapter.Viewholder>() {
    lateinit var mbinding: StartBookingPendingAdpterBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        mbinding = StartBookingPendingAdpterBinding.inflate(LayoutInflater.from(activity), parent, false)
        return Viewholder(mbinding)
    }

    override fun getItemCount(): Int {
       return alldatas.modelcategories.size



    }

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
       val data= alldatas.modelcategories[position]
        holder.bind(data)


    }

    class Viewholder(var mbinding: StartBookingPendingAdpterBinding) : RecyclerView.ViewHolder(mbinding.root) {
        fun bind(data: InnerModel) {

          //  mbinding.data=data
//            var name=data.name
            mbinding.textView3.text=data.name
            mbinding.textView4.text=data.price
//            mbinding.data!!.name=name
            mbinding.executePendingBindings()



        }

    }

}
