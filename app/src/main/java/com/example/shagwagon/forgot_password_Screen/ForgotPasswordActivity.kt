package com.example.shagwagon.forgot_password_Screen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.shagwagon.R
import com.example.shagwagon.databinding.ActivityForgotPasswordBinding
import com.example.shagwagon.sign_in.Sign_in_Activity

class ForgotPasswordActivity : AppCompatActivity(), ForgotClicked {


    lateinit var mvmodel: ForgotViewModel
    lateinit var mbinding: ActivityForgotPasswordBinding
    lateinit var text: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //full screen code enabled code
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        mvmodel = ViewModelProviders.of(this).get(ForgotViewModel::class.java)


        val toolbar = findViewById<Toolbar>(R.id.toolbar2)
        title = " "
        setSupportActionBar(toolbar)
        //enabled thebackbutton
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.back_arrow)

        //......................WebServer Observer Set..............................................//
        mvmodel.SucessfulData().observe(this, Observer {
            mbinding.button5.visibility = View.VISIBLE
            mbinding.progressBar2.visibility = View.GONE
            Toast.makeText(applicationContext, it.msg, Toast.LENGTH_LONG).show()

            //when the password is changed user o to the login screen
            val intent = Intent(this, Sign_in_Activity::class.java)
            startActivity(intent)
            finish()
        })
        //failure observer
        mvmodel.errorget().observe(this, Observer {

            mbinding.button5.visibility = View.VISIBLE
            mbinding.progressBar2.visibility = View.GONE
            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
        })

        //set the clicked
        mbinding.clicked = this

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            if (item.itemId == android.R.id.home) {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    //......................Webservice  forgotPassword..............................................//
    override fun setclick(email: String) {
        if (mbinding.edtemail.text!!.isEmpty()) {

            Toast.makeText(this, "Please Enter Email", Toast.LENGTH_SHORT).show()
        } else if (!isValidEmailId(mbinding.edtemail.text.toString().trim())) {
            Toast.makeText(applicationContext, "Enter the Valid Email", Toast.LENGTH_SHORT).show()

        } else {
            val email = email
            mvmodel.setEmail(email)
            mbinding.button5.visibility = View.GONE
            mbinding.progressBar2.visibility = View.VISIBLE


        }

    }

    //Email validate code
    private fun isValidEmailId(email: String): Boolean {

        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches())
    }
}
