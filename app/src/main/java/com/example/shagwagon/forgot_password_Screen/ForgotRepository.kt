package com.example.shagwagon.forgot_password_Screen

import android.app.Application
import android.provider.Settings
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ForgotRepository(var application: Application) {
    val mFailure = MutableLiveData<String>()
    val mSucessful = MutableLiveData<ForgotModel>()

    fun setEmail(email: String) {

        setEmails(email)
    }

    private fun setEmails(email: String) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()

        //getting the android id
        val device_id = Settings.Secure.getString(
            application.contentResolver,
            Settings.Secure.ANDROID_ID
        )

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.forgotpassword(email, "2")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    val resp = response.body()!!.string()
                    val json = JSONObject(resp)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val model = ForgotModel()
                    if (status == "true") {
                        model.status = status
                        model.msg = msg
                        mSucessful.value = model
                    } else {

                        mFailure.value = msg
                    }

                } else {


                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                mFailure.value = msg
            }
        })

    } fun sucessget():MutableLiveData<ForgotModel>{

        return mSucessful
    }

    fun errorget():MutableLiveData<String>{

        return mFailure
    }

}





