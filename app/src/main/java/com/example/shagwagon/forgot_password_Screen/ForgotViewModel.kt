package com.example.shagwagon.forgot_password_Screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.sign_in.ErrorModel
import com.example.shagwagon.sign_in.SignupModel

class ForgotViewModel(application: Application):AndroidViewModel(application) {



    //send the data to repo
    fun setEmail(email: String) {
        mRepo.setEmail(email)

    }
    //get the data
    fun SucessfulData(): MutableLiveData<ForgotModel> {
       return  mRepo.sucessget()
    }

    //error observer
    fun errorget(): MutableLiveData<String> {

        return mRepo.errorget()
    }


    val mRepo=ForgotRepository(application)

}
