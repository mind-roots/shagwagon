package com.example.shagwagon.sign_in

import android.app.Application
import android.widget.Button
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class SignUpViewmodel(application: Application):AndroidViewModel(application) {
    fun setData( model: SignupModel) {

        mrepo.setData(model)
    }

    fun getdata():MutableLiveData<SignupModel> {
        return mrepo.getdata()


    }



    val mrepo=SignRepository(application)




    fun errorget():MutableLiveData<ErrorModel>{

        return mrepo.errorget()
    }

}
