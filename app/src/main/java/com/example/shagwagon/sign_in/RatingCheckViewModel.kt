package com.example.shagwagon.sign_in

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.main_activity_parent.RatingCheckRepository
import com.example.shagwagon.main_activity_parent.RatingModel

class RatingCheckViewModel(application: Application) : AndroidViewModel(application)  {

    var mrepo = RatingCheckRepository(application)

    fun pendingSucessful(): MutableLiveData<RatingModel> {

        return mrepo.success()
    }

    fun pendingFailure(): MutableLiveData<Boolean> {

        return mrepo.failure()
    }

    fun setData() {
        mrepo.setdata()
    }
}