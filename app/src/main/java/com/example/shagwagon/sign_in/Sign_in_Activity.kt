package com.example.shagwagon.sign_in

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.example.shagwagon.main_activity_parent.MainActivity

import com.example.shagwagon.databinding.ActivitySignInBinding
import com.example.shagwagon.forgot_password_Screen.ForgotPasswordActivity
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.R
import com.example.shagwagon.main_activity_parent.FirebaseRepository
import com.google.firebase.iid.FirebaseInstanceId


class Sign_in_Activity : AppCompatActivity(), ClickBtn {


    override fun forgotPasssword() {

        val intent = Intent(this, ForgotPasswordActivity::class.java)
        startActivity(intent)
    }


    lateinit var mvmodel: SignUpViewmodel
    private lateinit var binding: ActivitySignInBinding
    lateinit var textview: String
    var error: String = ""
    var email_driver: String = ""
    var password_driver: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
        mvmodel = ViewModelProviders.of(this)[SignUpViewmodel::class.java]
        binding.click = this

        //..........................user automatically login after one time login.................//
        val data = Constants.getPrefs(application).getString(Constants.profile, "1")
//................................check the user is already login or not....................................//
        if (data != "1") {
            val intent = Intent(this, MainActivity::class.java)//if login
            startActivity(intent)
            finish()

        }


//.........................observer of login api...............................//

        mvmodel.getdata().observe(this, Observer {

            if (it != null) {
                binding.button2.visibility = View.VISIBLE
                binding.progressBar.visibility = View.GONE


                if (it.status == "true") {
                    Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
                    var firebaseRepository = FirebaseRepository(application)
                    var token = FirebaseInstanceId.getInstance().token
                    if(token!!.isNotEmpty()){
                        firebaseRepository.setdata(token)
                        Log.e("firebaseToken", token)
                    }
                    val launchIntent = Intent(this, MainActivity::class.java)
                    launchIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(launchIntent)
                    finish()
                }

            }
        })

        mvmodel.errorget().observe(this, Observer {
            binding.button2.visibility = View.VISIBLE
            binding.progressBar.visibility = View.GONE
            error = it.error
            if (error.contentEquals("We have detected a new login activity on this device. You would be logged out of other device on confirmation. Are you sure you want to continue?")) {
                dialog()
            }
            else{
                Toast.makeText(this,error,Toast.LENGTH_SHORT).show()
            }


            //Toast.makeText(application, it.error, Toast.LENGTH_SHORT).show()// handling all the error

        })

    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Alert")
        builder.setMessage("Are you sure you want to exit?")
        builder.setIcon(android.R.drawable.ic_dialog_alert)
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            super.onBackPressed()
        }
        builder.setNegativeButton("No") { dialog, which ->
            dialog.cancel()
        }
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    override fun setclick(name: String, password: String) {
        if (binding.Email.text.isEmpty()) {
            Toast.makeText(this, "Enter Email/Phone Number", Toast.LENGTH_SHORT).show()
        } else if (binding.password.text!!.isEmpty()) {
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show()
        } else {

            val model = SignupModel()
            model.edtemail = name
            email_driver = name
            model.edtpassword = password
            password_driver = password
            model.re_login = "0"
            //send the data to the view model for hitting the api
            mvmodel.setData(model)
            binding.button2.visibility = View.GONE
            binding.progressBar.visibility = View.VISIBLE

        }

    }

    fun dialog() {
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Alert")
        //set message for alert dialog
        builder.setMessage(error)
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            val model = SignupModel()
            model.edtemail = email_driver
            model.edtpassword = password_driver
            model.re_login = "1"
            //send the data to the view model for hitting the api
            binding.button2.visibility = View.GONE
            binding.progressBar.visibility = View.VISIBLE
            mvmodel.setData(model)

        }
        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->
            //  Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


}

