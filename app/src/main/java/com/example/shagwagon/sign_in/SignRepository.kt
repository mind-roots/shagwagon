package com.example.shagwagon.sign_in

import android.app.Application
import android.provider.Settings
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class SignRepository(var application: Application) {


    var mResult = MutableLiveData<SignupModel>()
    var nErrorr = MutableLiveData<ErrorModel>()

    fun setData(model: SignupModel) {


        setSignUPData(model)
    }

    private fun setSignUPData(
        model: SignupModel
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()

        //getting the android id
        val device_id = Settings.Secure.getString(
            application.contentResolver,
            Settings.Secure.ANDROID_ID
        )

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.login(model.edtemail, model.edtpassword, device_id, "1",model.re_login)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val modeldata = SignupModel()
                        modeldata.msg = json.optString("msg")
                        modeldata.status = status
                        if (status == "true") {

                            //below getting the data from the object
                            var jsonobject = json.getJSONObject("data")
                            val auth_code = jsonobject.optString("auth_code")
//user details of profile
                            val profile = jsonobject.getJSONObject("profile")


                            //save the authcode in shared prefrence
                            val editors = Constants.getPrefs(application).edit()
                            editors.putString(Constants.token, auth_code)
                            editors.apply()
                            //shared prefrence login detail saved

                            val editor = Constants.getPrefs(application).edit()

                            editor.putString(Constants.profile, profile.toString())
                            editor.apply()


                            mResult.value = modeldata

                        }
                        else {
                            val modelStore = ErrorModel()
                            modelStore.error = modeldata.msg//creating a new observer
                            modelStore.status = status
                            nErrorr.value = modelStore

                        }

                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                } else {
                    Toast.makeText(application, "Network", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val model = ErrorModel()
                model.error = "Network Error Please Check Your Network"
                nErrorr.value = model

            }

        })


    }
    

    //status true observer
    fun getdata(): MutableLiveData<SignupModel> {
        return mResult
    }

    //error observer
    fun errorget(): MutableLiveData<ErrorModel> {

        return nErrorr
    }


}
