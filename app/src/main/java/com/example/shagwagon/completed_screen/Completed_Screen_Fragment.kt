package com.example.shagwagon.completed_screen

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.databinding.FragmentCompletedScreenBinding
import com.example.shagwagon.home_screen.HomeFragment
import com.example.shagwagon.pending_screen.Pendingmodel
import com.example.shagwagon.sign_in.Sign_in_Activity
import kotlinx.android.synthetic.main.fragment_completed__screen.*


class Completed_Screen_Fragment : Fragment(), HomeFragment.setdataInterface2, CompleteScreenAdapter.UpdateOffset {
    private lateinit var recycleer: CompleteScreenAdapter
    lateinit var mbinding: FragmentCompletedScreenBinding
    lateinit var mvmodel: CompletedViewModel
    var islastpageData: Boolean = false
    var isLoadingMoreItems: Boolean = false

    var a: ModelSearch = ModelSearch()
    lateinit var update: completedData
    override fun setadapterData2(position: Int, data: Pendingmodel) {


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        mbinding = FragmentCompletedScreenBinding.inflate(LayoutInflater.from(context), container, false)
        mvmodel = ViewModelProviders.of(this).get(CompletedViewModel::class.java)
        return mbinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var layout = LinearLayoutManager(context)
        recycleer = CompleteScreenAdapter(activity!!, this)
        mbinding.recyclerview.layoutManager = layout
        mbinding.recyclerview.adapter = recycleer

        //.................by default visible the  skelton view and pagination
        // visible the shimer animation
        mbinding.shimmer.visibility = View.VISIBLE
        mbinding.noview.text = "No completed bookings yet"
        a = update.getData()
        //  mbinding.progressBar4.visibility = View.VISIBLE
//..............APi Call or Data set to the repository.................................//
        if (a.count == 0) {

            shimmer.startShimmerAnimation()
            mvmodel.setData(update.getOff(), false)
        } else {
            recycleer.UPdate(a.list)
            mbinding.shimmer.visibility = View.GONE
        }


        //.......................pagination..................//
        recyclerview.addOnScrollListener(object : CompletedPagination(layout) {


            override fun isLastPage(): Boolean {
                return islastpageData
            }

            override fun isLoading(): Boolean {
                return isLoadingMoreItems

            }

            override fun loadMoreItems() {
                isLoadingMoreItems = true
                mbinding.progressBar4.visibility = View.VISIBLE
                //   mbinding.shimmer.visibility = View.VISIBLE
                update.updateOff()
                mvmodel.setData(update.getOff(), true)
            }


        })


//.......................Completed APi Webservice observer.....................................//

        mvmodel.dataSucessful().observe(this, Observer {


            a = it

            update.sendingCompletedData(it)

            if (it.count > 0) {

                mbinding.shimmer.visibility = View.GONE
                mbinding.progressBar4.visibility = View.INVISIBLE
                mbinding.recyclerview.visibility = View.VISIBLE
                //set the layout
                mbinding.recyclerview.layoutManager = layout

                isLoadingMoreItems = false
                if (it.count == it.list.size) {
                    islastpageData = true
                }
                //set the adapter
                //  mbinding.recyclerview.adapter = CompleteScreenAdapter(context!!, it.list)
                recycleer.UPdate(update.getData().list)
                //recycleer.UPdate(it.list)

            } else {
                mbinding.shimmer.visibility = View.GONE
                mbinding.progressBar4.visibility = View.INVISIBLE
                mbinding.recyclerview.visibility = View.GONE
                //set the layout
                mbinding.recyclerview.layoutManager = layout

            }
        })
//failure observer
        mvmodel.dataFailure().observe(this, Observer {


            if (it.msg.contentEquals("Auth code has been expired.")) {

                Toast.makeText(activity, " Logout successfully", Toast.LENGTH_SHORT).show()


                val edit = Constants.getPrefs(activity!!).edit()
                edit.clear()
                edit.apply()

                val i = Intent(activity, Sign_in_Activity::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
                activity?.finish()
            }

            mbinding.shimmer.visibility = View.GONE
            mbinding.progressBar4.visibility = View.GONE
            //
            if (a.list.size > 0) {
                mbinding.recyclerview.visibility = View.VISIBLE
                mbinding.noview.visibility = View.GONE
                mbinding.imageView.visibility = View.GONE

            } else {
                mbinding.recyclerview.visibility = View.GONE
                mbinding.noview.visibility = View.VISIBLE
                mbinding.imageView.visibility = View.VISIBLE

            }


            //Toast.makeText(context!!, it.failureError.toString(), Toast.LENGTH_SHORT).show()
        })

    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        update = context as completedData
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        update = activity as completedData
    }

    override fun updateOff() {
        update.setOff()
    }

    interface completedData {
        fun sendingCompletedData(modelSearch: ModelSearch)
        fun getData(): ModelSearch
        fun getOff(): Int
        fun updateOff()
        fun setOff()
    }
}
