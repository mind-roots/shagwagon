package com.example.shagwagon.completed_screen

import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.booking_screen.BookingScreenActivity
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.databinding.CompletedAdapterBinding
import java.io.IOException
import java.lang.StringBuilder

class CompleteScreenAdapter(
    var context: Context,
    var completedScreenFragment: Completed_Screen_Fragment
) :
    RecyclerView.Adapter<CompleteScreenAdapter.ViewHolder>() {
    var it = ArrayList<Model>()

    lateinit var mbinding: CompletedAdapterBinding
    lateinit var latitude: String
    lateinit var longitude: String
    var update = completedScreenFragment as UpdateOffset
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mbinding = CompletedAdapterBinding.inflate(LayoutInflater.from(context), parent, false)

        return ViewHolder(mbinding)


    }

    override fun getItemCount(): Int {
        return it.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //get the data
        var data = it.get(position)
        //send the data to viewholder
        holder.bind(data)
        latitude = data.latitude
        longitude = data.longitude
        val Booking_type = data.booking_type
        if (Booking_type == "2") {
            //set the underline text

            val udata = "SCHEDULED"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView36.text = text
        } else if (Booking_type == "3") {
            //set the underline text
            val udata = "SUBSCRIBED"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView36.text = text
        } else {

            //set the underline text
            val udata = "INSTANT BOOKING"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView36.text = text
        }
        val time =
            Constants.parseTime(
                data.booking_time,
                Constants.SERVER_TIME_RESOURCES_FORMAT,
                Constants.LOCAL_TIME_FORMAT
            )
        mbinding.textView37.text = data.booking_date + " " + time

//method of converting the adress
        getAddress()

        //set the click
        holder.itemView.setOnClickListener(View.OnClickListener {


            //...............go to the another activity and send  all the data
            val intent = Intent(context, BookingScreenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.putExtra("AllData", data)
            context.startActivity(intent)
            //...........convert the address.....................//
            update.updateOff()


        })

    }

    fun UPdate(list: java.util.ArrayList<Model>) {

        it = list
        notifyDataSetChanged()
    }


    class ViewHolder(var mbinding: CompletedAdapterBinding) :
        RecyclerView.ViewHolder(mbinding.root) {
        fun bind(data: Model) {

            mbinding.data = data
            mbinding.executePendingBindings()
        }

    }

    //geo coading
    private fun getAddress(): String? {
        // 1
        val geocoder = Geocoder(context)
        val address: Address?

        try {
            // 2
            val addresses = geocoder.getFromLocation(latitude.toDouble(), longitude.toDouble(), 1)
            address = addresses[0]
            val builder = StringBuilder()
//            builder.append(address.featureName).append(",\t")
//            builder.append(address.thoroughfare).append(",\t")
            builder.append(address.subAdminArea).append(",\t")
            builder.append(address.countryName)
            var result = builder.toString()
            mbinding.textView11.text = result
        } catch (e: Exception) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return null
    }

    interface UpdateOffset {
        fun updateOff()
    }
}
