package com.example.shagwagon.completed_screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.facebook.shimmer.ShimmerFrameLayout

class CompletedViewModel(application: Application) : AndroidViewModel(application) {

    var mrepo = CompletedRepository(application)

//suceesful observer
    fun dataSucessful(): LiveData<ModelSearch> {

        return mrepo.dataSucessful()

    }

    //failure observer
    fun dataFailure(): MutableLiveData<ModelError> {

        return mrepo.dataFailure()

    }

    fun setData( offset: Int, Loadmore: Boolean) {

        mrepo.setdata(offset,Loadmore)
    }

}
