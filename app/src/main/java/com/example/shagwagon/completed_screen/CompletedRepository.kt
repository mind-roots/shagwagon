package com.example.shagwagon.completed_screen

import android.app.Application
import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class CompletedRepository(var application: Application) {
    var mSucessful = MutableLiveData<ModelSearch>()
    var mfailure = MutableLiveData<ModelError>()
    private var outerarray = ArrayList<Model>()


    fun setdata(offset: Int, loadmore: Boolean) {

        getcompleteRequest(offset, loadmore)
    }

    fun getcompleteRequest(
        offset: Int,
        loadmore: Boolean
    ) {val retrofit:Retrofit
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(5, TimeUnit.MINUTES)
            .writeTimeout(5, TimeUnit.MINUTES)
            .readTimeout(5, TimeUnit.MINUTES)
        var client = OkHttpClient()
        client = builder.build()
        //if (retrofit == null) {
        retrofit = Retrofit.Builder().client(client).baseUrl(Constants.BASE_URL).build()
        //}



        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.completedBookings(auth, "10", offset.toString())
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {


                    val res = response.body()!!.string()
                    val jsonobj = JSONObject(res)
                    val status = jsonobj.optString("status")
                    val msg = jsonobj.optString("msg")
                    val count = jsonobj.optString("count")

                    if (status == "true") {
                        val jsonarray = jsonobj.optJSONArray("data")

                        var outerarray1 = ArrayList<Model>()
                        for (i in 0 until jsonarray.length()) {

                            val outermodel = Model()  //storing the data in the model

                            val arrayobject = jsonarray.getJSONObject(i)
                            outermodel.booking_id = arrayobject.optString("booking_id")
                            outermodel.booking_type = arrayobject.optString("booking_type")
                            outermodel.booking_date = arrayobject.optString("booking_date")
                            outermodel.booking_time = arrayobject.optString("booking_time")
                            outermodel.latitude = arrayobject.optString("latitude")
                            outermodel.longitude = arrayobject.optString("longitude")
                            outermodel.total_amount = arrayobject.optString("total_amount")
                            outermodel.customer_name = arrayobject.optString("customer_name")
                            outermodel.customer_rating = arrayobject.optString("customer_rating")
                            outermodel.profile_image = arrayobject.optString("profile_image")
                            outermodel.driver_name = arrayobject.optString("driver_name")
                            outermodel.driver_rating = arrayobject.optString("driver_rating")
                            outermodel.driver_phone = arrayobject.optString("driver_phone")


                            outerarray.add(outermodel)//add the model in the array
                            outerarray1.add(outermodel)//add the model in the array

                            //get array in the object
                            val objectarray = arrayobject.optJSONArray("booked_services")

                            val inneraray = ArrayList<InnerModel>()
                            //storing the data in the array
                            for (i in 0 until objectarray.length()) {
                                //storing the data in the object
                                val innermodel = InnerModel()
                                val objects = objectarray.getJSONObject(i)
                                innermodel.service_id = objects.optString("service_id")
                                innermodel.price = objects.optString("price")
                                innermodel.name = objects.optString("name")

                                //stored these data in the array
                                inneraray.add(innermodel)
                                //storing the data in the outermodel

                                outermodel.modelcategory = inneraray
                                //set the data in the live data
                                // mSucessful.value = outerarray
                            }
                        }//creating a new model to stored all the data
                        val models = ModelSearch()
                        models.status = status
                        models.count = count.toInt()
                       // models.list = outerarray
                        models.list = outerarray1
                        mSucessful.value = models
                    } else {
                        var model = ModelError()
                        model.error1 = "Data Unsucessful"
                        model.msg=msg
                        mfailure.value = model
                    }



                }


            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                var model = ModelError()
                model.failureError = "Network Error Please Check Your Network"
                mfailure.value = model



            }

        })


    }

    // sucessful observer
    fun dataSucessful(): LiveData<ModelSearch> {

        return mSucessful

    }

    //failure observer
    fun dataFailure(): MutableLiveData<ModelError> {

        return mfailure

    }


}

class ModelSearch() : Parcelable{
    var status: String = ""
    var list = ArrayList<Model>()
    var count: Int = 0

    constructor(parcel: Parcel) : this() {
        status = parcel.readString()
        count = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeInt(count)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelSearch> {
        override fun createFromParcel(parcel: Parcel): ModelSearch {
            return ModelSearch(parcel)
        }

        override fun newArray(size: Int): Array<ModelSearch?> {
            return arrayOfNulls(size)
        }
    }
}

