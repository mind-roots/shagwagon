package com.example.shagwagon.setting_screen

import android.app.Application
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.R
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import com.example.shagwagon.sign_in.ErrorModel
import com.example.shagwagon.sign_in.SignupModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class PasswordRepository(var application: Application) {

    var mSucessful = MutableLiveData<String>()
    var mFailure = MutableLiveData<String>()

    fun setData(oldPass : String,newPass : String) {
        changePass(oldPass,newPass)
    }

    private fun changePass(oldPass : String,newPass : String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.ChangePassword(auth,oldPass,newPass,"2")
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val modeldata = SignupModel()
                        modeldata.msg = json.optString("msg")
                        modeldata.status = status
                        if (status == "true") {
                            mSucessful.value=modeldata.status
                           Toast.makeText(application,modeldata.msg,Toast.LENGTH_LONG).show()

                        }
                        else
                        {
                            if(modeldata.msg.contentEquals("Auth code has been expired")){
                                mFailure.value = modeldata.msg
                            }
                            else{


                                mFailure.value = modeldata.msg
                                var toast:Toast  = Toast.makeText(application,modeldata.msg,Toast.LENGTH_LONG)
                                toast.show()
                            }



                        }

                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                } else {
                    Toast.makeText(application, "Network", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val model = ErrorModel()
                model.error = "Network Error Please Check Your Network"
            }

        })


    }


    //status true observer
    fun getdata(): MutableLiveData<String> {
        return mSucessful
    }

    //error observer
    fun errorget(): MutableLiveData<String> {

        return mFailure
    }

}