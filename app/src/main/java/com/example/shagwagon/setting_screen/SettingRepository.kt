package com.example.shagwagon.setting_screen

import android.app.Application
import android.provider.Settings
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import com.example.shagwagon.sign_in.SignupModel
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File
import java.lang.Exception

class SettingRepository(var application: Application) {
    //.............update image.............//
    var mSucessful = MutableLiveData<SeetingModel>()
    var mFailure = MutableLiveData<String>()
    val model = SeetingModel()
    //.................delete account api.....................//
    private val deleteSucessful = MutableLiveData<SeetingModel>()
    private val deletefailure = MutableLiveData<String>()

    //.....................update profile api method.............................//
    private val updateSucessful = MutableLiveData<String>()
    private val updatefailure = MutableLiveData<String>()
    fun setimage(uri: File) {

        setimages(uri)
    }

    private fun setimages(uri: File) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val services = retrofit.create(WebServices::class.java)
        //..........................technique to storing the image in the web server......................//

        //pass it like this
        val file = uri
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        // MultipartBody.Part is used to send also the actual file name
        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)

// add another part within the multipart request
        val fullName = RequestBody.create(MediaType.parse("multipart/form-data"), auth)
        val type = RequestBody.create(MediaType.parse("multipart/form-data"), "2")// type

        val call: Call<ResponseBody> = services.changeProfile(fullName, body, type)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
//                    try {
                        var resp = response.body()!!.string()
                        //getting the object
                        val json = JSONObject(resp)
                        val status = json.optString("status")
                        val msg = json.optString("msg")

                        if (status == "true") {
                            model.status = status
                            model.msg = msg
                            val image = json.getString("data")//getting the image and stored in the profile
                            model.data = image


                            //storing the updating image
                            val sharedpref = Constants.getPrefs(application).getString(Constants.profile, "")
                            val gson = Gson()
                            val json = gson.fromJson(sharedpref, SignupModel::class.java)//convert the data
                            json.profile_image = image

                            //save the data by edit fun
                            val gson1 = Gson()
                            val json1 = gson1.toJson(json)

                            Constants.getPrefs(application).edit()
                                .putString(Constants.profile, json1)
                                .apply()

                            mSucessful.value = model

                        } else {
                            mFailure.value = msg


                        }


//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }

                } else {
                    // val msg=""

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                mFailure.value = msg
            }


        })


    }

    //sucessufl data saved
    fun mSucessful(): MutableLiveData<SeetingModel> {

        return mSucessful
    }

    ///failure data saved
    fun mFailure(): MutableLiveData<String> {

        return mFailure
    }


    //.............................delete Accouont Web SErvices..............................................//

    fun deleteAccount() {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()
//getting the authcode
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        //getting the android id

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.deleteaccount(auth, "2")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {

                        val resp = response.body()!!.string()
                        val json = JSONObject(resp)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val model = SeetingModel()
                        if (status == "true") {
                            model.status = status
                            model.msg = msg

                            deleteSucessful.value = model
                        } else {

                            deletefailure.value = msg
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                } else {
                    val resp = response.errorBody()!!.string()
                    deletefailure.value = resp
                    ///
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                deletefailure.value = msg

            }


        })

    }

    // delete sucessful
    fun deletesucessful(): MutableLiveData<SeetingModel> {

        return deleteSucessful
    }


    //delete failure
    fun deletefailure(): MutableLiveData<String> {

        return deletefailure
    }

    //.....................................update proflie api call............................................//

    fun setupdateProfile(model: UpdateProfileModel) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.updateprofile(auth, model.name, model.password, model.number, "2")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {


                    try {
                        val resp = response.body()!!.string()
                        val json = JSONObject(resp)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        if (status == "true") {
                            //update the data in the shared prefs
                            val prefs = Constants.getPrefs(application).getString(Constants.profile, "")
                            val gson = Gson()
                            val json = gson.fromJson(prefs, SignupModel::class.java)
                            json.name = model.name
                            json.contact_number = model.number

                            //save the data in the shared prefernce
                            val gson1 = Gson()
                            val json1 = gson1.toJson(json)
                            Constants.getPrefs(application).edit()
                                .putString(Constants.profile, json1)
                                .apply()

                            val msg = msg
                            updateSucessful.value = msg

                        } else {

                            updatefailure.value = msg
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }


            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                updatefailure.value = msg

            }


        })

    }

    fun setprofilesucess(): MutableLiveData<String> {


        return updateSucessful
    }

    fun setprofilefailure(): MutableLiveData<String> {
        return updatefailure


    }
}
