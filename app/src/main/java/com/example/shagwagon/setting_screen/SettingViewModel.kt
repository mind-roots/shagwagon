package com.example.shagwagon.setting_screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import java.io.File

class SettingViewModel(application: Application):AndroidViewModel(application) {
    val mrepo=SettingRepository(application)


    fun setImage(uri: File) {

        mrepo.setimage(uri)
    }

    //sucessufl data saved
    fun mSucessful(): MutableLiveData<SeetingModel> {

        return  mrepo.mSucessful()
    }
    ///failure data saved
    fun mFailure(): MutableLiveData<String> {

        return  mrepo.mFailure()
    }

    //........................delete Account Api methods
//send the data
    fun deleteAccount() {

        mrepo.deleteAccount()
    }
//get the data from api

    // delete sucessful
    fun deletesucessful():MutableLiveData<SeetingModel>{

        return mrepo. deletesucessful()
    }


    //delete failure
    fun deletefailure():MutableLiveData<String>{

        return mrepo. deletefailure()
    }

    //......................................update profile method..............................//

    fun setupdateProfile(model: UpdateProfileModel) {

        mrepo.setupdateProfile(model)
    }

    //get the data


    fun setprofilesucess(): MutableLiveData<String> {


        return mrepo.setprofilesucess()
    }

    fun setprofilefailure(): MutableLiveData<String> {
        return mrepo.setprofilefailure()


    }





}
