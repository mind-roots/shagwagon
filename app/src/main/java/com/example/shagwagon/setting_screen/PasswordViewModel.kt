package com.example.shagwagon.setting_screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class PasswordViewModel(application: Application): AndroidViewModel(application) {
    val mrepo=PasswordRepository(application)

    fun setData(oldPass : String,newPass : String)
    {
        mrepo.setData(oldPass,newPass)

    }

    fun mSucessful(): MutableLiveData<String> {

        return  mrepo.mSucessful
    }
    ///failure data saved
    fun mFailure(): MutableLiveData<String> {

        return  mrepo.mFailure
    }
}