package com.example.shagwagon.setting_screen

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.shagwagon.databinding.FragmentSettingBinding
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.shagwagon.R
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.databinding.OpenFromCameraPermissionBinding
import com.example.shagwagon.main_activity_parent.MainActivity
import com.example.shagwagon.sign_in.Sign_in_Activity
import com.example.shagwagon.sign_in.SignupModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_setting_.*
import java.io.File
import java.io.FileOutputStream


class Setting_Fragment : Fragment(), SaveClicked {

    lateinit var mvmodelPass: PasswordViewModel
    lateinit var mvmodel: SettingViewModel
    //set the binding here
    lateinit var mbinding: FragmentSettingBinding
    lateinit var text: TextView
    var check: Int = -1
    lateinit var mdialog: Dialog


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//set the binding here
        mbinding = FragmentSettingBinding.inflate(LayoutInflater.from(context), container, false)
        mvmodel = ViewModelProviders.of(this).get(SettingViewModel::class.java)
        mvmodelPass = ViewModelProviders.of(this).get(PasswordViewModel::class.java)


        mbinding.edtpassword.setOnClickListener {
            mdialog = Dialog(context!!)
            mdialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            mdialog.setContentView(R.layout.password_dialog)
            mdialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mdialog.show()

            val mOldPass: TextView = mdialog.findViewById(R.id.oldPassword)
            val mNewPass: TextView = mdialog.findViewById(R.id.newPassword)
            val mConfirmNewPass: TextView = mdialog.findViewById(R.id.confirmNewPassword)

            val submitButton: Button = mdialog.findViewById(R.id.passSubmitButton)
            submitButton.setOnClickListener {

                Constants.hideSoftKeyboard(it, activity!!)
                val oldPass = mOldPass.text.toString()
                val newPass = mNewPass.text.toString()
                val confirmNewPass = mConfirmNewPass.text.toString()



                if (newPass == confirmNewPass && oldPass != "") {

                    mvmodelPass.setData(oldPass, newPass)


                } else if (newPass != confirmNewPass && oldPass != "") {
                    Handler().postDelayed({
                        Toast.makeText(context, "Password entered did not match", Toast.LENGTH_LONG).show()
                    }, 500)


                } else
                    Toast.makeText(context, "Please Enter the Data in all the fields", Toast.LENGTH_LONG).show()
            }
        }

        mvmodelPass.mSucessful().observe(this, Observer {

            if (mvmodelPass.mSucessful().value == "true") {
                mdialog.dismiss()
            }

        })
        mvmodel.mSucessful().observe(this, Observer {
            mbinding.imageProgress.visibility = View.GONE
            mbinding.circleImageView3.visibility = View.VISIBLE
            mbinding.imageView8.visibility = View.VISIBLE
            //Toast.makeText(, it.msg, Toast.LENGTH_SHORT).show()
            //set the image

            Glide.with(this)
                .load(it.data)
                .into(mbinding.circleImageView3)//set the image
//            Picasso.with(context).load(profileData1.profile_image).memoryPolicy(MemoryPolicy.NO_CACHE).into(mbinding.circleImageView3)
//            mbinding.circleImageView3.invalidate()


        })
        mvmodelPass.mFailure().observe(this, Observer {
            if (it.contentEquals("Auth code has been expired")) {
                Toast.makeText(activity, " Logout successfully", Toast.LENGTH_SHORT).show()


                val edit = Constants.getPrefs(activity!!).edit()
                edit.clear()
                edit.apply()

                val i = Intent(activity, Sign_in_Activity::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
                activity?.finish()
            }
        })


        return mbinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//data is set in the navigation drawer
        //get the data of the sharedprefrences
        val prefs = Constants.getPrefs(context!!).getString(Constants.profile, "")
        //convert the data from the json
        val gson = Gson()
        val json = gson.fromJson(prefs, SignupModel::class.java)//get the data or convert the data from json to string
        //set the data the model
        val profile = ProfileModel()
        profile.names = json.name
        profile.contact_numbers = json.contact_number
        // profile.passwords=json.password
        mbinding.data = profile
        Glide.with(this)
            .load(json.profile_image)
            .into(mbinding.circleImageView3)






        //.......................................updateprofile response observer.......................................//

        mvmodel.setprofilesucess().observe(this, Observer {
            mbinding.progressBar3.visibility = View.GONE
            mbinding.button.visibility = View.VISIBLE

            //get the data from the shared prefs
            val prefs = Constants.getPrefs(context!!).getString(Constants.profile, "")
            val gson = Gson()
            val json = gson.fromJson(prefs, SignupModel::class.java)
            val number = json.contact_number
            val name = json.name
            val profile = ProfileModel()
            profile.names = name
            profile.contact_numbers = number
            mbinding.data = profile

            Toast.makeText(activity!!, it, Toast.LENGTH_SHORT).show()
        })
        //failure case
        mvmodel.setprofilefailure().observe(this, Observer {
            mbinding.progressBar3.visibility = View.GONE
            mbinding.button.visibility = View.VISIBLE

            Toast.makeText(activity!!, it, Toast.LENGTH_SHORT).show()

        })


//.......................................delete Account response observer.......................................//

        //when account delete
        mvmodel.deletesucessful().observe(this, Observer {

            Toast.makeText(activity!!, it.msg, Toast.LENGTH_SHORT).show()
            //calered the data from the shared preference
//            Constants.getPrefs(activity!!).edit()
//                .putString(Constants.profile, "")
//                .putString(Constants.token, "")
//                .clear()
//                .apply()
            val edit = Constants.getPrefs(activity!!).edit()
            edit.clear()
            edit.apply()


            val i = Intent(activity!!, Sign_in_Activity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
            activity!!.finish()

        })
        //when account is not delete
        mvmodel.deletefailure().observe(this, Observer {

            Toast.makeText(activity!!, it, Toast.LENGTH_SHORT).show()

        })


//................................................delete Account  click set ........................................................................//
        /* mbinding.DeleteAccount.setOnClickListener {

             val mDialog = AlertDialog.Builder(context)
             mDialog.setTitle("Delete Account")
             mDialog.setMessage("Are you sure you want to delete?")
             mDialog.setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { dialog, which ->

                 //.....................................webServices delete Account.................//
                 mvmodel.deleteAccount()

             })
             mDialog.setNegativeButton(android.R.string.no, DialogInterface.OnClickListener { dialog, which ->
                 dialog.cancel()

             })
             mDialog.show()
         }*/


        //..................................Image Updated Api observer...................................................//

        //when data is sucessful


        mbinding.imageChange.setOnClickListener {

            val checkSelfPermission =
                ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA)
            if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    2
                )

                if (targetRequestCode == 2) {
                    showDialog()
                }

            } else {
                showDialog()
            }


        }
        //set the clicked of update profile
        mbinding.clicked = this
    }

//    override fun onResume() {
//        super.onResume()
//        mvmodel.mSucessful().observe(this, Observer {
//            mbinding.imageProgress.visibility = View.GONE
//            mbinding.circleImageView3.visibility = View.VISIBLE
//            mbinding.imageView8.visibility = View.VISIBLE
//            Toast.makeText(context, it.msg, Toast.LENGTH_SHORT).show()
//            //set the image
//
//            Glide.with(this)
//                .load(it.data)
//                .into(mbinding.circleImageView3)//set the image
////            Picasso.with(context).load(profileData1.profile_image).memoryPolicy(MemoryPolicy.NO_CACHE).into(mbinding.circleImageView3)
////            mbinding.circleImageView3.invalidate()
//
//
//        })
//        //2nd observer  when data is failure
//        mvmodel.mFailure().observe(this, Observer {
//            mbinding.imageProgress.visibility = View.GONE
//            mbinding.circleImageView3.visibility = View.VISIBLE
//            mbinding.imageView8.visibility = View.VISIBLE
//            Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
//
//        })
//
//    }

    //....................................Update profile image by gallary and camera.................//
    private fun showDialog() {
        val builder: androidx.appcompat.app.AlertDialog.Builder = androidx.appcompat.app.AlertDialog.Builder(activity!!)
        builder.setTitle("Open By")
        val mBinding: OpenFromCameraPermissionBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.open_from_camera_permission, null, false)

        if (check == 0) {
            mBinding.camera.isChecked = true
            mBinding.gallery.isChecked = false
        }
        if (check == 1) {
            mBinding.gallery.isChecked = false
            mBinding.camera.isChecked = true
        }
        builder.setView(mBinding.root)

        builder.setCancelable(false)

        builder.setPositiveButton("Ok") { dialog, which ->

            if (mBinding.camera.isChecked) {
                openCamera()
                check = 0


            } else if (mBinding.gallery.isChecked) {
                openGallery()
                check = 1

            }
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, which ->

        }
        builder.create()
        builder.show()

    }

    private fun openCamera() {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, 29)
    }


    private fun openGallery() {
        val intent = Intent("android.intent.action.GET_CONTENT")
        intent.type = "image/*"
        startActivityForResult(intent, 1)
    }

    //url link get to used this
    private fun getImageUri(inImage: Bitmap): File {
        val root: String = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/req_images")
        myDir.mkdirs()
        val fname = "Image_profile.jpg"
        val file = File(myDir, fname)
        if (file.exists()) {
            file.delete()
        }

        try {
            val out = FileOutputStream(file)
            inImage.compress(Bitmap.CompressFormat.JPEG, 70, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return file
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try {
            if (requestCode == 29 && data != null) {

                val contentURI: Bitmap = data.extras.get("data") as Bitmap

                try {
                    val uri = getImageUri(contentURI)
                    //mvmodel.profileImage(uri, imageProgress)
                    //mbinding.  circleImageView3!!.setImageBitmap(contentURI)// we can set the image here but we want to save the image  in the server
//..................................Api FOr Update Image....................................................................//
                    mvmodel.setImage(uri)
                    mbinding.imageProgress.visibility = View.VISIBLE
                    mbinding.circleImageView3.visibility = View.VISIBLE
                    mbinding.imageView8.visibility = View.GONE


                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(context, "Failed!", Toast.LENGTH_SHORT).show()
                }
            } else if (requestCode == 1 && data != null) {
                try {
//val extras = data.extras
                    val bitmap2: Uri = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(context!!.contentResolver, bitmap2)
                    val uri = getImageUri(bitmap)
                    //..................................Api FOr Update Image from gallery....................................................................//
                    mvmodel.setImage(uri)
                    mbinding.imageProgress.visibility = View.VISIBLE
                    mbinding.circleImageView3.visibility = View.VISIBLE
                    mbinding.imageView8.visibility = View.GONE

                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(context, "Failed!", Toast.LENGTH_SHORT).show()
                }
            } else if (requestCode == 2 && data != null) {
                try {
                    Toast.makeText(context, "lll!", Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(context, "Failed!", Toast.LENGTH_SHORT).show()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //................................API FOR UPDATE PROFILE DATA WITH PASSWORD AND NUMBER 3RD API...................................//
    override fun setclicked(name: String, password: String, number: String) {

        if (mbinding.edtname.text!!.isEmpty()) {
            Toast.makeText(context, "Please Enter The Name", Toast.LENGTH_SHORT).show()

        } else if (mbinding.edtphoneno.text!!.isEmpty()) {

            Toast.makeText(context, "Please enter the phone no", Toast.LENGTH_SHORT).show()
        } else {
            val model = UpdateProfileModel()
            model.name = name
            model.number = number
            model.password = password
            edtpassword.setText("")
            //send the data
            mbinding.progressBar3.visibility = View.VISIBLE
            mbinding.button.visibility = View.GONE
            mvmodel.setupdateProfile(model)
        }


    }


}


