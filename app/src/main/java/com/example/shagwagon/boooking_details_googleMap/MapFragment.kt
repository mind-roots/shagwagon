package com.example.shagwagon.boooking_details_googleMap


import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.*
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shagwagon.R
import com.example.shagwagon.databinding.FragmentMapBinding
import com.example.shagwagon.home_screen.Home1
import com.example.shagwagon.pending_screen.*
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.map_dialog_adapter.view.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.concurrent.schedule


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MapFragment(home1Fragment: Home1) : Fragment(), OnMapReadyCallback, LocationListener, On_Click,
    GoogleApiClient.ConnectionCallbacks {
    override fun onConnected(p0: Bundle?) {

    }

    override fun onConnectionSuspended(p0: Int) {

    }

    private var polyline: Polyline? = null
    lateinit var mbinding: FragmentMapBinding
    private lateinit var recyclerAdapter: PendingAdapter
    lateinit var mvmodel: PendingViewmodel
    var data2: Pendingmodel = Pendingmodel()


    var interfaceAddress = home1Fragment as setdataInterface3
    private var googleMap: GoogleMap? = null
    private var longitudesource: Double = 28.5383
    private var latitudesource: Double = -81.3792
    private var longitudedestination: String = "28.5383 "
    private var latitudedestination: String = "-81.3792 "
    private var markerPoints: ArrayList<LatLng> = ArrayList()
    private lateinit var lastLocation: Location
    var direction = DirectionsJSONParser(activity)
    var dialogRun = 0

    var abc = "123"
    var once = 0
    var once11 = 0.008
    var totalAmount: String = ""
    lateinit var adapter: PendingAdapter
    private var fusedLocationClient: FusedLocationProviderClient? = null

    @SuppressLint("MissingPermission")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //mbinding = FragmentCompletedScreenBinding.inflate(LayoutInflater.from(context), container, false)
        mbinding = FragmentMapBinding.inflate(LayoutInflater.from(context), container, false)
        mvmodel = ViewModelProviders.of(this).get(PendingViewmodel::class.java)


        this.mbinding.map.onCreate(savedInstanceState)
        try {
            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            mbinding.progressBarMap.visibility = View.VISIBLE
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mbinding.serviceStart.setOnClickListener {
            val sharedPreference = activity!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putString("status", "5")
            editor.commit()

            /* val intent = Intent(context, MyService::class.java)
             context?.stopService(intent)*/
            interfaceAddress.setadapterData3()
        }


        var customerName = ""
        var bookingId = ""


        val sharedPreference = context!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
        val gson: Gson = Gson()
        val json: String = sharedPreference.getString("data", "")

        if (json.isNotEmpty()) {
            val data_stored: Pendingmodel = gson.fromJson(json, Pendingmodel::class.java)
            //Log.e("Shared Preference data",data_stored.total_amount)
            latitudedestination = data_stored.latitude
            longitudedestination = data_stored.longitude
            totalAmount = data_stored.total_amount
            customerName = data_stored.customer_name
            bookingId = data_stored.booking_id

            data2 = data_stored
        } else {
            latitudedestination = arguments?.getString("latitude").toString()
            longitudedestination = arguments?.getString("longitude").toString()
            totalAmount = arguments?.getString("totalAmount").toString()
            customerName = arguments?.getString("customerName").toString()
            bookingId = arguments?.getString("booking_id").toString()
            abc = arguments?.getString("abc").toString()
            data2 = arguments?.getParcelable("data")!!

            Log.e("Map also done", data2.booking_type)

        }

        //mbinding.textView11.text = data2.customer_phone
        mbinding.textView10.text = customerName
        mbinding.textView9.text = "Booking #$bookingId"


        /* if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) !=
             PackageManager.PERMISSION_GRANTED &&
             checkSelfPermission(
                 context!!,
                 Manifest.permission.ACCESS_COARSE_LOCATION
             ) != PackageManager.PERMISSION_GRANTED
         ) {

             requestPermissions(
                 activity!!, arrayOf(
                     Manifest.permission.ACCESS_FINE_LOCATION,
                     Manifest.permission.ACCESS_COARSE_LOCATION
                 ), 101
             )


         }*/

        try {
            val locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5F, this)

        } catch (e: Throwable) {
            e.printStackTrace()
        }






        mbinding.click1 = this
        Log.e("here anything", data2.booking_type)
        /*Glide.with(activity!!)
            .load(data2.profile_image)
            .into(mbinding.profilePicture)*/

        if (data2.profile_image.isNotEmpty()) {
            Glide.with(this)
                .load(data2.profile_image)
                .into(mbinding.profilePicture)
        } else {
            Glide.with(this)
                .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                .into(mbinding.profilePicture)
        }


//        mbinding.textView11.setOnClickListener {
//
//
//
//        }
        Log.e("Map Fragment ", latitudedestination)
        Log.e("Map Fragment ", longitudedestination)
        this.mbinding.map.onResume()
        this.mbinding.map.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)
        return mbinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mbinding.click1 = this
//sucessful case
        //........................Api observer..........................//
        mvmodel.pendingSucessful().observe(this, Observer {
            if (it.size > 0) {

                recyclerAdapter.update(it)
            }
        })

        // failure case

        mvmodel.pendingFailure().observe(this, Observer {
            //   Toast.makeText(context!!, it, Toast.LENGTH_SHORT).show()

        })
    }

    override fun textView() {
        val mDialog = androidx.appcompat.app.AlertDialog.Builder(activity!!)
        mDialog.setMessage("Do you want to call")
        mDialog.setCancelable(true)

        //yes button set the api of alertv dialog
        mDialog.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
            val intent = Intent(Intent.ACTION_DIAL);
            val number = data2.customer_phone
            intent.data = Uri.parse("tel:$number")
            startActivity(intent)
        })

        mDialog.setNegativeButton("No", DialogInterface.OnClickListener { dialog, which ->
            dialog.cancel()
        })
        mDialog.show()
    }


    @SuppressLint("WrongConstant")
    override fun textViewList() {
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.map_dialog_adapter, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(activity!!)
            .setView(mDialogView)

        //
        // mDialogView.background = ColorDrawable(Color.TRANSPARENT)

        val recyclerView: RecyclerView = mDialogView.findViewById(R.id.innerrecyclerview)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


        recyclerView.adapter = MapDialogInnerAdapter(activity!!, data2)


        val textView: TextView = mDialogView.findViewById(R.id.totalAmount)
        textView.text = "$" + data2.total_amount


        val mAlertDialog = mBuilder.show()
        //mAlertDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        //set the click on the button in dialog box
        mDialogView.buttondialog1.setOnClickListener {


            //Toast.makeText(this, "Enbaled location scuessfully", Toast.LENGTH_SHORT).show()

            //dismiss dialog when be  click the button
            mAlertDialog.dismiss()
        }

    }


    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
        // Toast.makeText(activity, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show()
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onLocationChanged(location: Location?) {
/*
        val geocoder = Geocoder(context)
        val address: Address?
        try {
            // 2
            val addresses = geocoder.getFromLocation(location!!.latitude, location.longitude, 1)
            address = addresses[0]
            Log.e("Map Fragment ",address.toString())
            val builder = StringBuilder()
            builder.append(address.featureName).append(",\t")
            builder.append(address.getAddressLine(0)).append(",\t")
            resultOrigin = builder.toString()
            Log.e("Map Fragment2 ",resultOrigin)
        } catch (e: IOException) {
            Log.e("MapsActivity", e.localizedMessage)
        }*/
    }

//      <-----------------------------------2nd way for map direction passing lat and long-------------------->


    override fun onMapReady(map: GoogleMap?) {
        googleMap = map
        val mainHandler = Handler(Looper.getMainLooper())


        setupGoogleMapScreenSettings(googleMap)
        //googleMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
        googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
        try {
            if (checkSelfPermission(
                    context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                    context!!, Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // ActivityCompat.requestPermissions(getApplication(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 123);

                fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                    if (location != null) {
                        latitudesource = location.latitude
                        longitudesource = location.longitude


                        //Timer("SettingUp", false).schedule(500) {

                        setMap()
                        // }


                    } else {
                        try {
                            var shared = context!!.getSharedPreferences("Location", Context.MODE_PRIVATE)
                            latitudesource = shared.getString("latitude", "").toDouble()
                            longitudesource = shared.getString("longitude", "").toDouble()
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Toast.makeText(context, "Your Location Not found please Enable Gps", Toast.LENGTH_LONG)
                                .show()


                        }

                    }
                }
            } else {
                fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                    if (location != null) {
                        latitudesource = location.latitude
                        longitudesource = location.longitude
                        // Timer("SettingUp", false).schedule(500) {
//                            Toast.makeText(context, "", Toast.LENGTH_LONG)
//                                .show()

                        setMap()

                        // }


                    } else {
                        try {
                            val shared = context!!.getSharedPreferences("Location", Context.MODE_PRIVATE)
                            latitudesource = shared.getString("latitude", "").toDouble()
                            longitudesource = shared.getString("longitude", "").toDouble()
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Toast.makeText(context, "Your Location Not found please Enable Gps", Toast.LENGTH_LONG)
                                .show()


                        }


                    }
                }


            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    private fun setMap() {
        val lat1: Double?
        val longitude1: Double?
        val lat2: Double?
        val longitude2: Double?


        if (latitudesource != 28.5383 && longitudesource != -81.3792) {
            lat1 = latitudesource
            longitude1 = longitudesource

            lat2 = data2.latitude.toDouble()
            longitude2 = data2.longitude.toDouble()

            val latLng = LatLng(lat1, longitude1)
            val latLng1 = LatLng(lat2, longitude2)

            markerPoints.add(latLng)
            markerPoints.add(latLng1)

            val options = MarkerOptions()
            options.position(latLng1)
            /*if (markerPoints.size == 1) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
            } else if (markerPoints.size == 2) {
                //options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            }*/
            val mainHandler = Handler(Looper.getMainLooper())


            googleMap?.addMarker(options)
            // Timer("SettingUp", true).schedule(100) {
            mainHandler.post(object : Runnable {
                @SuppressLint("MissingPermission")
                override fun run() {

                    mainHandler.postDelayed(this, 60000)
//                    if (checkSelfPermission(
//                            activity!!,
//                            Manifest.permission.ACCESS_FINE_LOCATION
//                        ) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
//                            activity!!, Manifest.permission.ACCESS_COARSE_LOCATION
//                        ) != PackageManager.PERMISSION_GRANTED
//                    ) {
                    once11 = once11 + 0.001
                    fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                        if (location != null) {
                            latitudesource = location.latitude
                            longitudesource = location.longitude
                            Log.e("Latitttttttttt  ", "$latitudesource : $longitudesource")
//                    Toast.makeText(context,latitudesource.toString()+"  :  "+longitudesource.toString(),Toast.LENGTH_SHORT).show()
                        } else {
                            try {
                                var shared = context!!.getSharedPreferences("Location", Context.MODE_PRIVATE)
                                latitudesource = shared.getString("latitude", "").toDouble()
                                longitudesource = shared.getString("longitude", "").toDouble()
                                Log.e("Latitttttttttt22  ", "$latitudesource : $longitudesource")
                            } catch (e: Exception) {
                                e.printStackTrace()

                            }
//                    Toast.makeText(context,latitudesource.toString()+"  :  "+longitudesource.toString(),Toast.LENGTH_SHORT).show()

//                            }
                        }
                    }
                    val latLng = LatLng(latitudesource, longitudesource)
                    val latLng1 = LatLng(lat2, longitude2)
                    markerPoints.clear()
                    markerPoints.add(latLng)
                    markerPoints.add(latLng1)

                    if (markerPoints.size >= 2) {
                        val origin = markerPoints[0]
                        val dest = markerPoints[1]

                        // Getting URL to the Google Directions API
                        val url = getDirectionsUrl(origin, dest)



                        direction.isNetworkConnected(context)

                        try {
                            if (direction.isNetworkConnected(context)) {

                                val downloadTask = DownloadTask()

                                // Start downloading json data from Google Directions API
                                downloadTask.execute(url)
                            }

                        } catch (e: Exception) {

                        }
                    }
                }
            })
            getEndLocationTitle(googleMap)
            //}
        }

    }

    private fun getEndLocationTitle(map: GoogleMap?) {
        try {
            val shared = context!!.getSharedPreferences("Map", Context.MODE_PRIVATE)
            val address = shared.getString("address", "")

            val latLng = LatLng(latitudedestination.toDouble(), longitudedestination.toDouble())

            if (address != "") {

                map?.addMarker(
                    MarkerOptions().position(latLng)
                        .title(address)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private inner class DownloadTask : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg url: String): String {

            var data = ""

            try {
                data = downloadUrl(url[0])
            } catch (e: Exception) {
                Log.d("Background Task", e.toString())
            }



            return data
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            try {
                val parserTask = ParserTask()
                parserTask.execute(result)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    private inner class ParserTask : AsyncTask<String, Int, List<List<HashMap<String, String>>>>() {

        // Parsing the data in non-ui thread
        override fun doInBackground(vararg jsonData: String): List<List<HashMap<String, String>>>? {

            val jObject: JSONObject
            var routes: List<List<HashMap<String, String>>>? = null

            try {
                jObject = JSONObject(jsonData[0])
                val parser = DirectionsJSONParser(activity)

                routes = parser.parse(jObject)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return routes
        }

        override fun onPostExecute(result: List<List<HashMap<String, String>>>) {
            var points: ArrayList<LatLng>
            var lat = 28.5383
            var lng = -81.3792
            var lineOptions: PolylineOptions? = null

            for (i in result.indices) {

                points = ArrayList()
                lineOptions = PolylineOptions()

                val path = result[i]

                for (j in path.indices) {
                    val point = path[j]

                    lat = java.lang.Double.parseDouble(point["lat"]!!)
                    lng = java.lang.Double.parseDouble(point["lng"]!!)
                    val position = LatLng(lat, lng)

                    points.add(position)
                }

                lineOptions.addAll(points)
                lineOptions.width(12f)
                lineOptions.color(Color.RED)
                lineOptions.geodesic(true)

            }


            if (lineOptions != null) {
                polyline?.remove()
                polyline = googleMap?.addPolyline(lineOptions)
                if (once == 0) {
                    once = 1
                    googleMap?.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                lat, lng
                            ), 8F
                        )
                    )
                }
                try {
                    mbinding.progressBarMap.visibility = View.INVISIBLE
                    val shared = context!!.getSharedPreferences("Map", Context.MODE_PRIVATE)
                    val time = shared.getString("time", "")

                    if (time != "") {
                        val a = time.replace("hours", "h")
                        val b = a.replace("mins", "m")
                        mbinding.textView12.text = b
                        mbinding.textView12.text = time
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else {
                mbinding.progressBarMap.visibility = View.INVISIBLE
                googleMap?.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            lat, lng
                        ), 7F
                    )
                )
                mbinding.textView12.text = R.string.time.toString()
                if (dialogRun == 0) {
                    dialogRun = 1
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("No route exist")
                    builder.setPositiveButton("Ok")
                    { dialog, which ->
                        dialog.dismiss()
                        // Close all activites

//                    finish()
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }

        }


        // Drawing polyline in the Google Map for the i-th route


    }

    private fun getDirectionsUrl(origin: LatLng, dest: LatLng): String {

        // Origin of route
        val strOrigin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val strDest = "destination=" + dest.latitude + "," + dest.longitude

        // Sensor enabled
        val sensor = "sensor=false"
        val mode = "mode=driving"
        val key = "key=AIzaSyCL6OAjSuN_k848r8KRC_VHJT4ft9N2rus"
        // Building the parameters to the web service
        val parameters = "$strOrigin&$strDest&$sensor&$mode&$key"

        // Output format
        val output = "json"

        // Building the url to the web service


        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters"
    }

    /**
     * A method to download json data from url
     */
    @Throws(IOException::class)
    private fun downloadUrl(strUrl: String): String {
        var data = ""
        var iStream: InputStream? = null
        var urlConnection: HttpURLConnection? = null
        try {
            val url = URL(strUrl)

            urlConnection = url.openConnection() as HttpURLConnection

            urlConnection.connect()

            iStream = urlConnection.inputStream

            val br = BufferedReader(InputStreamReader(iStream))

            data = direction.readData(br)

            br.close()

        } catch (e: Exception) {
            Log.d("Exception", e.toString())
        } finally {
            iStream!!.close()
            urlConnection!!.disconnect()
        }
        return data
    }

    @SuppressLint("MissingPermission")
    private fun setupGoogleMapScreenSettings(googleMap: GoogleMap?) {
        googleMap?.isBuildingsEnabled = true
        googleMap?.isIndoorEnabled = true
        googleMap?.isTrafficEnabled = true
        try {
            googleMap?.isMyLocationEnabled = true
        } catch (e: Exception) {

        }


        val mUiSettings = googleMap?.uiSettings
        //mUiSettings?.isZoomControlsEnabled = true
        mUiSettings?.isCompassEnabled = true
        mUiSettings?.isMyLocationButtonEnabled = true
        mUiSettings?.isMyLocationButtonEnabled = true
        mUiSettings?.isScrollGesturesEnabled = true
        mUiSettings?.isZoomGesturesEnabled = true
        mUiSettings?.isTiltGesturesEnabled = true
        mUiSettings?.isRotateGesturesEnabled = true
        mUiSettings?.isMapToolbarEnabled = true
        mUiSettings?.isMapToolbarEnabled = true
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()

        try {
            val locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5F, this)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        mbinding.map.onResume()

    }

    override fun onPause() {
        super.onPause()
        mbinding.map.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mbinding.map.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mbinding.map.onLowMemory()
    }

    interface setdataInterface3 {

        fun setadapterData3()


    }

}





