package com.example.shagwagon.boooking_details_googleMap

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.R
import com.example.shagwagon.databinding.MapDialogInnerAdapterBinding
import com.example.shagwagon.databinding.PendingInnerAdapterBinding
import com.example.shagwagon.pending_screen.InnerModel
import com.example.shagwagon.pending_screen.PendingInnerAdapter
import com.example.shagwagon.pending_screen.Pendingmodel
import kotlinx.android.synthetic.main.map_dialog_inner_adapter.view.*
import kotlinx.android.synthetic.main.pending_inner_adapter.view.*
import kotlinx.android.synthetic.main.pending_inner_adapter.view.textView

class MapDialogInnerAdapter(var context: Context, var data: Pendingmodel) :
    RecyclerView.Adapter<MapDialogInnerAdapter.ViewHolder>() {

    //lateinit var mbinding: MapDialogInnerAdapterBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var view = LayoutInflater.from(context).inflate(R.layout.map_dialog_inner_adapter, parent, false)
        //mbinding = MapDialogInnerAdapterBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.modelcategories!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = data.modelcategories!![position]

        holder.item.textView.text = data.name
        holder.item.textView1.text = "$" + data.price


        //holder.bind(data)

    }


    class ViewHolder(var item: View) : RecyclerView.ViewHolder(item) {
//        fun bind(data: InnerModel) {
//
//            mbinding.data = data
//
//            mbinding.executePendingBindings()
//
//
//        }

    }


}