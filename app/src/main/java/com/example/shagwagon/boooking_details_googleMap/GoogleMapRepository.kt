package com.example.shagwagon.boooking_details_googleMap

import android.app.Application
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class GoogleMapRepository(var application: Application) {

    var msucessful = MutableLiveData<GoogleApiModel>()
    var mfailure = MutableLiveData<String>()

    fun setlocation(ctx : Context,latitude: Double, longitude: Double) {

        addlocation(ctx,latitude, longitude)
    }


    fun addlocation(ctx:Context,latitude: Double, longitude: Double) {

        //add the authcode
        val auth = Constants.getPrefs(ctx).getString(Constants.token, "")


        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.location(auth, latitude.toString(), longitude.toString())
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val response = response.body()!!.string()
                        val json = JSONObject(response)  //getting the data from the object
                        val model = GoogleApiModel()
                        val status = json.optString("status")
                        val msg = json.optString("msg")

                        if (status == "true") {

                            model.status = status
                            model.msg = msg
                            msucessful.value = model

                        } else {

                            mfailure.value = msg

                        }


                    } catch (e: Exception) {
                        e.printStackTrace()

                    }

                } else {

                    var msg = "Response Unsucessful"
                    mfailure.value = msg

                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                val msg = "Network Error Please Check Your Network"
                mfailure.value = msg

            }
        })


    }

    fun mSucessful(): MutableLiveData<GoogleApiModel> {

        return msucessful
    }

    fun mFailure(): MutableLiveData<String> {

        return mfailure
    }
}
