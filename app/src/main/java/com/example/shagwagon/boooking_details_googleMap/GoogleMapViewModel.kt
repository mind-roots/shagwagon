package com.example.shagwagon.boooking_details_googleMap

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class GoogleMapViewModel(application: Application) : AndroidViewModel(application) {

    val repo = GoogleMapRepository(application)


    fun setlocation(ctx : Context,latitude: Double, longitude: Double) {

        repo.setlocation(ctx,latitude, longitude)
    }


    //send the data to the activity of sucessful
    fun mSucessful(): MutableLiveData<GoogleApiModel> {

        return repo.mSucessful()
    }


    //send the data to the activity of failure
    fun mFailure(): MutableLiveData<String> {

        return repo.mFailure()
    }

}
