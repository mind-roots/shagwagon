package com.example.shagwagon.boooking_details_googleMap

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.databinding.MapDialogAdapterBinding
import com.example.shagwagon.pending_screen.PendingInnerAdapter
import com.example.shagwagon.pending_screen.Pendingmodel

class MapDialogAdapter(var context: FragmentActivity,
                       mapFragment: MapFragment)  :
RecyclerView.Adapter<MapDialogAdapter.ViewHolder>() {
    var it = ArrayList<Pendingmodel>()
    lateinit var mbinding: MapDialogAdapterBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mbinding = MapDialogAdapterBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(mbinding)

    }

    override fun getItemCount(): Int {
        return it.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = it[position]
        //send the data
        holder.bind(data)

        var manager = LinearLayoutManager(context!!)

//        mbinding.innerrecyclerview.layoutManager = manager
//        mbinding.innerrecyclerview.adapter = MapDialogInnerAdapter(context!!, data)
    }

    class ViewHolder(var mbinding: MapDialogAdapterBinding) : RecyclerView.ViewHolder(mbinding.root) {
        fun bind(data: Pendingmodel) {

            mbinding.data = data
            mbinding.executePendingBindings()
        }


    }

    fun update(size: ArrayList<Pendingmodel>) {

        it = size
        notifyDataSetChanged()

    }

}