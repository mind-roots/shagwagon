package com.example.shagwagon.main_activity_parent

import android.app.Application
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class FirebaseRepository(var application: Application) {
    var success = MutableLiveData<Boolean>()


    fun setdata(deviceId : String) {
        getData(deviceId)
    }
    fun getData(deviceId: String) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()
        val auth = Constants.getPrefs(application as Context).getString(Constants.token, "")

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.RegisterDevice(auth, deviceId,"1","2")
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val ratingModel = RatingModel()
                    val resp = response.body()!!.string()
                    val json = JSONObject(resp)
                    val status = json.optString("status")
                }
                else{

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }
        })
    }
}