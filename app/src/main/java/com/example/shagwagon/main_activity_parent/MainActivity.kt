package com.example.shagwagon.main_activity_parent

import android.Manifest
import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.content.*
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.example.shagwagon.home_screen.HomeFragment
import com.example.shagwagon.setting_screen.Setting_Fragment
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.shagwagon.utils.DriverTimeOut
import com.example.shagwagon.utils.DriverTimeViewModel
import com.example.shagwagon.utils.MyService
import com.example.shagwagon.R
import com.example.shagwagon.completed_screen.Completed_Screen_Fragment
import com.example.shagwagon.completed_screen.ModelSearch
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.databinding.ActivityMainBinding
import com.example.shagwagon.home_screen.Home1
import com.example.shagwagon.home_screen.RatingThemeActivity
import com.example.shagwagon.pending_screen.PendingViewmodel
import com.example.shagwagon.pending_screen.Pending_Fragment
import com.example.shagwagon.pending_screen.Pendingmodel
import com.example.shagwagon.sign_in.RatingCheckViewModel
import com.example.shagwagon.sign_in.Sign_in_Activity
import com.example.shagwagon.sign_in.SignupModel
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.seprate_alertdialog.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, DrawerLayout.DrawerListener,
    OnMapReadyCallback,
    HomeFragment.setdataInterfaceMain, Completed_Screen_Fragment.completedData, Pending_Fragment.pendingData,
    Home1.updateCompleted, LifecycleObserver {
    //var  permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
    var offset = 0
    public var completeArray = ArrayList<ModelSearch>()

    companion object {
        var completeArray = ArrayList<ModelSearch>()
        var pendingArray: ArrayList<Pendingmodel> = ArrayList()
        public var isAppRunning: Boolean = true
        var pendingSize = 0
    }

    override fun onDrawerStateChanged(newState: Int) {


        if (newState == 2) {

            //set the data
            val getprefs = Constants.getPrefs(application).getString(Constants.profile, "")
            val gson = Gson()
            val json = gson.fromJson(getprefs, SignupModel::class.java)
            tvUserName.text = json.name
            ratingBar.rating = json.rating.toFloat()

            vehicle_infos.text = json.vehicle_info
            // Picasso.with(this).load(json.profile_image).into(image)

            if (json.profile_image.isNotEmpty()) {
                Glide.with(this)
                    .load(json.profile_image)
                    .into(image)
            } else {
                Glide.with(this)
                    .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                    .into(image)
            }


        }
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {

    }

    override fun onDrawerOpened(drawerView: View) {

    }

    private lateinit var lastLocation: Location
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private lateinit var childFragment: Fragment
    private lateinit var fragment1: Fragment
    lateinit var mvmodel: DrawerViewmodel
    lateinit var mvmodelDrive: DriverTimeViewModel
    lateinit var mvmodelRating: RatingCheckViewModel
    lateinit var mvmodelPending: PendingViewmodel

    lateinit var mbinding: ActivityMainBinding
    lateinit var image: ImageView
    lateinit var tvUserName: TextView
    lateinit var location: LocationRequest
    lateinit var ratingBar: RatingBar

    lateinit var vehicle_infos: TextView
    lateinit var manger: LocationManager

    private lateinit var actionbar: ActionBar
    private lateinit var navView: NavigationView
    lateinit var textview: TextView//set the text for toolbar
    private var googleApiClient: GoogleApiClient? = null
    var edit = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        isAppRunning = true
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mvmodel = ViewModelProviders.of(this).get(DrawerViewmodel::class.java)
        mvmodelDrive = ViewModelProviders.of(this).get(DriverTimeViewModel::class.java)
        mvmodelRating = ViewModelProviders.of(this).get(RatingCheckViewModel::class.java)
        mvmodelPending = ViewModelProviders.of(this).get(PendingViewmodel::class.java)


        Handler().postDelayed({
            mvmodelRating.setData()
        }, 1000)



        mvmodelRating.pendingSucessful().observe(this, Observer {
            if (it.status == "true") {
                Log.e("MainActivity", "Rating Response Left")
                Log.e("MainActivity", it.booking_id)
                val intent = Intent(this, RatingThemeActivity::class.java)
                intent.putExtra("booking_id", it.booking_id)
                intent.putExtra("driver_id", it.driver_id)
                intent.putExtra("customer_id", it.customer_id)
                intent.putExtra("customer_name", it.customer_name)
                intent.putExtra("profile_image", it.profile_image)
                startActivity(intent)
            } else {
                Log.e("MainActivity", "No Rating Response")
            }
        })


//        if (manger.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            Toast.makeText(this, "GPS is Enabled in your device", Toast.LENGTH_SHORT).show();
//        } else {
//            setdialogbox()
//        }
        var requiredPermission = "android.permission.ACCESS_FINE_LOCATION";
        var checkVal = checkCallingOrSelfPermission(requiredPermission);
        if (checkVal != PackageManager.PERMISSION_GRANTED) {
            setdialogbox()
            setUpMap()
        }


        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar//first step start
        title = " "
        textview = toolbar.findViewById(R.id.toolbartxt)
        textview.text = "Home"
        setSupportActionBar(toolbar)
        setSupportActionBar(toolbar)

//method of storing the data in the nav header
        NavHeaderData()
//set by default layout when  the screen is open


        //set notification intent

        try {


            if (intent.getStringExtra("booking").isNotEmpty()) {

                pendingArray.clear()
                val bundle = Bundle()
                bundle.putString("booking", "booking")

                val fragmentLoad = HomeFragment()
                fragmentLoad.arguments = bundle
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.frame, fragmentLoad)
                childFragment = Pending_Fragment()
                transaction.commit()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }



        try {

            if (intent.getStringExtra("noType").isNotEmpty()) {
                pendingArray.clear()
                val bundle = Bundle()
                bundle.putString("booking", "booking")
                val fragmentLoad = HomeFragment()
                fragmentLoad.arguments = bundle
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.frame, fragmentLoad)
                childFragment = Pending_Fragment()
                transaction.commit()
            }
        } catch (e: Exception) {
        }


        try {


            if (intent.getStringExtra("reLoad").isNotEmpty()) {

                val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                nMgr.cancelAll();

                pendingArray.clear()

                mvmodelPending.getdata()
                val bundle = Bundle()
                bundle.putString("booking", "booking")

                val fragmentLoad = HomeFragment()
                fragmentLoad.arguments = bundle
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.frame, fragmentLoad)
                childFragment = Pending_Fragment()
                transaction.commit()

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


        /*try {
            if(intent.getStringExtra("foreground").isNotEmpty()){
                val builder = AlertDialog.Builder(this)
                //set title for alert dialog
                builder.setTitle("Alert")
                //set message for alert dialog
                builder.setMessage("You have received a new Booking Update")
                builder.setIcon(android.R.drawable.ic_dialog_alert)

                //performing positive action
                builder.setPositiveButton("Yes") { dialogInterface, which ->


                    val bundle = Bundle()
                    bundle.putString("booking", "booking")

                    val fragmentLoad = HomeFragment()
                    fragmentLoad.arguments = bundle
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.frame, fragmentLoad)
                    childFragment=Pending_Fragment()
                    transaction.commit()


                }
                //performing negative action
                builder.setNegativeButton("No") { dialogInterface, which ->
                    //  Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
                }
                // Create the AlertDialog
                val alertDialog: AlertDialog = builder.create()
                // Set other dialog properties
                alertDialog.setCancelable(false)
                alertDialog.show()
            }
        }catch (e: Exception){
            e.printStackTrace()
        }*/


        //set service status
        var a = getSharedPreferences("Service", Context.MODE_PRIVATE)
        var b = a.getString("serviceStatus", "")
        if (b == "2") {
            edit = 0
//            val sharedPreference = getSharedPreferences("Service", Context.MODE_PRIVATE)
//            var editor = sharedPreference.edit()
//
//            editor.clear()
//            editor.putString("serviceStatus", "1")
//            editor.commit()
            inflaterTransection(HomeFragment())

        } else if (b == "") {
            edit = 1
//            val sharedPreference = getSharedPreferences("Service", Context.MODE_PRIVATE)
//            var editor = sharedPreference.edit()
//            editor.clear()
//            editor.putString("serviceStatus", "2")
//            editor.commit()
            inflaterTransection(DriverTimeOut())
            childFragment = DriverTimeOut()

        } else {
            edit = 1
            val sharedPreference = getSharedPreferences("Service", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.clear()
            editor.putString("serviceStatus", "2")
            editor.commit()
            inflaterTransection(DriverTimeOut())
            childFragment = DriverTimeOut()
        }

        //set the dialog box enableLoc()


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.navView)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        actionbar = this.supportActionBar!!
        actionbar.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_white)
            //   drawerLayout.setDrawerListener(SetDr)

        }


        mbinding.drawerLayout.map.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        setUpMap()
        navView.setNavigationItemSelectedListener(this)
        drawerLayout.addDrawerListener(this)

        //...............................logout webservices observer...............................................//

        mvmodel.success().observe(this, Observer {


            Toast.makeText(application, " Logout successfully", Toast.LENGTH_SHORT).show()


            val edit = Constants.getPrefs(this).edit()
            edit.clear()
            edit.apply()

            val i = Intent(this, Sign_in_Activity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
            this.finish()

        })


    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults.contains(-1)) {

                Toast.makeText(this, "Please accept permission", Toast.LENGTH_LONG).show()
                Handler().postDelayed({ setdialogbox() }, 1000)


            } else {
                //do something someday
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        isAppRunning = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        Log.e("Yeeey", "App in foreground")
        isAppRunning = true
    }

    override fun onStop() {
        super.onStop()
        isAppRunning = true
    }


    override fun onDestroy() {
        super.onDestroy()
        isAppRunning = false
    }

    override fun onMapReady(p0: GoogleMap?) {


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 101) {

        } else {

        }
    }

    //enabled  Alert dialog show
    fun setdialogbox() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.location_dialog, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mDialogView.buttondialog.setOnClickListener {
            enableLocation()
            setUpMap()
            //dismiss dialog when be  click the button
            mAlertDialog.dismiss()
        }

    }

    // header data store
    private fun NavHeaderData() {
        navView = findViewById(R.id.navView)
        val headerView: View = navView.getHeaderView(0)
        tvUserName = headerView.findViewById(R.id.tvUserName)
        ratingBar = headerView.findViewById(R.id.rating)
        vehicle_infos = headerView.findViewById(R.id.vehicle_infos)
        image = headerView.findViewById(R.id.userimage)
        // vehicle_infonumber =  headerView.findViewById(R.id.vehicle_infonumber)


        //set the data


        val getprefs = Constants.getPrefs(application).getString(Constants.profile, "")
        val gson = Gson()
        val json = gson.fromJson(getprefs, SignupModel::class.java)


        try {

            val shared = getSharedPreferences("Status", Context.MODE_PRIVATE)
            val g: Gson = Gson()
            val j: String? = shared?.getString("data", "")
            if (j != "") {
                val data_stored: Pendingmodel = g.fromJson(j, Pendingmodel::class.java)
                val editor = Constants.getPrefs(application).edit()
                json.rating = data_stored.driver_rating
                json.profile_image = json.profile_image
                json.contact_number = json.contact_number
                json.created_at = json.created_at
                json.driver_lat = json.driver_lat
                json.driver_long = json.driver_long
                json.edtemail = json.edtemail
                json.edtpassword = json.edtpassword
                json.email = json.email
                json.id = json.id
                json.last_updated = json.last_updated
                json.modified_at = json.modified_at
                json.msg = json.msg
                json.name = json.name
                json.password = json.password
                json.re_login = json.re_login
                json.service_ids = json.service_ids
                json.status = json.status
                json.statuss = json.statuss
                json.type = json.type
                json.vehicle_info = json.vehicle_info

                //save the data by edit fun
                val gson1 = Gson()
                val json1 = gson1.toJson(json)

                Constants.getPrefs(application).edit()
                    .putString(Constants.profile, json1)
                    .apply()
                ratingBar.rating = data_stored.driver_rating.toFloat()
            } else {
                ratingBar.rating = json.rating.toFloat()
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }




        tvUserName.text = json.name

        vehicle_infos.text = json.vehicle_info
        if (json.profile_image.isNotEmpty()) {
            Glide.with(this)
                .load(json.profile_image)
                .into(image)
        } else {
            Glide.with(this)
                .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                .into(image)
        }
    }


    private fun enableLocation(): Boolean {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 101
            )
            fusedLocationClient?.lastLocation?.addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.
                // 3
                if (location != null) {
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    Log.e("MainActLocation", currentLatLng.toString())
                    val sharedPreference = this.getSharedPreferences("Location", Context.MODE_PRIVATE)
                    var editor = sharedPreference.edit()
                    editor?.putString("latitude", location.latitude.toString())
                    editor?.putString("longitude", location.longitude.toString())
                    editor?.commit()
                }

                if (location == null) {

                    Toast.makeText(this, "Please Enable Gps", Toast.LENGTH_LONG).show()
                    var intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            }




            return true


        } else {
            return false
        }


    }


    // .............used for enable the fone location acess open or denied..................//
    private fun enableLoc(boolean: Boolean) {


        if (googleApiClient == null) {

            googleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API!!)
                .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                    override fun onConnected(bundle: Bundle?) {

                    }

                    override fun onConnectionSuspended(i: Int) {
                        googleApiClient!!.connect()
                    }
                })
                .addOnConnectionFailedListener { connectionResult ->
                    Log.d(
                        "Location error",
                        "Location error " + connectionResult.errorCode
                    )
                }
                .build()
            googleApiClient!!.connect()

            val locationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = (30 * 1000).toLong()
            locationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

            builder.setAlwaysShow(true)

            val result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
            result.setResultCallback { result ->
                val status = result.status
                when (status.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this@MainActivity, 1)


                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                }
            }
        }

    }


    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {


            val builder = AlertDialog.Builder(this)
            //set title for alert dialog
            builder.setTitle("Alert")
            builder.setMessage("Are you sure you want to exit?")
            builder.setIcon(android.R.drawable.ic_dialog_alert)
            builder.setPositiveButton("Yes") { dialogInterface, which ->
                super.onBackPressed()
            }
            builder.setNegativeButton("No") { dialog, which ->
                dialog.cancel()
            }
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.


        when (item.itemId) {
            R.id.setting -> {
                inflaterTransection(Setting_Fragment())
                textview.text = "Settings"
                childFragment = Setting_Fragment()

            }
            R.id.help -> {


                val mailto = "mailto:bob@example.org" +
                        "?cc=" + "alice@example.com" +
                        "&subject=" + Uri.encode("Help") +
                        "&body=";

                val emailIntent = Intent(Intent.ACTION_SENDTO)
                emailIntent.data = Uri.parse(mailto)

                try {
                    startActivity(emailIntent);
                } catch (e: Exception) {

                }


            }
            R.id.privacy -> {

            }
            R.id.termsconditn -> {

            }
            R.id.logout -> {

                var a_status = getSharedPreferences("Service", Context.MODE_PRIVATE)
                var status = a_status.getString("serviceStatus", "")

                if (status == "2") {

                    Toast.makeText(this, "Please Time out before Logging out", Toast.LENGTH_SHORT).show()

                } else {
                    logout()

                }

            }
            R.id.home -> {

                if (childFragment !is DriverTimeOut) {

                    inflaterTransection(HomeFragment())
                    textview.text = "Home"
                    childFragment = HomeFragment()

                }


            }


        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        //getting date and time for driver time out repository
        var calander = Calendar.getInstance();
        var time = SimpleDateFormat("HH:mm:ss", Locale.UK)
        var time1 = time.format(calander.time)
        var cDate: Date = Date();
        var fDate: String = SimpleDateFormat("yyyy-MM-dd").format(cDate)
        var timeDate = "$fDate $time1"
        if (item!!.itemId == R.id.time_in) {

            var a_status = getSharedPreferences("Service", Context.MODE_PRIVATE)
            var status = a_status.getString("serviceStatus", "")
            if (edit == 0) {
                var a = getSharedPreferences("Status", Context.MODE_PRIVATE)
                var b = a.getString("status", "")
                edit = 1
                if (b == "5" || b == "4") {
                    edit = 0

                    val builder = AlertDialog.Builder(this)
                    //set title for alert dialog
                    builder.setTitle("Alert")
                    builder.setMessage("Please complete the current Service")
                    builder.setIcon(android.R.drawable.ic_dialog_alert)
                    builder.setPositiveButton("Ok") { dialogInterface, which ->
                    }
                    val alertDialog: AlertDialog = builder.create()
                    // Set other dialog properties
                    alertDialog.setCancelable(false)
                    alertDialog.show()

                } else {
                    edit = 1
                    invalidateOptionsMenu()
                    mvmodelDrive.setData("", timeDate)
                    inflaterTransection(DriverTimeOut())
                    textview.text = "Home"
                    childFragment = DriverTimeOut()
                    val sharedPreference = getSharedPreferences("Service", Context.MODE_PRIVATE)
                    var editor = sharedPreference.edit()
                    editor.clear()
                    editor.putString("serviceStatus", "1")
                    editor.commit()
                    val intent = Intent(this, MyService::class.java)
                    stopService(intent)
                }
            } else if (edit == 1) {
                edit = 0
                invalidateOptionsMenu()
                mvmodelDrive.setData(timeDate, "")
                inflaterTransection(HomeFragment())
                textview.text = "Home"
                childFragment = Home1()

                val sharedPreference = getSharedPreferences("Service", Context.MODE_PRIVATE)
                var editor = sharedPreference.edit()
                editor.clear()
                editor.putString("serviceStatus", "2")
                editor.commit()
                val intent = Intent(this, MyService::class.java)
                startService(intent)

            }
            //update = this as UpdatingCartFromWishlist
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        super.onPause()
        isAppRunning = true
    }


    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {

        var a_status = getSharedPreferences("Service", Context.MODE_PRIVATE)
        var status = a_status.getString("serviceStatus", "")
        if (edit == 0) {
            menu?.getItem(0)?.icon = getDrawable(R.drawable.ic_time_out)
        } else if (edit == 1) {
            menu?.getItem(0)?.icon = getDrawable(R.drawable.ic_time_in)
        }

        return super.onPrepareOptionsMenu(menu)
    }

    override fun setChildFragment(fragment: Fragment) {
        childFragment = fragment

    }

    override fun onResume() {
        super.onResume()
        isAppRunning = true




        try {
            if (intent.getStringExtra("foreground").isNotEmpty()) {
                val builder = AlertDialog.Builder(this)
                //set title for alert dialog
                builder.setTitle("Alert")
                //set message for alert dialog
                builder.setMessage("You have received a new Booking Update")
                builder.setIcon(android.R.drawable.ic_dialog_alert)

                //performing positive action
                builder.setPositiveButton("Yes") { dialogInterface, which ->


                    val bundle = Bundle()
                    bundle.putString("booking", "booking")

                    val fragmentLoad = HomeFragment()
                    fragmentLoad.arguments = bundle
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.frame, fragmentLoad)
                    childFragment = Pending_Fragment()
                    transaction.commit()


                }
                //performing negative action
                builder.setNegativeButton("No") { dialogInterface, which ->
                    //  Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
                }
                // Create the AlertDialog
                val alertDialog: AlertDialog = builder.create()
                // Set other dialog properties
                alertDialog.setCancelable(false)
                alertDialog.show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


        //  var ff=HomeFragment(Home1());
        try {
            if (childFragment == null) {
                childFragment = Home1()
            }
        } catch (e: Exception) {
            childFragment = Home1()
        }


        try {
            if (childFragment is Setting_Fragment) {
                inflaterTransection(Setting_Fragment())
            }

            if (childFragment is DriverTimeOut) {
                inflaterTransection(DriverTimeOut())
            }

            if (childFragment is Home1) {
                inflaterTransection(HomeFragment())
            }

            if (childFragment is Pending_Fragment) {

                val bundle = Bundle()
                bundle.putString("booking", "booking")

                val fragmentLoad = HomeFragment()
                fragmentLoad.arguments = bundle
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.frame, fragmentLoad)
                childFragment = Pending_Fragment()
                transaction.commit()
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }


        val getprefs = Constants.getPrefs(application).getString(Constants.profile, "")
        val gson = Gson()
        val json = gson.fromJson(getprefs, SignupModel::class.java)


        try {

            val shared = getSharedPreferences("Status", Context.MODE_PRIVATE)
            val g: Gson = Gson()
            val j: String? = shared?.getString("data", "")
            if (j != "") {
                val data_stored: Pendingmodel = g.fromJson(j, Pendingmodel::class.java)
                val editor = Constants.getPrefs(application).edit()
                json.rating = data_stored.driver_rating
                json.profile_image = json.profile_image
                json.contact_number = json.contact_number
                json.created_at = json.created_at
                json.driver_lat = json.driver_lat
                json.driver_long = json.driver_long
                json.edtemail = json.edtemail
                json.edtpassword = json.edtpassword
                json.email = json.email
                json.id = json.id
                json.last_updated = json.last_updated
                json.modified_at = json.modified_at
                json.msg = json.msg
                json.name = json.name
                json.password = json.password
                json.re_login = json.re_login
                json.service_ids = json.service_ids
                json.status = json.status
                json.statuss = json.statuss
                json.type = json.type
                json.vehicle_info = json.vehicle_info

                //save the data by edit fun
                val gson1 = Gson()
                val json1 = gson1.toJson(json)

                Constants.getPrefs(application).edit()
                    .putString(Constants.profile, json1)
                    .apply()
                ratingBar.rating = data_stored.driver_rating.toFloat()
            } else {
                ratingBar.rating = json.rating.toFloat()
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }


        //var fragments:Fragment= HomeFragment.getFragment()

    }

    internal fun inflaterTransection(fragment: Fragment) {
        fragment1 = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame, fragment)
        transaction.commit()

    }


//this method is used to hide the keyboard when we open the navigation drawer

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)

    }


//.........................Logout Api...................................................//

    fun logout() {

        val mDialog = AlertDialog.Builder(this)
        mDialog.setTitle("Logout")
        mDialog.setMessage("Are you sure you want to logout?")
        Log.e("hello", "hello")

        //yes button set the api of alertv dialog
        mDialog.setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { dialog, which ->


            mvmodel.setdata()


        })

        mDialog.setNegativeButton(android.R.string.no, DialogInterface.OnClickListener { dialog, which ->
            dialog.cancel()

        })
        mDialog.show()


    }

    @SuppressLint("MissingPermission")
    private fun setUpMap() {
        var requiredPermission = "android.permission.ACCESS_FINE_LOCATION";
        var checkVal = checkCallingOrSelfPermission(requiredPermission);
        if (checkVal == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient?.lastLocation?.addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.
                // 3
                if (location != null) {
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    Log.e("MainActLocation", currentLatLng.toString())
                    val sharedPreference = this.getSharedPreferences("Location", Context.MODE_PRIVATE)
                    var editor = sharedPreference.edit()
                    editor?.putString("latitude", location.latitude.toString())
                    editor?.putString("longitude", location.longitude.toString())
                    editor?.commit()

                    //placeMarkerOnMap(currentLatLng)//set the marker on the current location
                    // googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16f))

                    //.......................................API SENDING THE DATA.........................................//

                    // mvmodel.setlocation(currentLatLng.latitude, currentLatLng.longitude)//set the current location
                }

                if (location == null) {


                    Toast.makeText(this, "Please Enable Gps", Toast.LENGTH_LONG).show()
                    var intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent)
                }
            }
            return
        }
// if (ActivityCompat.checkSelfPermission(
//                this,
//                android.Manifest.permission.ACCESS_FINE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED
//        ) {
//            ActivityCompat.requestPermissions(
//                this,
//                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
//                UserPermission.LOCATION_PERMISSION_REQUEST_CODE
//            )
//
//            return
//        }
        //by default eabled the location on google map
        //googleMap!!.isMyLocationEnabled = false

// 2
    }

    override fun sendingCompletedData(modelSearch: ModelSearch) {
        //completeArray.clear()
        if (completeArray.size > 0) {
            var model = ModelSearch()
            model.count = modelSearch.count
            model.status = modelSearch.status
            for (i in 0 until modelSearch.list.size) {
                completeArray[0].list.add(modelSearch.list[i])
            }
            model.list = completeArray[0].list
            // completeArray.clear()
            completeArray.add(model)
        } else {
            completeArray.clear()
            completeArray.add(modelSearch)
        }
    }

    override fun getData(): ModelSearch {
        if (completeArray.size > 0)
            return completeArray[0]
        else
            return ModelSearch()
    }

    override fun getOff(): Int {
        return offset
    }

    override fun updateOff() {
        offset++
    }

    override fun sendingPendingData(pendingmodel: ArrayList<Pendingmodel>) {
        pendingArray.clear()

        for (i in 0 until pendingmodel.size)
            pendingArray.add(pendingmodel[i])

        if (fragment1 is HomeFragment) {
            (fragment1 as HomeFragment).updateSize(pendingArray.size)
        }

    }

    override fun getPendingData(): ArrayList<Pendingmodel> {

        pendingSize = pendingArray.size
        if (pendingArray.size > 0)
            return pendingArray
        else
            return ArrayList()
    }

    override fun getPendingSize(): Int {
        return pendingArray.size
    }

    override fun updatePendingArray(data: Pendingmodel) {
        var pendingArray1: ArrayList<Pendingmodel> = ArrayList()
        pendingArray1.addAll(pendingArray)
        for (i in 0 until pendingArray.size) {
            if (pendingArray[i].booking_id.equals(data.booking_id)) {
                pendingArray1.removeAt(i)
            }
        }
        pendingArray.clear()
        pendingArray.addAll(pendingArray1)
        pendingSize = pendingArray.size
    }

    override fun setOff() {
//      offset=0
//        completeArray[0].count=0
    }

    override fun update(data: Pendingmodel) {
        var modelSearch = ModelSearch()
        var model = com.example.shagwagon.completed_screen.Model()
        model.total_amount = data.total_amount
        model.profile_image = data.profile_image
        model.longitude = data.longitude
        model.latitude = data.latitude
        model.driver_phone = data.driver_phone
        model.driver_rating = data.driver_rating
        model.driver_name = data.driver_name
        model.customer_rating = data.customer_rating
        model.customer_name = data.customer_name
        model.booking_type = data.booking_type
        model.booking_time = data.booking_time
        model.booking_id = data.booking_id
        model.booking_date = data.booking_date
        modelSearch.list.add(model)
        completeArray.set(0, modelSearch)

    }


}






