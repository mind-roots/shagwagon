package com.example.shagwagon.main_activity_parent

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RatingCheckRepository(var application: Application) {

    var success = MutableLiveData<RatingModel>()
    var failure = MutableLiveData<Boolean>()


    fun setdata() {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()
        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.CheckLastBookingStatus(auth, "2")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val ratingModel = RatingModel()
                        val resp = response.body()!!.string()
                        val json = JSONObject(resp)
                        val status = json.optString("status")
                        ratingModel.status = status
                        val msg = json.optString("msg")

                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val booking = data.optJSONObject("booking")
                            ratingModel.booking_id = booking.optString("booking_id")
                            ratingModel.customer_id = booking.optString("customer_id")
                            ratingModel.driver_id = booking.optString("driver_id")
                            ratingModel.customer_name = booking.optString("customer_name")
                            ratingModel.profile_image = booking.optString("profile_image")

                            success.value = ratingModel
                        }

                        if (status == "false") {
                            failure.value = ratingModel.status.toBoolean()

                        }

                    } catch (e: Exception) {

                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                failure.value = true

            }

        })


    }

    //send the data to the viewmodel
    fun success(): MutableLiveData<RatingModel> {
        return success

    }

    fun failure(): MutableLiveData<Boolean> {
        return failure
    }
}