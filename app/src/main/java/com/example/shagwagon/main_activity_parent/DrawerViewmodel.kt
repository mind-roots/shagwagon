package com.example.shagwagon.main_activity_parent

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class DrawerViewmodel(application: Application) : AndroidViewModel(application) {
    //set the data in the repository
    fun setdata() {

        mrepo.setdata()
    }
    //getting the data from the repository


    val mrepo = DrawerRepository(application)


    fun success(): MutableLiveData<Boolean> {
        return mrepo.success()

    }
//    fun nfailure():MutableLiveData<String>{
//        return mrepo.nfailure()
//
//    }

}
