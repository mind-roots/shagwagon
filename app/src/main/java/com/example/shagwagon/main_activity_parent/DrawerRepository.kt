package com.example.shagwagon.main_activity_parent

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class DrawerRepository(var application: Application) {
    var success = MutableLiveData<Boolean>()



    fun setdata() {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()
        val prefs = Constants.getPrefs(application).getString(Constants.token, "")

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.logout(prefs,"2")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                success.value = true
//                if (response.isSuccessful) {
//                nsucess.value=true
//
//                }
            }
//                    try {


//                    val res = response.body()!!.string()
//                    val json = JSONObject(res)
//                    val status = json.optString("status")
//
//                    val msg = json.optString("msg")
//                    if (status == "true") {
//                        val model = ResponseModel()
//                        model.msg = msg
//                        nsucess.value = model
//
//                    } else {
//                        nfailure.value=msg
//
//
//                    }
//

//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                }else{
//
//                    val msg="unsucessfull"
//                    nfailure.value=msg
//                }


//            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                success.value = true

            }

        })


    }

    //send the data to the viewmodel
    fun success(): MutableLiveData<Boolean> {
        return success

    }

//    //on failure check
//    fun nfailure(): MutableLiveData<String> {
//        return nfailure
//
//    }
}
