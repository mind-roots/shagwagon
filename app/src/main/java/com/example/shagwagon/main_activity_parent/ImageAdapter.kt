package com.example.shagwagon.main_activity_parent

import android.content.Context
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso



@BindingAdapter("setimage")

fun getimage(image: ImageView, text: String) {

    Picasso.get().load(text).into(image)

}

