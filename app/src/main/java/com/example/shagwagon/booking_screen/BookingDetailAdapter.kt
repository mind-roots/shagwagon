package com.example.shagwagon.booking_screen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.completed_screen.InnerModel
import com.example.shagwagon.completed_screen.Model
import com.example.shagwagon.databinding.BookingDetailAdapterBinding

class BookingDetailAdapter(var activity: BookingScreenActivity, var data: Model) :
    RecyclerView.Adapter<BookingDetailAdapter.Viewholder>() {
    private lateinit var mbinding: BookingDetailAdapterBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        mbinding = BookingDetailAdapterBinding.inflate(LayoutInflater.from(activity), parent, false)
        return Viewholder(mbinding)
    }

    override fun getItemCount(): Int {

        return data.modelcategory!!.size

    }

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        var data = data.modelcategory!![position]
        holder.bind(data)

    }


    class Viewholder(var mbinding: BookingDetailAdapterBinding) : RecyclerView.ViewHolder(mbinding.root) {
        fun bind(data: InnerModel) {

            val name = data.name
            val price = data.price
            val service_id = data.service_id
            mbinding.textView7.text=name
            mbinding.textView16.text= "$$price"
        }


    }

}
