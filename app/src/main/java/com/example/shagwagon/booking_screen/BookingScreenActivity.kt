package com.example.shagwagon.booking_screen

import android.location.Address
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.shagwagon.R
import com.example.shagwagon.completed_screen.Model
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.databinding.ActivityBookingScreenBinding
import kotlinx.android.synthetic.main.activity_booking__screen.*
import java.io.IOException
import java.lang.StringBuilder

class BookingScreenActivity : AppCompatActivity() {
    lateinit var mbinding: ActivityBookingScreenBinding
    // lateinit var mvmodel: BookingCompleteViewModel
    lateinit var text: TextView
    lateinit var latitude: String
    lateinit var longitude: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_booking__screen)
        // mvmodel = ViewModelProviders.of(this).get(BookingCompleteViewModel::class.java)

        //get the data from the intent
        var data = intent.getParcelableExtra<Model>("AllData")


//mbinding.setdata=data
        val booking_id = data.booking_id
        val customer_name = data.customer_name
        val booking_date = data.booking_date
        val booking_time = data.booking_time
        val total_amount = data.total_amount
        var profile_image = data.profile_image


        //..........check  for the booking type................//
        var booking_type = data.booking_type
        if (booking_type == "2") {
            mbinding.imageView6.setImageResource(R.drawable.ic_booking_details_subscribed)
//UNDER LINE THE TEXT BY SPANNABLE
            val udata = "SCHEDULED"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView33.text = text


        } else if (booking_type == "3") {

            mbinding.imageView6.setImageResource(R.drawable.ic_booking_details_scheduled)
            //UNDER LINE THE TEXT BY SPANNABLE
            val udata = "SUBSCRIBED"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView33.text = text

        } else {
            mbinding.imageView6.setImageResource(R.drawable.ic_booking_details_completed)
            //UNDER LINE THE TEXT BY SPANNABLE
            val udata = "INSTANT BOOKING"
            val text = SpannableString(udata)
            text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView33.text = text

        }


        latitude = data.latitude
        longitude = data.longitude// this lat long set to the get address fun
        //change the time format
        val time =
            Constants.parseTime(booking_time, Constants.SERVER_TIME_RESOURCES_FORMAT, Constants.LOCAL_TIME_FORMAT)
//set the data
        mbinding.textView31.text = customer_name
        mbinding.textView42.text = booking_date
        mbinding.textView43.text = time
        mbinding.textView17.text = "$$total_amount"
        mbinding.textView29.text = "Booking #"+booking_id
        //set the image in layout


        Glide.with(this)
            .load(profile_image)
            .into(imageView10)

        if(profile_image.isNotEmpty()){
            Glide.with(this)
                .load(profile_image)
                .into(imageView10)
        }
        else
        {
            Glide.with(this)
                .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                .into(imageView10)
        }
        getAddress()//set the location

        //set the adpter
        val manger = LinearLayoutManager(this)
        mbinding.recyclerView2.layoutManager = manger
        mbinding.recyclerView2.adapter = BookingDetailAdapter(this, data)


//toolbar set
        title = ""
        //get the id of toolbar and textview
        text = findViewById(R.id.toolbar23text)
        //set the text
        text.text = "Booking Details"
        setSupportActionBar(toolbar23)
        //enabled thebackbutton
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            if (item.itemId == android.R.id.home) {
                finish()

            }
        }
        return super.onOptionsItemSelected(item)
    }

    //geo coading converting the address
    private fun getAddress(): String? {
        // 1
        val geocoder = Geocoder(this)
        val address: Address?

        try {
            // 2
            val addresses = geocoder.getFromLocation(latitude.toDouble(), longitude.toDouble(), 1)
            address = addresses[0]
            val builder = StringBuilder()
//            for (i in 0 until address.maxAddressLineIndex) {
//                builder.append(address.getAddressLine(i)).append("\n")
//            }
//            builder.append(address.featureName).append(",\t")
//            builder.append(address.thoroughfare).append(",\t")
            builder.append(address.subAdminArea).append(",\t")
            builder.append(address.countryName)
            var result = builder.toString()

            mbinding.textView32.text = result

        } catch (e: Exception) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return null
    }


}
