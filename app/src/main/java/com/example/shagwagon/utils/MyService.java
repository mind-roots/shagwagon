package com.example.shagwagon.utils;

import android.Manifest;
import android.app.*;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import com.example.shagwagon.R;
import com.example.shagwagon.boooking_details_googleMap.GoogleMapRepository;
import com.example.shagwagon.main_activity_parent.MainActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MyService extends Service {
    //creating a mediaplayer object
//    private MediaPlayer player;

    private NotificationManager mNM;
    Timer myTimer = new Timer();


    @Override
    public void onCreate() {
        super.onCreate();


    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        SharedPreferences myPrefs;
        myPrefs = getSharedPreferences("Service", MODE_PRIVATE);
        String StoredValue = myPrefs.getString("serviceStatus", "");


        myTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                try {
                    FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
                    GoogleMapRepository googleMapRepository = new GoogleMapRepository(getApplication());
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // ActivityCompat.requestPermissions(getApplication(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 123);

                        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                try {
                                    if (location != null) {
                                        googleMapRepository.setlocation(getApplication(), location.getLatitude(), location.getLongitude());

                                    } else {
                                        SharedPreferences myPrefs;
                                        myPrefs = getSharedPreferences("Location", MODE_PRIVATE);
                                        Double latitude = Double.valueOf(myPrefs.getString("latitude", ""));
                                        Double longitude = Double.valueOf(myPrefs.getString("longitude", ""));
                                        googleMapRepository.setlocation(getApplication(), latitude, longitude);


                                    }
                                    Log.e("Service", "Api Hit");

                                } catch (Exception e) {
                                    e.printStackTrace();


                                }
                            }
                        });
                    } else {
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {

                                try {
                                    if (location != null) {
                                        googleMapRepository.setlocation(getApplication(), location.getLatitude(), location.getLongitude());

                                    } else {
                                        SharedPreferences myPrefs;
                                        myPrefs = getSharedPreferences("Location", MODE_PRIVATE);
                                        Double latitude = Double.valueOf(myPrefs.getString("latitude", ""));
                                        Double longitude = Double.valueOf(myPrefs.getString("longitude", ""));
                                        googleMapRepository.setlocation(getApplication(), latitude, longitude);

                                    }

                                    Log.e("Service", "Api Hit");

                                } catch (Exception e) {
                                    e.printStackTrace();

                                }


                            }
                        });
                    }
                } catch (Exception e) {

                }

            }
        }, 0, 6000);


        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);


        Log.e("Map Service", " Service running now");
        if (isAppIsInBackground(getApplicationContext())) {
            mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            startForeground(startId, showNotification());
            showNotification();

        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //stopping the player when service is destroyed
//        player.stop();
        myTimer.cancel();
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        SharedPreferences myPrefs;
        myPrefs = getSharedPreferences("Service", MODE_PRIVATE);
        String StoredValue = myPrefs.getString("serviceStatus", "");
        Intent intent = new Intent();
        String a = intent.getStringExtra("key");

        if (StoredValue == "2") {
            //
            //
            // Toast.makeText(getApplicationContext(), "Service Task destroyed", Toast.LENGTH_LONG).show();

            Log.e("Map Service", " Service stopped");
            try {
                Intent intentService = new Intent(getApplicationContext(), MyService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    getApplicationContext().startForegroundService(intentService);
                } else {
                    getApplicationContext().startService(intentService);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (StoredValue == "1") {
            Intent intent1 = new Intent(this, MyService.class);
            stopService(intent1);

            Log.e("Map Service", " Service stopped");
        }


    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        Log.e("Map Service", " Task removed");
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        SharedPreferences myPrefs;
        myPrefs = getSharedPreferences("Service", MODE_PRIVATE);
        String StoredValue = myPrefs.getString("serviceStatus", "");

        if (StoredValue == "2") {
            try {
                Intent intentService = new Intent(getApplicationContext(), MyService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    getApplicationContext().startForegroundService(intentService);
                } else {
                    getApplicationContext().startService(intentService);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (StoredValue == "1") {
            rootIntent = new Intent(this, MyService.class);
            stopService(rootIntent);
        }


    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        SharedPreferences myPrefs;
        myPrefs = getSharedPreferences("Service", MODE_PRIVATE);
        String StoredValue = myPrefs.getString("serviceStatus", "");

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            try {

                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (String activeProcess : processInfo.pkgList) {
                            if (activeProcess.equals(context.getPackageName())) {
                                isInBackground = false;
                            }
                        }
                    }
                }

            } catch (Exception e) {

            }


        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }


        return isInBackground;
    }


    private Notification showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = "Your location is being updated in realtime for tracking.";

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        // Set the info for the views that show in the notification panel.
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.logo)  // the status icon
                .setTicker(text)  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                //.setContentTitle(getText(R.string.local_service_label))  // the label of the entry
                .setContentText(text)// the contents of the entry
                .setContentIntent(contentIntent); // The intent to send when the entry is clicked

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            builder.setChannelId(createNotificationChannel("com.shagwagon.driver", "Driver"));
        }


        // Send the notification.
        //mNM.notify(0, notification);
        return builder.build();
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(String channelId, String channelName) {
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(chan);
        return channelId;
    }


}
