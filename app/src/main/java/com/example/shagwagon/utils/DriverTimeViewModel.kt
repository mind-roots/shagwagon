package com.example.shagwagon.utils

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class DriverTimeViewModel (application: Application) : AndroidViewModel(application) {

    var mrepo = DriverTimeRespository(application)
    fun setData(clock_in: String, clock_out: String){
        mrepo.setData(clock_in,clock_out)

    }

    fun mSuccess() : MutableLiveData<String> {
        return mrepo.msucessful
    }

    fun mFailure() : MutableLiveData<String> {
        return mrepo.mfailure
    }
}