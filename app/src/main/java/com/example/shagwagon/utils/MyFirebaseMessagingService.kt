package com.example.shagwagon.utils

import android.app.NotificationManager
import android.content.Intent
import android.util.Log
import com.example.shagwagon.main_activity_parent.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class  MyFirebaseMessagingService : FirebaseMessagingService() {

    val TAG = "Service"
    private lateinit var notificationManager: NotificationManager
    private val ADMIN_CHANNEL_ID = "Shagwagon"

    override fun onMessageReceived(msg: RemoteMessage) {

        super.onMessageReceived(msg)

        try {
            if (MainActivity.isAppRunning) {

                var intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("reLoad", "reLoad")
                startActivity(intent)
                val notificationHelper = NotificationHelper(this)
                notificationHelper.createNotification(msg)
            } else {
                val notificationHelper = NotificationHelper(this)
                notificationHelper.createNotification(msg)
            }
        } catch (e: Exception) {

        }


    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.i(TAG, token)
    }


    // Handle FCM messages here.
    // If the application is in the foreground handle both data and notification messages here.
    // Also if you intend on generating your own notifications as a result of a received FCM
    // message, here is where that should be initiated.
    /* Log.d(TAG, "From: " + msg!!.from)
     Log.d(TAG, "Notification Message Body: " + msg.notification?.body!!)*/


    /* @RequiresApi(api = Build.VERSION_CODES.O)
     private fun setupNotificationChannels() {
         val adminChannelName = "Shagwagon"
         val adminChannelDescription = "Shagwagon"

         val adminChannel: NotificationChannel
         adminChannel =
             NotificationChannel(adminChannelDescription, adminChannelName, NotificationManager.IMPORTANCE_LOW)
         adminChannel.description = adminChannelDescription
         adminChannel.enableLights(true)
         adminChannel.lightColor = Color.RED
         adminChannel.enableVibration(true)
         notificationManager.createNotificationChannel(adminChannel)
     }*/


}