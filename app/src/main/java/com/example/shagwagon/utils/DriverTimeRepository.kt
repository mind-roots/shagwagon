package com.example.shagwagon.utils

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class DriverTimeRespository(var application: Application) {

    var msucessful = MutableLiveData<String>()
    val mfailure = MutableLiveData<String>()



    fun setData(clock_in: String, clock_out: String) {
        getData(clock_in, clock_out)
    }

    fun getData(clock_in: String, clock_out: String) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()

        val auth = Constants.getPrefs(application).getString(Constants.token, "")

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.DriveInandOut(auth, clock_in, clock_out)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        if (status == "true") {

                            msucessful.value = msg

                        }
                        else {
                            msucessful.value = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                } else {
                    Toast.makeText(application, "Network", Toast.LENGTH_SHORT).show()
                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

            }


        })
    }
}