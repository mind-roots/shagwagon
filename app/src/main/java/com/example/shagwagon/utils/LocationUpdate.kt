package com.example.shagwagon.utils

import android.Manifest
import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.android.gms.tasks.OnSuccessListener
import android.content.pm.PackageManager
import android.Manifest.permission
import android.Manifest.permission.ACCESS_COARSE_LOCATION
import androidx.core.app.ActivityCompat
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Application
import android.util.Log
import androidx.lifecycle.ViewModelProviders
import com.example.shagwagon.boooking_details_googleMap.GoogleMapRepository
import com.example.shagwagon.boooking_details_googleMap.GoogleMapViewModel
import com.example.shagwagon.constant_webservices.Constants
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.FusedLocationProviderClient



class LocationUpdate(var appContext: Context,workerParameters: WorkerParameters) : Worker(appContext,workerParameters) {
    override fun doWork(): Result {

        Log.e("Work Manager","Work Manager Started")

        var googleMapRepository = GoogleMapRepository(Application())


        try {
            val mFusedLocationClient = LocationServices.getFusedLocationProviderClient(applicationContext)
            if (ActivityCompat.checkSelfPermission(
                    applicationContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // ActivityCompat.requestPermissions(getApplication(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 123);

                mFusedLocationClient.lastLocation.addOnSuccessListener { location ->
                    googleMapRepository.setlocation(appContext,location.latitude,location.longitude)
                    Log.e("if","location update")



                }
            } else {
                mFusedLocationClient.lastLocation.addOnSuccessListener { location ->
                    googleMapRepository.addlocation(appContext,location.latitude,location.longitude)
                    Log.e("else","location update")

                }

            }

        } catch (e: Exception) {

        }


        return Result.success()

    }

}