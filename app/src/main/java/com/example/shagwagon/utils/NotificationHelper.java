package com.example.shagwagon.utils;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.example.shagwagon.R;
import com.example.shagwagon.main_activity_parent.MainActivity;
import com.example.shagwagon.pending_screen.PendingRepository;
import com.example.shagwagon.pending_screen.PendingViewmodel;
import com.google.firebase.messaging.RemoteMessage;

public class NotificationHelper {


    private Context mContext;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    private static final String NOTIFICATION_CHANNEL_ID = "10001";
    PendingRepository pendingRepository;
    PendingViewmodel pendingViewmodel;

    NotificationHelper(Context context) {
        mContext = context;
    }







    /**
     * Create and push the notification
     */
    void createNotification(RemoteMessage msg) {
        /**Creates an explicit intent for an Activity in your app**/


        Intent resultIntent = null;
        String title = msg.getData().get("message");
        String type = msg.getData().get("type");
        Log.e("type", type);
        Log.e("title", title);



        try {
                if (type.equals("1")) {
                    resultIntent = new Intent(mContext, MainActivity.class);
                    resultIntent.putExtra("booking", "booking");
                } else if (type.equals("3")) {
                    resultIntent = new Intent(mContext, MainActivity.class);
                    resultIntent.putExtra("noType", "noType");
                } else {
                    resultIntent = new Intent(mContext, MainActivity.class);
                    resultIntent.putExtra("noType", "noType");
                }


        } catch (Exception e) {
            e.printStackTrace();
        }


        //resultIntent.setAction(path);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);


        mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setSmallIcon(R.mipmap.shagwagon_driver_app_icon);
        // mBuilder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.app_icon));
        mBuilder.setContentTitle(title)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Wagon App", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }

        // notification.flags = Notification.FLAG_NO_CLEAR;
        assert mNotificationManager != null;
        //final int random = new Random().nextInt(61) + 20;
        mNotificationManager.notify(0/* Request Code */, mBuilder.build());
    }

}
