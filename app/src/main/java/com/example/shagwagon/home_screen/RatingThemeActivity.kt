package com.example.shagwagon.home_screen

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.shagwagon.R
import com.example.shagwagon.databinding.ActiveRatingDialogboxBinding
import com.example.shagwagon.main_activity_parent.MainActivity
import com.example.shagwagon.pending_screen.Pendingmodel
import com.google.gson.Gson
import com.bumptech.glide.Glide


class RatingThemeActivity : Activity() {

    //lateinit var mvmodel: MarkCompleteViewModel
    lateinit var binding: ActiveRatingDialogboxBinding
    var booking_id = ""
    var driver_id = ""
    var customer_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.active_rating_dialogbox)
        this.setFinishOnTouchOutside(false)
        try {
            val sharedPreference = this.getSharedPreferences("Status", Context.MODE_PRIVATE)
            val gson: Gson = Gson()
            var json: String = sharedPreference.getString("data", "")

            if (json != "") {
                val data_stored: Pendingmodel = gson.fromJson(json, Pendingmodel::class.java)
                if (data_stored.profile_image.isNotEmpty()) {
                    Glide.with(this)
                        .load(data_stored.profile_image)
                        .into(binding.infoProfile)
                } else {
                    Glide.with(this)
                        .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                        .into(binding.infoProfile)
                }

                binding.textView64.text = resources.getString(R.string.rateUs) + " " + data_stored.customer_name
            } else {
                booking_id = intent.getStringExtra("booking_id")
                driver_id = intent.getStringExtra("driver_id")
                customer_id = intent.getStringExtra("customer_id")
                var customer_name = intent.getStringExtra("customer_name")
                var profile_image = intent.getStringExtra("profile_image")


                if (profile_image.isNotEmpty()) {
                    Glide.with(this)
                        .load(profile_image)
                        .into(binding.infoProfile)
                } else {
                    Glide.with(this)
                        .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                        .into(binding.infoProfile)
                }

                binding.textView64.text = resources.getString(R.string.rateUs) + " " + customer_name

            }


        } catch (e: Exception) {

        }


        //mvmodel = ViewModelProviders.of( ).get(MarkCompleteViewModel::class.java)
/*       val mDialogView = LayoutInflater.from(this).inflate(R.layout.active_rating_dialogbox, null)
       //AlertDialogBuilder
       val mBuilder = AlertDialog.Builder(this)
           .setView(mDialogView)

       //this.setTheme(R.drawable.rectangle_transparent)
       // mBuilder.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
       val mAlertDialog = mBuilder.show()*/
/*

        title = "Please Rate "
        var booking_id = intent.getStringExtra("booking_id")
        Log.e("RatingThemeActivity", booking_id)
*/

        var button: Button = findViewById(R.id.button8)
        button.setOnClickListener {


            var msg: String = ""

            if (binding.ratingBar.rating == 0.0f) {
                msg = binding.ratingBar.rating.toString()
                Log.e("Rating Bar", msg)
                Toast.makeText(this, "Please Rate", Toast.LENGTH_SHORT).show()
            } else {


                if (booking_id != "" && customer_id != "" && driver_id != "") {

                    msg = binding.ratingBar.rating.toString()

                    var ratingRespository = RatingRespository(application)
                    ratingRespository.getdata(
                        booking_id,
                        customer_id,
                        driver_id,
                        msg
                    )
                    //Toast.makeText(this, "Rated Successfully", Toast.LENGTH_LONG).show()
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    finish()
                    startActivity(intent)


                } else {

                    try {
                        msg = binding.ratingBar.rating.toString()
                        val sharedPreference = this.getSharedPreferences("Status", Context.MODE_PRIVATE)
                        val gson: Gson = Gson()
                        var json: String = sharedPreference.getString("data", "")
                        val data_stored: Pendingmodel = gson.fromJson(json, Pendingmodel::class.java)

                        var ratingRespository = RatingRespository(application)
                        ratingRespository.getdata(
                            data_stored.booking_id,
                            data_stored.customer_id,
                            data_stored.driver_id,
                            msg
                        )
                        //Toast.makeText(this, "Rated Successfully", Toast.LENGTH_LONG).show()
                        var a = getSharedPreferences("Status", Context.MODE_PRIVATE)
                        var b = a?.getString("status", "")
                        var editor = a?.edit()
                        editor?.remove("status")
                        editor?.remove("data")
                        editor?.clear()
                        editor?.commit()

                        var a_status = getSharedPreferences("Services", Context.MODE_PRIVATE)
                        var b_status = a_status?.getString("serviceStatus", "")
                        var editor_status = a?.edit()
                        editor_status?.clear()
                        val intent = Intent(this, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        finish()
                        startActivity(intent)

                    } catch (e: Exception) {

                    }


                }
            }


        }


    }


}
