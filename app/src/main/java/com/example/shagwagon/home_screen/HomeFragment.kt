package com.example.shagwagon.home_screen

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.shagwagon.R
import com.example.shagwagon.completed_screen.Completed_Screen_Fragment
import com.example.shagwagon.databinding.FragmentHomeBinding
import com.example.shagwagon.main_activity_parent.MainActivity
import com.example.shagwagon.pending_screen.InnerModel
import com.example.shagwagon.pending_screen.Pending_Fragment
import com.example.shagwagon.pending_screen.Pendingmodel
import com.example.shagwagon.start_pending_booking.BusProvider
import com.example.shagwagon.start_pending_booking.Data
import com.example.shagwagon.start_pending_booking.MyValue
import com.google.gson.Gson
import com.squareup.otto.Subscribe


class HomeFragment : Fragment(), Button_Click, Pending_Fragment.setdataInterface1 {


    override fun setadapterData1(position: Int, data: Pendingmodel, abc: String?) {
        interfaceMain.updatePendingArray(data)

        data2 = data.modelcategories

        // interfaceAddress.setadapterData2(position, data)
        model.customer_name = data.customer_name
//        Log.e("here anything1", data.modelcategories[0].toString())

        try {

            if (data.latitude != "") {
                args.putParcelable("data", data)
                model.latitude = data.latitude
                model.longitude = data.longitude
                model.total_amount = data.total_amount
                model.customer_name = data.customer_name
                model.booking_id = data.booking_id
            } else {
                val shared = activity!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
                val g: Gson = Gson()
                var j: String? = shared?.getString("data", "")

                val data_stored: Pendingmodel = g.fromJson(j, Pendingmodel::class.java)
                Log.e("Shared Preference Home", data_stored.total_amount)
                args.putParcelable("data", data_stored)
                model.latitude = data_stored.latitude
                model.longitude = data_stored.longitude
                model.total_amount = data_stored.total_amount
                model.customer_name = data_stored.customer_name
                model.booking_id = data_stored.booking_id
                data_stored.modelcategories
                val a = true
                set_lat_long(model.latitude, model.longitude)
            }
            //data.modelcategories
            // set_lat_long(model.latitude, model.longitude)
            setbutton2()
            Handler().postDelayed({
                mbinding.radioGroup.check(R.id.radioButton)
            }, 100)


        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    // lateinit var model1 : Pendingmodel
    lateinit var data2: List<InnerModel>
    val model = Data()
    private val args = Bundle()
    var getBooking = ""


    lateinit var interfaceMain: setdataInterfaceMain

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        interfaceMain = context as setdataInterfaceMain


    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        interfaceMain = activity as setdataInterfaceMain


        try {
            val getBooking = arguments?.getString("booking")

            if (getBooking!!.isNotEmpty()) {

                fragment = Pending_Fragment()
                setbutton3()
            }

        } catch (e: Exception) {

        }

    }

    fun set_lat_long(lat: String, longi: String) {
        var latitute = lat
        var longitute = longi
        Log.e("set_lat_long", latitute + " " + longitute)
//        val args = Bundle()
//        args.putParcelable("latitude", latitute)
//        args.putString("longitude",longitute)
//        val newFragment = Home1()
//        newFragment.arguments = args
    }

    // lateinit var interfaceAddress: setdataInterface2
    lateinit var mbinding: FragmentHomeBinding
    var fragment: Fragment = Home1()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


//by default open the home layout
        mbinding = FragmentHomeBinding.inflate(LayoutInflater.from(context), container, false)


//        when (fragment) {
//            Home1() -> mbinding.radioGroup.check(R.id.radioButton)
//            Completed_Screen_Fragment() -> mbinding.radioGroup.check(R.id.radioButton1)
//            Pending_Fragment(activity!!, HomeFragment(Home1())) -> mbinding.radioGroup.check(R.id.radioButton2)
//        }
        try {
            // interfaceAddress = homeFragment as setdataInterface2
        } catch (e: Exception) {

        }


        val sharedPreference = context!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
        val gson: Gson = Gson()
        var json: String = sharedPreference.getString("data", "")

        if (json.isNotEmpty()) {
            val data: Pendingmodel = gson.fromJson(json, Pendingmodel::class.java)
            Log.e("Shared Preference data", data.total_amount)

        }



//        fragment = Home1()
//        interfaceMain.setChildFragment(fragment)
//        val transaction = childFragmentManager.beginTransaction()
//        transaction.replace(R.id.frame, fragment)
//        transaction.commit()


        return mbinding.root

    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        //  interfaceAddress = homeFragment as setdataInterface2
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post(object : Runnable {
            override fun run() {
                if (interfaceMain.getPendingSize()!=0) {
                    mbinding.radioButton2.text = "Pending (" + interfaceMain.getPendingSize() + ")"
                }
                else{
                    mbinding.radioButton2.text = "Pending"
                }
                mainHandler.postDelayed(this, 10)
            }
        })
        //

        mbinding.click = this
        if (fragment is Completed_Screen_Fragment) {
            mbinding.radioButton1.isChecked = true
        }
        if (fragment is Home1) {
            mbinding.radioButton.isChecked = true
        }
        if (fragment is Pending_Fragment) {
            mbinding.radioButton2.isChecked = true
        }


        /* try {
             var getFragment = arguments?.getString("booking")
             if (getFragment!!.isNotEmpty()) {
                 setbutton2()
             }
         } catch (e: Exception) {
             e.printStackTrace()

         }*/


    }

    //get the data from event bus

    @Subscribe
    fun getdata(test: MyValue) {


        var model = test.syncStatusMessage
        //send the data to the home 1 fragment


        //setdata(model)

        // BusProvider.getInstance().post(MyValue(model))
        // Toast.makeText(activity!!, model.customer_name, Toast.LENGTH_SHORT).show()
        //  activity!!.finish()


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BusProvider.getInstance().register(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)

    }
    //event bus


    override fun onResume() {
        super.onResume()
        try {
            // interfaceAddress = homeFragment as setdataInterface2
        } catch (e: Exception) {

        }
        if (fragment != null) {
            val transaction = childFragmentManager.beginTransaction()
            transaction.replace(R.id.frame, fragment)
            transaction.commit()

        }


    }

    override fun setbutton() {
        val transaction = childFragmentManager.beginTransaction()
        mbinding.radioGroup.check(R.id.radioButton1)
        fragment = Completed_Screen_Fragment()
        interfaceMain.setChildFragment(fragment)

        transaction.replace(R.id.frame, fragment)
        transaction.commit()

    }


    override fun setbutton2() {

        try {
            val transaction = childFragmentManager.beginTransaction()
            mbinding.radioGroup.check(R.id.radioButton)

            args.putString("latitude", model.latitude)
            args.putString("longitude", model.longitude)
            args.putString("totalAmount", model.total_amount)
            args.putString("customerName", model.customer_name)
            args.putString("booking_id", model.booking_id)

            fragment = Home1()
            interfaceMain.setChildFragment(fragment)
            fragment.arguments = args
            Log.e("home on create", model.latitude)


            transaction.replace(R.id.frame, fragment)
            transaction.commit()
        } catch (e: Exception) {
            e.printStackTrace()

        }


    }

    override fun setbutton3() {

        val transaction = childFragmentManager.beginTransaction()
        mbinding.radioGroup.check(R.id.radioButton2)
        var abc = "hello"
        args.putString("ifcheck", abc)
        fragment = Pending_Fragment()
        interfaceMain.setChildFragment(fragment)
        fragment.arguments = args
        transaction.replace(R.id.frame, fragment)
        transaction.commit()


    }

    fun updateSize(size: Int) {

       // mbinding.radioButton2.text = "Pending " +size.toString()
    }

    interface setdataInterface2 {

        fun setadapterData2(position: Int, data: Pendingmodel)


    }

    interface setdataInterfaceMain {


        fun setChildFragment(fragment: Fragment)
        fun updatePendingArray(data: Pendingmodel)
        fun getPendingSize():Int

    }


}



