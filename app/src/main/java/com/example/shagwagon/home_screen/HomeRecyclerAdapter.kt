package com.example.shagwagon.home_screen

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.shagwagon.databinding.HomeRecyclerAdapterBinding
import com.example.shagwagon.pending_screen.InnerModel
import com.example.shagwagon.pending_screen.Pendingmodel

class HomeRecyclerAdapter(var activity: FragmentActivity?, var data2: Pendingmodel) :
    RecyclerView.Adapter<HomeRecyclerAdapter.Viewholder>() {
    private lateinit var mbinding: HomeRecyclerAdapterBinding
    var it = ArrayList<Pendingmodel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        mbinding = HomeRecyclerAdapterBinding.inflate(LayoutInflater.from(activity), parent, false)

        return Viewholder(mbinding)

    }

    override fun getItemCount(): Int {

        return data2.modelcategories.size
    }

    override fun onBindViewHolder(holder: Viewholder, position: Int) {

        val data2 = data2.modelcategories[position]
        holder.bind(data2)

    }

    class Viewholder(var mbinding: HomeRecyclerAdapterBinding) : RecyclerView.ViewHolder(mbinding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data2: InnerModel) {

            mbinding.textView18.text = "$ " + data2.price
            mbinding.textView56.text = data2.name
            mbinding.executePendingBindings()


        }

    }
    fun update(size: ArrayList<Pendingmodel>) {

        it = size
        notifyDataSetChanged()

    }

}
