package com.example.shagwagon.home_screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData


class MarkCompleteViewModel(application: Application) : AndroidViewModel(application) {
    val mrepo = MarkCompleteRepository(application)


    fun setData(booking_id : String) {

        mrepo.setData(booking_id)
    }

    fun success(): MutableLiveData<CompleteBookingModel> {
        return mrepo.success()

    }

    fun setSuccess() {

        return mrepo.setSuccess()
    }

}