package com.example.shagwagon.home_screen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class RatingViewModel(application: Application) : AndroidViewModel(application) {
    var mrepo = RatingRespository(application)

    fun pendingSucessful(): MutableLiveData<ArrayList<String>> {

        return mrepo.pendingSucessful()
    }

    fun pendingFailure(): MutableLiveData<String> {

        return mrepo.pendingFailure()
    }

    fun getdata(booking_id: String, customer_id: String, driver_id: String, rating: String) {
        mrepo.getdata(booking_id, customer_id, driver_id, rating)
    }
}