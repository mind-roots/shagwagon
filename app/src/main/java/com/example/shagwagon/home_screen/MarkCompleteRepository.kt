package com.example.shagwagon.home_screen

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import com.example.shagwagon.pending_screen.Pendingmodel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class MarkCompleteRepository(var application: Application) {


    var success = MutableLiveData<CompleteBookingModel>()
   // var success = MutableLiveData<CompleteBookingModel>()



    fun setData(booking_id: String) {

        getdata(booking_id)

    }

    fun getdata(booking_id: String) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()

        val prefs = Constants.getPrefs(application).getString(Constants.token, "")

        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.MarkasComplete(prefs, booking_id)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {

                    val resp = response.body()!!.string()
                    try {


                        val json = JSONObject(resp)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val completeBookingModel = CompleteBookingModel()
                        completeBookingModel.status=status.toBoolean()
                        completeBookingModel.msg=msg
                        success.value=completeBookingModel
                        Log.e("Message Complete", msg)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }else{
                    val completeBookingModel = CompleteBookingModel()
                    completeBookingModel.status=false
                    completeBookingModel.msg=""
                    success.value=completeBookingModel
                }


            }



            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val completeBookingModel = CompleteBookingModel()
                completeBookingModel.status=false
                completeBookingModel.msg=""
                success.value=completeBookingModel

            }
        })
    }

    fun success(): MutableLiveData<CompleteBookingModel> {
        return success

    }

    fun setSuccess() {

        val completeBookingModel = CompleteBookingModel()
        completeBookingModel.status=false
        completeBookingModel.msg=""
        success.value=completeBookingModel
    }


}