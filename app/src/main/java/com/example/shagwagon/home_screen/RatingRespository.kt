package com.example.shagwagon.home_screen

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.shagwagon.expanded_view_screen.ExpandedListModel
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.constant_webservices.WebServices
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.*

class RatingRespository(var application: Application) {
    private var msucessful = MutableLiveData<ArrayList<String>>()
    private val mfailure = MutableLiveData<String>()
    val modelarray = ArrayList<ExpandedListModel>()
    fun getdata(booking_id: String, customer_id: String, driver_id: String, rating: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .build()
        val auth = Constants.getPrefs(application).getString(Constants.token, "")
        val services = retrofit.create(WebServices::class.java)
        val call: Call<ResponseBody> = services.GiveRating(auth, booking_id, customer_id, driver_id, rating, "2")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val resp = response.body()!!.string()
                    val json = JSONObject(resp)
                    val statuss = json.optString("status")
                    val msg = json.optString("msg")


                    if (statuss == "true") {
                        //Toast.makeText(application, msg, Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(application, msg, Toast.LENGTH_LONG).show()
                    }

                } else {

                    val msg = "No Response"
                    mfailure.value = msg
                    Log.e(msg, mfailure.value)
                }


            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val msg = "Network Error Please Check Your Network"
                mfailure.value = msg
                Log.e("Failure", mfailure.value)

            }


        })
    }

    fun pendingSucessful(): MutableLiveData<ArrayList<String>> {

        return msucessful
    }

    //failure
    fun pendingFailure(): MutableLiveData<String> {

        return mfailure
    }
}