package com.example.shagwagon.home_screen

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.shagwagon.expanded_view_screen.ExpandedListModel
import com.example.shagwagon.R
import com.example.shagwagon.boooking_details_googleMap.MapFragment
import com.example.shagwagon.constant_webservices.Constants
import com.example.shagwagon.databinding.AddScanSheetBinding
import com.example.shagwagon.databinding.FragmentHome1Binding
import com.example.shagwagon.expanded_view_screen.ExpandedView
import com.example.shagwagon.pending_screen.Pendingmodel
import com.example.shagwagon.start_pending_booking.BookingViewmodel
import com.google.gson.Gson


import java.io.IOException
import java.lang.NullPointerException
import java.lang.StringBuilder


class Home1() : Fragment(), HomeFragment.setdataInterface2, MapFragment.setdataInterface3 {
    override fun setadapterData3() {
        mbinding.nestedlayout.visibility = View.VISIBLE
        sendStatus()

    }


    //lateinit var mvmodel: HomeViewModel
    private var longitude: String = "123 "
    var abc = "123"
    private var latitude: String = "123"
    private var customerName: String = "abc"
    private var totalAmount: String = "123"
    private var booking_id: String = "121"
    var close = false
    var data2: Pendingmodel = Pendingmodel()
    override fun setadapterData2(position: Int, data: Pendingmodel) {

        //data2 = data
        //latitude = data2.latitude
        //longitude = data.longitude
//
        //Log.e("tag",longitude + " " + latitude)
        //Log.e("setadapter",data2.latitude)
        //Log.e("setadapter",data2.toString())
    }

    lateinit var homeFragment: HomeFragment


    lateinit var mbinding: FragmentHome1Binding
    var data: ExpandedListModel =
        ExpandedListModel()
    var size: Int = 1
    lateinit var mvmodel: BookingViewmodel
    lateinit var mvmodel1: MarkCompleteViewModel
    lateinit var interface1 : updateCompleted


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        //Log.e("onCreat",data2.toString())


        val sharedPreference = context!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
        val gson: Gson = Gson()
        var json: String = sharedPreference.getString("data", "")

        if (json.isNotEmpty()) {
            val data_stored: Pendingmodel = gson.fromJson(json, Pendingmodel::class.java)
            Log.e("Shared Preference Home1", data_stored.total_amount)
            val bundle = Bundle()
            Log.e("check here", data2.customer_name)

            latitude = data_stored.latitude
            longitude = data_stored.longitude
            customerName = data_stored.customer_name
            totalAmount = data_stored.total_amount
            booking_id = data_stored.booking_id
            data2 = data_stored
        } else {
            val bundle = Bundle()
            Log.e("check here", data2.customer_name)
            latitude = arguments?.getString("latitude").toString()
            longitude = arguments?.getString("longitude").toString()
            customerName = arguments?.getString("customerName").toString()
            totalAmount = arguments?.getString("totalAmount").toString()
            booking_id = arguments?.getString("booking_id").toString()
            abc = arguments?.getString("ifcheck").toString()
        }

        Log.e("here on create", data2.toString())
        //latitude=data2.latitude
        Log.e("here on create", latitude)
        Log.e("here on create", longitude)
        // Inflate the layout for this fragment

        mbinding = FragmentHome1Binding.inflate(LayoutInflater.from(context), container, false)
        mvmodel = ViewModelProviders.of(activity!!).get(BookingViewmodel::class.java)
        mvmodel1 = ViewModelProviders.of(activity!!).get(MarkCompleteViewModel::class.java)




        //mvmodel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        //mvmodel.getdatas()

        //        val model =homeFragment.getdata()
//        Log.e("Home1",model.toString())

        //getting the data from the interface
        //data2=setinterface.getdata()


        return mbinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.e("here on before if", latitude)
        Log.e("here on before if", longitude)
        Log.e("here on before if", abc)
        var a = context!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
        var b = a.getString("status", "")

        Log.e("check shared preference", b)

        if (b == "4") {


            var bundle: Bundle = Bundle()

            try {

                if (data2.latitude == "") {
                    data2 = arguments?.getParcelable<Pendingmodel>("data")!!
                    Log.e("home 1 is done", data2.booking_type)
                }


//            mbinding.nestedlayout.visibility = View.VISIBLE

                val transaction = childFragmentManager.beginTransaction()
                val args = Bundle()
                args.putString("latitude", latitude)
                args.putString("longitude", longitude)
                args.putString("totalAmount", totalAmount)
                args.putString("customerName", customerName)
                args.putString("booking_id", booking_id)
                args.putParcelable("data", data2)
                args.putString("abc", abc)
                val newFragment = MapFragment(this)
                newFragment.arguments = args
                transaction.replace(R.id.frame, newFragment)
                transaction.commit()
            } catch (e: NullPointerException) {
                print(e.stackTrace)
            }


//            mbinding.nestedlayout.visibility = View.VISIBLE

//            mbinding.fragmentlayout.visibility = View.GONE
//
            mbinding.textView57.text = data2.customer_name
//
//          getAddress()
//        //convert the address
//
            //convert the time
            val time = data2.booking_time
            val dates = Constants.parseTime(time, Constants.SERVER_TIME_RESOURCES_FORMAT, Constants.LOCAL_TIME_FORMAT)
//            mbinding.textView66.text = dates

            //set the booking type text
            var Booking_type = data2.booking_type

            if (Booking_type == "2") {
                //set the underline text
                val udata = "SCHEDULE"
                val text = SpannableString(udata)
                //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
                mbinding.textView60.text = text
            } else if (Booking_type == "3") {
                //set the underline text
                val udata = "SUBSCRIBED"
                val text = SpannableString(udata)
                //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
                mbinding.textView60.text = text
            } else {

                //set the underline text
                val udata = "INSTANT BOOKING"
                val text = SpannableString(udata)
                //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
                mbinding.textView60.text = text
            }
            getAddress()
            mbinding.textView57.text = data2.customer_name

            mbinding.ratingBar.rating = data2.customer_rating.toFloat()
            /*Glide.with(this)
                .load(data2.profile_image)
                .into(mbinding.imageView16)*/
            if(data2.profile_image.isNotEmpty()){
                Glide.with(this)
                    .load(data2.profile_image)
                    .into(mbinding.imageView16)
            }
            else
            {
                Glide.with(this)
                    .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                    .into(mbinding.imageView16)
            }

            val manger = LinearLayoutManager(context)
            mbinding.recyclerView.layoutManager = manger
            mbinding.recyclerView.adapter = HomeRecyclerAdapter(activity, data2)

        } else if (b == "5") {


            mbinding.textView57.text = data2.customer_name
            mbinding.ratingBar.rating = data2.customer_rating.toFloat()

            if(data2.profile_image.isNotEmpty()){
                Glide.with(this)
                    .load(data2.profile_image)
                    .into(mbinding.imageView16)
            }
            else
            {
                Glide.with(this)
                    .load("https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png")
                    .into(mbinding.imageView16)
            }
            getAddress()
            val manger = LinearLayoutManager(context)
            mbinding.recyclerView.layoutManager = manger
            mbinding.recyclerView.adapter = HomeRecyclerAdapter(activity, data2)
            mbinding.nestedlayout.visibility = View.VISIBLE
        } else {
            mbinding.nestedlayout.visibility = View.GONE
            mbinding.fragmentlayout.visibility = View.VISIBLE

        }


        mbinding.button7.setOnClickListener {


            mbinding.progressBar.visibility = View.VISIBLE
            var a = activity!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
            var b = a?.getString("status", "")

            if (b == "5") {
                mvmodel1.setData(booking_id)
                mbinding.button7.visibility = View.INVISIBLE
            }

        }
        mvmodel1.setSuccess()
        mvmodel1.success().observe(this, Observer {

            if (it.status) {
                Log.e("response", it.toString())
                mbinding.progressBar.visibility = View.INVISIBLE
                Toast.makeText(activity, it.msg, Toast.LENGTH_LONG).show()
                var a = activity?.getSharedPreferences("Status", Context.MODE_PRIVATE)
                var b = a?.getString("status", "")
                var editor = a?.edit()
                editor?.remove("status")
                editor?.commit()
                val intent = Intent(context, RatingThemeActivity::class.java)
                val bundle = Bundle()
                bundle.putString("booking_id", data2.booking_id)
                intent.putExtras(bundle)
                intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(intent)



            } else {
                if (it.msg.isNotEmpty()) {
                    mbinding.progressBar.visibility = View.INVISIBLE
                    mbinding.button7.visibility = View.VISIBLE
                    Toast.makeText(activity, it.msg, Toast.LENGTH_LONG).show()
                }

                else{
                    mbinding.progressBar.visibility = View.INVISIBLE
                    mbinding.button7.visibility = View.VISIBLE
                }

            }

        })
        mbinding.imageView17.setOnClickListener {

            val intent = Intent(activity, ExpandedView::class.java)
            val bundle = Bundle()
            bundle.putString("booking_id", data2.booking_id)
            bundle.putString("total_amount", data2.total_amount)

            intent.putExtras(bundle)

            startActivity(intent)
        }


        //.......................1st layout.....................................//
//set the click on the image
        /*mbinding.imageView.setOnClickListener {

            val fragment = BottomSheetfragment()
            fragment.show(fragmentManager, fragment.tag)

        }*/
        //UNDER LINE THE TEXT BY SPANNABLE

        var Booking_type = data2.booking_type

        if (Booking_type == "2") {
            //set the underline text
            val udata = "SCHEDULE"
            val text = SpannableString(udata)
            //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView60.text = text
        } else if (Booking_type == "3") {
            //set the underline text
            val udata = "SUBSCRIBED"
            val text = SpannableString(udata)
            //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView60.text = text
        } else {

            //set the underline text
            val udata = "INSTANT BOOKING"
            val text = SpannableString(udata)
            //text.setSpan(UnderlineSpan(), 0, udata.length, 0)
            mbinding.textView60.text = text
        }


        Glide.with(this)
            .load(data2.profile_image)
            .into(mbinding.imageView16)



//        mbinding.imageView.setOnClickListener {
//
//            val fragment = BottomSheetfragment()
//            fragment.show(fragmentManager, fragment.tag)
//
//
//        }


        //getting the data
        /*       mvmodel.pendingSucessful().observe(this, Observer {


                   // var data = it
                   for (i in 0 until it.size) {
                       data = it[i]
                       Log.e("category_name1", data.category_name)
                       for(j in 0 until data.modelcategories.size)
                       {
                           var data2 = data.modelcategories[j]
                           Log.e("service_name1",data2.service_name)
                       }

                   }
                   size=it.size



                   Toast.makeText(activity, "all gud", Toast.LENGTH_SHORT).show()
               })
               //on failure
               mvmodel.pendingFailure().observe(this, Observer {


               })*/

    }


    interface setDataInterface {
        fun mapAdapter(position: Int, data: Pendingmodel)
    }


    interface updateCompleted{
        fun update(data: Pendingmodel)
    }


    //get the data of event bus
//    @Subscribe
//    fun getdata(model: MyValue) {
//
//        mbinding.nestedlayout.visibility = View.VISIBLE
//        mbinding.fragmentlayout.visibility = View.GONE
//        var data = model.syncStatusMessage
//        val txt = data.customer_name
//        Toast.makeText(activity, txt, Toast.LENGTH_SHORT).show()
//
//    }


    //destroy

//    override fun onDestroy() {
//        super.onDestroy()
//        BusProvider.getInstance().unregister(this)
//    }


    //geo coading
    private fun getAddress(): String? {
        // 1
        val geocoder = Geocoder(context)
        val address: Address?

        try {
            // 2
            val addresses = geocoder.getFromLocation(latitude.toDouble(), longitude.toDouble(), 1)
            address = addresses[0]
            val builder = StringBuilder()
//            builder.append(address.featureName).append(",\t")
//            builder.append(address.thoroughfare).append(",\t")
            builder.append(address.subAdminArea).append(",\t")
            builder.append(address.countryName)
            var result = builder.toString()
            //mbinding.textView58.text = result
        } catch (e: Exception) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return null
    }

    fun sendStatus() {
        val booking_id = data2.booking_id
        mvmodel.SetActiveBooking(booking_id, "5")
        val sharedPreference = activity!!.getSharedPreferences("Status", Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()
        editor.putString("status", "5")
        editor.commit()

    }

    override fun onResume() {
        super.onResume()
    }

//set the interface
//    override fun onAttach(context: Context?) {
//        super.onAttach(context)
//        setinterface = context as getdatainterface
//    }
//
    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
    interface1 = activity as updateCompleted
    }

}